if ($args[1])
{
	$store = get-item Cert:\LocalMachine\Root     
	$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($args[1]) 
	Write-Host "removing the certificate"
	$store.Open("ReadWrite") 
	$store.Add($cert) 
	Write-Host "Certificate is added to store"
}
else
{
	$certFile = get-childitem $(get-location) | where {$_.Extension -match "cer"} 
	Write-Host certificate file is $certFile
	if ($certFile -ne $NULL) {
		$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($certFile.FullName)
		$store = get-item Cert:\LocalMachine\Root     		
		$store.Open("ReadOnly") 
		$searchedCert = $store.Certificates.Find("FindBySerialNumber",$cert.SerialNumber,$FALSE)[0]
		$store.Close()
		if ($searchedCert -ne $null)
		{
			Write-Host "Certificate with Serial Number" $cert.SerialNumber "is already added to store"			
		}
		else
		{
			$CertificatePath = "$(get-location)\$certFile"
			Write-Host "CertificatePath full path " $CertificatePath
			$ScriptPath = $null
			try
			{
				$ScriptPath = (Get-Variable MyInvocation).Value.MyCommand.Path
			}
			catch {}

			if (!$ScriptPath)
			{
				PrintMessageAndExit $UiStrings.ErrorNoScriptPath $ErrorCodes.NoScriptPath
			}
			
			$RelaunchArgs = '-ExecutionPolicy Unrestricted -file "' + $ScriptPath + '"'
			$RelaunchArgs += ' -CertificatePath "' + $CertificatePath + '"'
			try
			{				
				Write-Host "relaunch " $RelaunchArgs
				$AdminProcess = Start-Process "$PsHome\PowerShell.exe" -Verb RunAs -ArgumentList $RelaunchArgs -PassThru
			}
			catch
			{
			}

			while (!($AdminProcess.HasExited))
			{
				Start-Sleep -Seconds 2
			}
		}
	}
}
