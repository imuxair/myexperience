@echo off
for /f "tokens=4-5 delims=. " %%i in ('ver') do set osVersion=%%i.%%j
rem if "%osVersion%" == "10.0" echo Windows 10
rem if "%osVersion%" == "6.3" echo Windows 8.1
rem if "%osVersion%" == "6.2" echo Windows 8.
rem if "%osVersion%" == "6.1" echo Windows 7.
rem if "%osVersion%" == "6.0" echo Windows Vista.
@echo %osVersion%