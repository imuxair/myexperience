package com.pat.tool.keditor.resources;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.pat.tool.keditor.constants.IMobileChannelNameConstants;
import com.pat.tool.keditor.utils.KConstants;

public class MappingEditorMessages { //extends NLS {

	
	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.mappingeditor_messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private MappingEditorMessages() {
	}
	

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return "";
		}
	}
	
	public static String getAPI(String name) {
		return getString(name);
	}
	
	public static String getOperatorSymbols(String projectType) {
		String value = "";
		
		if (KConstants.PROJECT_TYPE_LUA.equals(projectType))
			value = getString("lua_Operator_symbols");
		else if (KConstants.PROJECT_TYPE_JS.equals(projectType))
			value = getString("js_Operator_symbols");
		//else if ("cpp".equals(projectType)) value = cpp_Operator_symbols;
		
		
		return value;
	}
	
	public static String getPlatformXML(String plat) {
		String value = IMobileChannelNameConstants.general;
		String[] kplats = getString("KonyplatformsXML").split(",");
		for(int i=0; i<kplats.length; i++) {
			String[] kplatsxml = kplats[i].split("::");
			if(plat.equals(kplatsxml[0])) {
				value = kplatsxml[1];
				break;
			}
		}
		return value;
	}

}
