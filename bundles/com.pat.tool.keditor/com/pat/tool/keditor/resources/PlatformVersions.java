package com.pat.tool.keditor.resources;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.framework.Bundle;

import com.kony.studio.StudioContext;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.ant.CustomDefaultLogger;
import com.pat.tool.keditor.console.ConsoleDisplayManager;
import com.pat.tool.keditor.model.psp.Version;
import com.pat.tool.keditor.utils.KConstants;

public class PlatformVersions {

	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.platform_versions"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	public static final String BB_VERSIONS_KEY = "blackberry_versions";
	public static final String ANDROID_VERSIONS_KEY = "android_versions";
	
	private static String[] bb_versions;
	private static String[] js_bb_versions;
	private static String[] android_versions;
	private static String[] android_allowSelfSignedCertsTypeArray;
	private static String android_allowSelfSignedCertsTypeString = "All,Allow Bundled,Allow Pinned";
	private static String[] windows_allowSelfSignedCertsTypeArray;
	private static String windows_allowSelfSignedCertsTypeString = "All,Allow Bundled,Allow Pinned";
	private static Map<String, String> androidAPIVersionsMap;
	private static String API_VERSIONS_FILE = "api_versions.json";
	private static String PLAY_STORE_MINIMUM_TARGET_SDK_API_LEVEL = "playstore_min_target";
	private static int DEFAULT_PLAYSTORE_MIN_TARGET_API_LEVEL = 26;
	
	private PlatformVersions() {
	}
	
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private static void initializeVersions(){
		String string = PlatformVersions.getString(BB_VERSIONS_KEY);
		bb_versions = string.split(",");
		List<String> jsBbVersions = new ArrayList<String>(Arrays.asList(bb_versions));
		//Js FFI Supports bb versions 5.0 and above only
		//So do the following
		for (Iterator<String> iterator = jsBbVersions.iterator(); iterator.hasNext();) {
			String stringVersion = iterator.next();
			if(stringVersion != null){
				Version version = new Version(stringVersion);
				if(version != null && version.compareTo(new Version("5.0")) < 0){
					iterator.remove();
				}
			}
		}
		js_bb_versions = jsBbVersions.toArray(new String[]{});
		/*string = PlatformVersions.getString(ANDROID_VERSIONS_KEY);
		android_versions = string.split(",");*/
	}
	
	public static String[] getAndroidAllowSelfSignedCertsTypes(){
		if(android_allowSelfSignedCertsTypeArray == null)
			android_allowSelfSignedCertsTypeArray = android_allowSelfSignedCertsTypeString.split(",");
		return android_allowSelfSignedCertsTypeArray;
	}
	
	public static String[] getWindowsAllowSelfSignedCertsTypes(){
		if(windows_allowSelfSignedCertsTypeArray == null)
			windows_allowSelfSignedCertsTypeArray = windows_allowSelfSignedCertsTypeString.split(",");
		return windows_allowSelfSignedCertsTypeArray;
	}
	
	public static String[] getBb_versions(String projectType) {
		if(bb_versions == null || js_bb_versions == null)
			initializeVersions();
		if(KConstants.PROJECT_TYPE_LUA.equals(projectType))
			return bb_versions;
		else if(KConstants.PROJECT_TYPE_JS.equals(projectType)){
			return js_bb_versions;
		}
		return new String[]{};
	}
	
	public static String[] getAndroidVersions(){
		if(android_versions == null)
			initializeAndroidVersions();
		return android_versions;
	}
	
	private static void initializeAndroidVersions() {
		
		try {
			String androidExecScriptPath = getAndroidPluginPath();
			if(androidExecScriptPath == null)  {
				return;
			}
			String AndroidAPIVersionFilePath = androidExecScriptPath+"api_versions.json";
			JSONParser parser = new JSONParser();
			JSONObject jsonObject;
			FileReader fileReader = new FileReader(AndroidAPIVersionFilePath);
			jsonObject = (JSONObject) parser.parse(fileReader);
			if(jsonObject.containsKey("api_versions")) {
				androidAPIVersionsMap = new HashMap<String, String>();
				List<String> apiList = new ArrayList<String>();
				JSONArray apiVersionsJsonObject = (JSONArray) jsonObject.get("api_versions");
				for (Object object : apiVersionsJsonObject) {
					String apiLevel = (String) ((JSONObject) object).get("api_level");
					String apiVersion = (String) ((JSONObject) object).get("api_version");
					androidAPIVersionsMap.put(apiVersion, apiVersion +" ("+apiLevel+")");
					apiList.add(apiVersion +" ("+apiLevel+")");
				}
				android_versions = apiList.toArray(new String [apiList.size()]);
			}
		} catch (IOException | ParseException e) {
			KEditorPlugin.logError("Exception while parsing android api_versions.json file.", e);
		}
	}

	private static String getAndroidPluginPath() throws IOException {
		String bundleName = KEditorPlugin.ANDROID_PLUGIN_ID;
		Bundle pBundle = Platform.getBundle(bundleName );
		if(pBundle == null) {
			return null;
		}
		URL fileUrl = FileLocator.toFileURL(FileLocator.find(pBundle, new Path("/"), null)); //$NON-NLS-1$
		String androidPluginLocation = fileUrl.getPath();
		String androidExecScriptPath = androidPluginLocation + KEditorPlugin.ANDROID_EXEC_SCRIPT_FILE_LOCATION;
		return androidExecScriptPath;
	}
	
	public static boolean isTargetSdkAvailabe (String targetSDK) {
		boolean targetSDKStatus = false;
		String androidExecScriptPath;
		String androidTargetSDK = "25";
		if(targetSDK != null && !"None".equals(targetSDK)) {
			androidTargetSDK = getAndroidApiLevel(targetSDK);
		}
		try {
			androidExecScriptPath = getAndroidPluginPath();
			if(androidExecScriptPath == null)  {
				return false;
			}
			String AndroidAPIVersionFilePath = androidExecScriptPath+"target_sdk_check.xml";
			String androidHome = StudioContext.getInstance().getAndroidHome();
			if(androidHome == null || androidHome.isEmpty()) {
				KEditorPlugin.getDefault().logWarning("Android Home not found.");
				return false;
			}
			Project antProject = new Project();
			antProject.init();
			antProject.setProperty("fork", "true");
			antProject.setNewProperty("andtargetsdkkey", androidTargetSDK);
			antProject.setNewProperty("plugin.loc", KEditorPlugin.getDefault().getInstallLocation());
			antProject.setNewProperty("android.home",androidHome);
			ProjectHelper.configureProject(antProject, new File(AndroidAPIVersionFilePath));
			CustomDefaultLogger logger = new CustomDefaultLogger();
			int logLevel = Project.MSG_INFO;
			if(KEditorPlugin.DEBUG_MODE) {
				logLevel = Project.MSG_VERBOSE;
			}
			logger.setMessageOutputLevel(logLevel);
			if(KEditorPlugin.isGUIMode()) {
				logger.setLogMessage(true);
			} else {
				PrintStream psInfo= new PrintStream(System.out);
				psInfo.append(ConsoleDisplayManager.getCurrentDate());
				logger.setOutputPrintStream(psInfo);
				logger.setErrorPrintStream(psInfo);
			}
			antProject.addBuildListener(logger);
			antProject.executeTarget("check_download_required");
			targetSDKStatus = true;
			
		} catch (Exception e) {
			targetSDKStatus = false;
			KEditorPlugin.logError("Exception while executing target_sdk_check.xml file.", e);
		}
		return targetSDKStatus ;
	}

	public static String getAndroidApiLevel(String apiString){
		String apiLevel = "14";
		if(apiString == null || "None".equals(apiString)) {
			return apiLevel;
		}
		String[] split = apiString.split(" ");
		if(split.length > 1) {
			apiLevel = split[1].substring(1,split[1].length()-1);
		} else {
			if(androidAPIVersionsMap == null || androidAPIVersionsMap.isEmpty()) {
				initializeAndroidVersions();
			}
			String string = androidAPIVersionsMap.get(apiString);
			apiLevel = getAndroidApiLevel(string);
		}
		return apiLevel;
	}
	
	public static String getAndroidApiVersion(String apiString){
		String apiVersion = "4.0";
		if(apiString == null || "None".equals(apiString)) {
			return apiVersion;
		}
		String[] split = apiString.split(" ");
		apiVersion = split[0];
		return apiVersion;
	}
	
	/*public static String getAndroidApiStringfromLevel(String level) {
		String apiString = "None";
		if(androidAPIVersionsMap == null || androidAPIVersionsMap.isEmpty()) {
			initializeAndroidVersions();
		}
		if(androidAPIVersionsMap.get(level) != null) {
			apiString = androidAPIVersionsMap.get(level);
		}
		return apiString;
	}*/
	
	public static String getAndroidApiStringfromVersion(String version) {
		String apiString = "None";
		if(androidAPIVersionsMap == null || androidAPIVersionsMap.isEmpty()) {
			initializeAndroidVersions();
		}
		apiString = android_versions[0];
		if(androidAPIVersionsMap.get(version) != null) {
			apiString = androidAPIVersionsMap.get(version);
		}
		return apiString;
	}
	
	public static String[] getAndroidWearSDKVersions() {
		String[] androidVersions = getAndroidVersions();
		List<String> androidWearVersionsList = new ArrayList<String>();
		for (int i = 0; i < androidVersions.length; i++) {
			if (Integer.parseInt(PlatformVersions.getAndroidApiLevel(androidVersions[i])) >= 25) {
				androidWearVersionsList.add(androidVersions[i]);
			}
		}
		String[] andWearVersions = new String[androidWearVersionsList.size()];
		andWearVersions = androidWearVersionsList.toArray(andWearVersions);
		return andWearVersions;
	}

	public static int getPlayStoreMinTarget() {
		int playStoreMinTargetAPILevel = DEFAULT_PLAYSTORE_MIN_TARGET_API_LEVEL;
		try {
			String androidExecScriptPath = getAndroidPluginPath();
			if (androidExecScriptPath == null) {
				return playStoreMinTargetAPILevel;
			}
			String AndroidAPIVersionFilePath = androidExecScriptPath + API_VERSIONS_FILE;
			JSONParser parser = new JSONParser();
			JSONObject jsonObject;
			FileReader fileReader = new FileReader(AndroidAPIVersionFilePath);
			jsonObject = (JSONObject) parser.parse(fileReader);
			if (jsonObject.containsKey(PLAY_STORE_MINIMUM_TARGET_SDK_API_LEVEL)) {
				playStoreMinTargetAPILevel = Integer.parseInt((String) jsonObject.get(PLAY_STORE_MINIMUM_TARGET_SDK_API_LEVEL));
			}
		} catch (Exception e) {
			KEditorPlugin.logError("Error occured while getting playstore minimum target sdk api level", e);
		}

		return playStoreMinTargetAPILevel;

	}
}
