package com.pat.tool.keditor.resources;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class BuildScreenPlatforms {
	
	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.buildscreen_platforms";
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	public static final String BLACKBERRY_VERSIONS_KEY = "bb_versions";
	public static final String BLACKBERRY_JS_VERSIONS_KEY = "bb_js_versions";
	public static final String BLACKBERRY_7_0_VERSIONS_KEY = "blackberry_7.0_versions";
	public static final String BLACKBERRY_6_0_VERSIONS_KEY = "blackberry_6.0_versions";
	public static final String BLACKBERRY_5_0_VERSIONS_KEY = "blackberry_5.0_versions";
	public static final String BLACKBERRY_4_7_VERSIONS_KEY = "blackberry_4.7_versions";
	public static final String BLACKBERRY_4_5_VERSIONS_KEY = "blackberry_4.5_versions";
	public static final String BLACKBERRY_4_2_1_VERSIONS_KEY = "blackberry_4.2.1_versions";
	
	
	public static final String  WINMOBILE_VERSIONS_KEY = "win_versions";
	private static String[] winmobile_versions;
	
	//for build screen
	private static String[] blackberry_versions;
	private static String[] bb_versions_7_0;
	private static String[] bb_versions_6_0;
	private static String[] bb_versions_5_0;
	private static String[] bb_versions_4_7;
	private static String[] bb_versions_4_5;
	private static String[] bb_versions_4_2_1;
	
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private static void initializeVersions(){
		String string = getString(BLACKBERRY_VERSIONS_KEY);
		blackberry_versions = string.split(",");
		string = getString(BLACKBERRY_7_0_VERSIONS_KEY);
		bb_versions_7_0 = string.split(",");
		string = getString(BLACKBERRY_6_0_VERSIONS_KEY);
		bb_versions_6_0 = string.split(",");
		string = getString(BLACKBERRY_5_0_VERSIONS_KEY);
		bb_versions_5_0 = string.split(",");
		string = getString(BLACKBERRY_4_7_VERSIONS_KEY);
		bb_versions_4_7 = string.split(",");
		string = getString(BLACKBERRY_4_5_VERSIONS_KEY);
		bb_versions_4_5 = string.split(",");
		string = getString(BLACKBERRY_4_2_1_VERSIONS_KEY);
		bb_versions_4_2_1 = string.split(",");
	}
	
	public static String[] getBlackberry_versions() {
		if(blackberry_versions == null)
			initializeVersions();
		return blackberry_versions;
	}
	
	public static String[] getBlackberry_7_0versions() {
		if(bb_versions_7_0 == null)
			initializeVersions();
		return bb_versions_7_0;
	}
	
	public static String[] getBlackberry_6_0versions() {
		if(bb_versions_6_0 == null)
			initializeVersions();
		return bb_versions_6_0;
	}
	
	public static String[] getBlackberry_5_0versions() {
		if(bb_versions_5_0 == null)
			initializeVersions();
		return bb_versions_5_0;
	}
	
	public static String[] getBlackberry_4_7versions() {
		if(bb_versions_4_7 == null)
			initializeVersions();
		return bb_versions_4_7;
	}
	
	public static String[] getBlackberry_4_5versions() {
		if(bb_versions_4_5 == null)
			initializeVersions();
		return bb_versions_4_5;
	}
	
	public static String[] getBlackberry_4_2_1versions() {
		if(bb_versions_4_2_1 == null)
			initializeVersions();
		return bb_versions_4_2_1;
	}
	
	private static void initializeWinVersions(){
		String string = getString(WINMOBILE_VERSIONS_KEY);
		winmobile_versions = string.split(",");
		
	}
	
	public static String[] getWinmobile_versions() {
		if(winmobile_versions == null)
			initializeWinVersions();
		return winmobile_versions;
	}
	/**
	 * This API returns the height and width of the corresponding platform
	 * @param platform
	 * @return int []
	 */
	public static int[] getPreviewFormFactors(String platform){
		try{
			String formFactor[] = getString(platform).trim().split(",");
			int hw[]={Integer.valueOf(formFactor[0]),Integer.valueOf(formFactor[1])};
			return hw;
		}catch(Exception e)
		{ 
			String formFactor[] = getString("DEFAULT").trim().split(",");
			int hw[]={Integer.valueOf(formFactor[0]),Integer.valueOf(formFactor[1])};
			return hw;
		}				
	}
}
