package com.pat.tool.keditor.resources;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.model.IWidget;
import com.pat.tool.keditor.model.KForm;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.model.WidgetUtil;
import com.pat.tool.keditor.model.WidgetUtil.Widget;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.SerializationUtil;
import com.pat.tool.keditor.utils.windowsViews.WinViewsConstants;

/**
 * @author Ram Anvesh
 * @since  3.2
 * @date Jul 26, 2011
 */
public class WidgetDefaultsUtils {

	/**
	 * 
	 */
	private static final String DEFAULTS_CONSTANT = "defaults";
	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.widget_default_props"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	public static final String DEFAULTS_FOLDER_NAME = DEFAULTS_CONSTANT;
	public static final String DEFAULTS_FILE_NAME = DEFAULTS_CONSTANT + ".kl";
	public static final String DEFAULTS_FORM_ID = DEFAULTS_CONSTANT;
	
	private static ArrayList<IWidget> applicableWidgets;
	private static Map<String, List<String>> applicablePropsMap = new HashMap<String, List<String>>();
	
	private WidgetDefaultsUtils() {
	}

	public static String getProperty(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	public static List<String> getPropertiesList(String widgetDisplayName){
		List<String> applicableProps = applicablePropsMap.get(widgetDisplayName);
		if(applicableProps != null)
			return applicableProps;
		String string = getProperty(widgetDisplayName);
		String[] split = string.split("\\s*,\\s*");
		for (int i = 0; i < split.length; i++) {
			split[i] = split[i].trim();
		}
		applicableProps = Arrays.asList(split);
		applicablePropsMap.put(widgetDisplayName, applicableProps);
		return applicableProps;
	}
	
	public static List<IWidget> getApplicableWidgetList(){
		if(applicableWidgets != null)
			return applicableWidgets;
		Enumeration<String> keys = RESOURCE_BUNDLE.getKeys();
		ArrayList<String> list = Collections.list(keys);
		applicableWidgets = new ArrayList<IWidget>(list.size());
		for(String widgetName : list){
			applicableWidgets.add(WidgetUtil.getWidgetByName(widgetName));
		}
		return applicableWidgets;
	}

	/**
	 * Loads and returns the kPage object which contains all widgets which contain the project default properties.
	 * If the defaults.kl file does not exist, it creates a new one and returns a new object.
	 * @return
	 */
	public static KPage getDefaultsModel(IProject project) {
		IFile defaultsFile = project.getFile(DEFAULTS_FOLDER_NAME + File.separator + DEFAULTS_FILE_NAME);
		KPage page = null;
		if(!defaultsFile.exists()){
			page = createNewDefaultsModel(project);
		} else {
			try {
				defaultsFile.refreshLocal(IResource.DEPTH_ZERO, null);
				JAXBContext context = KEditorPlugin.getFormjaxbcontext(); //FIX: Load should only happen through SerializationUtil.getPage(IFile file) 
				Unmarshaller um = context.createUnmarshaller();
				page = (KPage) um.unmarshal(defaultsFile.getContents());
				String fileName = defaultsFile.getName();
				String formId = fileName.substring(0, fileName.length()-3);
				initNewPage(project, page, formId);
			} catch (Exception e) {
				KEditorPlugin.logError(e.getMessage(), e);
			}
		}
		return page;
	}

	private static void initNewPage(IProject project, KPage page, String formId) {
		page.setProjectName(project.getName());
		KForm form = page.getForm();
		if (form != null) {
			form.setID(formId);
			page.setFormName(formId);
		}
	}
	
	private static KPage createNewDefaultsModel(IProject project) {
		KPage page = new KPage();
		page.addChild1(new KForm());
		initNewPage(project, page, DEFAULTS_FORM_ID);
		return page;
	}
	
	public static void saveDefaultsModel(KPage page, IProject project){
		IFile defaultsFile = project.getFile(DEFAULTS_FOLDER_NAME + File.separator + DEFAULTS_FILE_NAME);
		try {
			JAXBContext formjaxbcontext = KEditorPlugin.getFormjaxbcontext(); //FIX: Save should only happen through SerializationUtil.saveForm(..)
			Marshaller marshaller = formjaxbcontext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			if(!defaultsFile.exists()){
				marshaller.marshal(page, new File(KUtils.getProjectLocation(project) + File.separator +
						DEFAULTS_FOLDER_NAME + File.separator + DEFAULTS_FILE_NAME));
			} else {
				StringWriter writer = new StringWriter();
				marshaller.marshal(page, writer); 
				ByteArrayInputStream bis = new ByteArrayInputStream(writer.toString().getBytes("UTF-8"));
				if(defaultsFile.isReadOnly()) {
					MessageDialog msgbox = new MessageDialog(Display.getCurrent().getActiveShell(), "Error", null, 
							"Can't save the file, please remove read-only property of file.", MessageDialog.ERROR, new String[] {"OK"}, 1);
					msgbox.open();
				} else {
					defaultsFile.setContents(bis, true, false, null);
				}
			}
		} catch (Exception e) {

		}			
	}

	/**
	 * This method is used for PSPs which have project level Default properties. 
	 * 
	 * @param formValue
	 * @param propertyName
	 * @param props
	 * @return <code>formValue</code> if it is not equal to "Default". Project level default value if <code>formValue</code> equals "Default" 
	 */
	public static String getResultant(String formValue, String propertyName, Properties props) {
		if(WinViewsConstants.DEFAULT.equals(formValue)){
			Object object = props.get(propertyName);
			if(object == null)
				return formValue;
			else
				return object.toString();
		} else 
			return formValue;
	}
}
