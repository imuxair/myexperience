package com.pat.tool.keditor.dialogs;
/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Sebastian Davids <sdavids@gmx.de> - Fix for bug 93353 - 
 *     [Dialogs] InternalErrorDialog#buttonPressed should explicitly call super
 *******************************************************************************/

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Added a Details button to the MessageDialog to show the exception
 * stack trace.
 * 
 * Note: This class is a copy of org.eclipse.ui.internal.ide.dialogs.InternalErrorDialog with some minor modifications
 * 
 */
public class ExceptionDialog extends MessageDialog {

    private Throwable detail;

    private int detailButtonID = -1;

    private Text text;

    //Workaround. SWT does not seem to set the default button if 
    //there is not control with focus. Bug: 14668
    private int defaultButtonIndex = 0;

    /**
     * Size of the text in lines.
     */
    private static final int TEXT_LINE_COUNT = 15;

    /**
     * Create a new dialog.
     * 
     * @param parentShell the parent shell
     * @param dialogTitle the  title
     * @param dialogTitleImage the title image
     * @param dialogMessage the message
     * @param detail the error to display
     * @param dialogImageType the type of image
     * @param dialogButtonLabels the button labels
     * @param defaultIndex the default selected button index
     */
    public ExceptionDialog(Shell parentShell, String dialogTitle,
            Image dialogTitleImage, String dialogMessage, Throwable detail,
            int dialogImageType, String[] dialogButtonLabels, int defaultIndex) {
        super(parentShell, dialogTitle, dialogTitleImage, dialogMessage,
                dialogImageType, dialogButtonLabels, defaultIndex);
        defaultButtonIndex = defaultIndex;
        this.detail = detail;
        setShellStyle(getShellStyle() | SWT.APPLICATION_MODAL | SWT.RESIZE);
    }

    //Workaround. SWT does not seem to set rigth the default button if 
    //there is not control with focus. Bug: 14668
    public int open() {
        create();
        Button b = getButton(defaultButtonIndex);
        b.setFocus();
        b.getShell().setDefaultButton(b);
        return super.open();
    }
    
    /**
     * Set the detail button;
     * @param index the detail button index
     */
    public void setDetailButton(int index) {
        detailButtonID = index;
    }

    /* (non-Javadoc)
     * Method declared on Dialog.
     */
    protected void buttonPressed(int buttonId) {
        if (buttonId == detailButtonID) {
            toggleDetailsArea();
        } else {
            super.buttonPressed(buttonId);
        }
    }

    /**
     * Toggles the unfolding of the details area.  This is triggered by
     * the user pressing the details button.
     */
    private void toggleDetailsArea() {
        Point windowSize = getShell().getSize();
        Point oldSize = getContents().computeSize(SWT.DEFAULT, SWT.DEFAULT);

        if (text != null) {
            text.dispose();
            text = null;
            getButton(detailButtonID).setText(
                    IDialogConstants.SHOW_DETAILS_LABEL);
        } else {
            createDropDownText((Composite) getContents());
            getButton(detailButtonID).setText(
                    IDialogConstants.HIDE_DETAILS_LABEL);
        }

        Point newSize = getContents().computeSize(SWT.DEFAULT, SWT.DEFAULT);
        getShell()
                .setSize(
                        new Point(windowSize.x, windowSize.y
                                + (newSize.y - oldSize.y)));
    }

    /**
     * Create this dialog's drop-down list component.
     *
     * @param parent the parent composite
     */
    protected void createDropDownText(Composite parent) {
        // create the list
        text = new Text(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.READ_ONLY);
        text.setFont(parent.getFont());

        // print the stacktrace in the text field
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            detail.printStackTrace(ps);
            ps.flush();
            baos.flush();
            text.setText(baos.toString());
        } catch (IOException e) {
        }

        GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL
                | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
                | GridData.GRAB_VERTICAL);
        data.heightHint = text.getLineHeight() * TEXT_LINE_COUNT;
        data.horizontalSpan = 2;
        text.setLayoutData(data);
    }

	public static void openError(Shell parent, String title, String message, Throwable detail, int defaultIndex) {
		openError(parent, title, message, detail, defaultIndex, null);
	}
	
	public static void openError(Shell parent, String title, String message, Throwable detail, int defaultIndex, QuickFixRunnable runnable) {
		String[] labels;
		if (detail == null) {
			labels = new String[] { IDialogConstants.OK_LABEL };
		} else if (runnable != null) {
			labels = new String[] { IDialogConstants.OK_LABEL, IDialogConstants.SHOW_DETAILS_LABEL, runnable.getQuickFixLabel()};
		} else {
			labels = new String[] { IDialogConstants.OK_LABEL, IDialogConstants.SHOW_DETAILS_LABEL };
		}

		ExceptionDialog dialog = new ExceptionDialog(parent, title, null, // accept the default window icon
				message, detail, ERROR, labels, defaultIndex);
		if (detail != null) {
			dialog.setDetailButton(1);
		}
		int open = dialog.open();
		if (open == 2) {
			runnable.run();
		}
	}

	public static interface QuickFixRunnable extends Runnable {
		String getQuickFixLabel();
	}

}
