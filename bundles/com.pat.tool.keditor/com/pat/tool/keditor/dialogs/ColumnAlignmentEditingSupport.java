package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.KUtils;

/**
 * 
 * @author Rakesh
 * 3 Feb,2012
 */
public class ColumnAlignmentEditingSupport extends EditingSupport {
	
	private TableDataDialog tableDataDialog;
	private ComboBoxCellEditor contentAlignTypeCellEditor;
	
	public ColumnAlignmentEditingSupport(TableDataDialog tableDataDialog, ColumnViewer viewer, int columnIndex){
		super(viewer);
		Composite parent = (Composite) viewer.getControl();
		contentAlignTypeCellEditor = new ComboBoxCellEditor(parent, KUtils.CONTENT_ALIGNS, SWT.READ_ONLY | SWT.SINGLE);
		this.tableDataDialog = tableDataDialog;
	}
	@Override
	protected CellEditor getCellEditor(Object element) {
		return contentAlignTypeCellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		if(element instanceof ColumnData){
			ColumnData cData = (ColumnData) element;
			return cData.align;
		}
		return null;
	}

	@Override
	protected void setValue(Object element, Object value) {
		if(element instanceof ColumnData){
			ColumnData cData = (ColumnData) element;
			cData.align = (Integer)value;
			tableDataDialog.validatePage(true);
			getViewer().refresh(cData);
		}
	}

}
