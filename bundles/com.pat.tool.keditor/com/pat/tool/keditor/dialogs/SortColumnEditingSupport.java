package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.KUtils;

/**
 * 
 * @author Rakesh
 * 3 Feb,2012
 */
public class SortColumnEditingSupport extends EditingSupport {
	
	private static final int TRUE = 1;
	private static final int FALSE = 0;
	private int columnIndex;
	private TableDataDialog tableDataDialog;
	private ComboBoxCellEditor sortTypeCellEditor;
	
	
	public SortColumnEditingSupport(TableDataDialog tableDataDialog, ColumnViewer viewer, int columnIndex){
		super(viewer);
		Composite parent = (Composite) viewer.getControl();
		sortTypeCellEditor = new ComboBoxCellEditor(parent, KUtils.booleanArray, SWT.READ_ONLY | SWT.SINGLE);
		this.columnIndex = columnIndex;
		this.tableDataDialog = tableDataDialog;	
	}
	
	@Override
	protected CellEditor getCellEditor(Object element) {
		return sortTypeCellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		if(element instanceof ColumnData){
			ColumnData cData = (ColumnData) element;
			if(cData.sort == Boolean.FALSE){
				return FALSE;
			}
			return TRUE;
		}
		return FALSE;
	}

	@Override
	protected void setValue(Object element, Object value) {
		if(element instanceof ColumnData){
			ColumnData cData = (ColumnData) element;
			int parseInt = Integer.parseInt(value.toString());
			cData.sort = (parseInt == FALSE ?Boolean.FALSE:Boolean.TRUE);
			tableDataDialog.validatePage(true);
			getViewer().refresh(cData);
		}
	}

}
