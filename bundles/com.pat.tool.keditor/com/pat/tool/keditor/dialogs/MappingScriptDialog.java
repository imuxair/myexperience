/* @copyright Kony Solutions Inc.(c) 2011 */
package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;

import com.konylabs.middleware.dataobject.Dataset;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.clipboard.Clipboard;
import com.pat.tool.keditor.editors.KEditor;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.mapping.model.NBinding;
import com.pat.tool.keditor.mapping.model.NBindingElement;
import com.pat.tool.keditor.mapping.model.NBindingRoot;
import com.pat.tool.keditor.mapping.model.NMapping;
import com.pat.tool.keditor.mapping.tabs.AlertComposite;
import com.pat.tool.keditor.mapping.tabs.CollectionVariableComposite;
import com.pat.tool.keditor.mapping.tabs.CommentComposite;
import com.pat.tool.keditor.mapping.tabs.DecisionComposite;
import com.pat.tool.keditor.mapping.tabs.ElseIfConditionComposite;
import com.pat.tool.keditor.mapping.tabs.EventMappingUtil;
import com.pat.tool.keditor.mapping.tabs.ExpressionComposite;
import com.pat.tool.keditor.mapping.tabs.FormPopupNavigationComposite;
import com.pat.tool.keditor.mapping.tabs.FunctionComposite;
import com.pat.tool.keditor.mapping.tabs.IConfiguration;
import com.pat.tool.keditor.mapping.tabs.IStatusUpdater;
import com.pat.tool.keditor.mapping.tabs.PreDecisionComposite;
import com.pat.tool.keditor.mapping.tabs.ServiceComposite;
import com.pat.tool.keditor.mapping.tabs.SimpleVariableComposite;
import com.pat.tool.keditor.model.IPropertyNameConstants;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.model.KWidget;
import com.pat.tool.keditor.utils.AddVariableWindow;
import com.pat.tool.keditor.utils.EventAction;
import com.pat.tool.keditor.utils.EventAlertAction;
import com.pat.tool.keditor.utils.EventCommentAction;
import com.pat.tool.keditor.utils.EventDecisionAction;
import com.pat.tool.keditor.utils.EventElseifAction;
import com.pat.tool.keditor.utils.EventExpressionAction;
import com.pat.tool.keditor.utils.EventFormAction;
import com.pat.tool.keditor.utils.EventFunctionAction;
import com.pat.tool.keditor.utils.EventInvokeServiceAction;
import com.pat.tool.keditor.utils.EventMappingAction;
import com.pat.tool.keditor.utils.EventPreprocessDecisionAction;
import com.pat.tool.keditor.utils.EventRoot;
import com.pat.tool.keditor.utils.EventSequence;
import com.pat.tool.keditor.utils.EventSequenceAction;
import com.pat.tool.keditor.utils.EventVariableAction;
import com.pat.tool.keditor.utils.ExpressionEditorUtils;
import com.pat.tool.keditor.utils.FilterListWindow;
import com.pat.tool.keditor.utils.FunctionParamValueCellEditor;
import com.pat.tool.keditor.utils.FunctionParamValueData;
import com.pat.tool.keditor.utils.HttpHeaderData;
import com.pat.tool.keditor.utils.IEventActionTypes;
import com.pat.tool.keditor.utils.KConstants;
import com.pat.tool.keditor.utils.KLFileManager;
import com.pat.tool.keditor.utils.KNUtils;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.LMFactory;
import com.pat.tool.keditor.utils.NScriptDialog;
import com.pat.tool.keditor.views.NavigationView;
import com.pat.tool.keditor.widgets.MobileChannels;
import com.pat.tool.keditor.wizards.CustomWizardDialog;
import com.pat.tool.keditor.wizards.NMappingWizard;

/**
 * 
 * This class is re-written from its original class {@link NScriptDialog}. 
 * Idea of writing this class to restructure and shuffle reusable code in
 * separate java file. This class can be further customized.
 * 
 * @author Rakesh
 * @since Jan 24, 2011  
 */

public class MappingScriptDialog extends TrayDialog implements IStatusUpdater{

	public static final String CALLBACK_CONSTANT = "callback sequence";
	public static final String SUCCESS_CALLBACK_CONSTANT = "Success callback sequence";
	public static final String ERROR_CALLBACK_CONSTANT = "Error callback sequence";
	
 	public static boolean actnPerfrm;

	private Combo globalseqCombo;
	private Button globalRadio;
	private Button localRadio;
	
	private boolean isGlobal = false;
	private String seqName = "";
	
	private String actionItems[] = {"Invoke Service (Synchronous)", "Navigate to Form/Popup", "Show Alert", "Add Decision", "Add Snippet", "Invoke Function", 
			"Add Local Variable", "Add Preprocess Decision", "Add Comment", "Add Else if", "Add Mapping","Paste", "Invoke Sequence","Invoke Service (Asynchronous)"};
	private MenuItem invokeServiceSyncMenuItem;
	private MenuItem formMenuItem;
	private MenuItem alertMenuItem;
	private MenuItem expressionMenuItem;
	private MenuItem decisionMenuItem;
	private MenuItem functionMenuItem;
	private MenuItem sequenceMenuItem;
	private MenuItem invokeServiceAsyncMenuItem;
	
	private MenuItem variableMenuItem;
	private MenuItem simplevarMenuItem;
	private MenuItem collectionvarMenuItem;
	
	private MenuItem deleteMenuItem;
	private MenuItem moveUPMenuItem;
	private MenuItem moveDownMenuItem;
	private MenuItem copyMenuItem;
	private MenuItem pasteMenuItem;
	private MenuItem predecisionMenuItem;
	private MenuItem commentMenuItem;
	private MenuItem mappingMenuItem;
	
	private MenuItem elseifMenuItem;
	private MenuItem addbeforeMenuItem;
	private MenuItem addafterMenuItem;
	
	private MenuItem beforeInvokeServiceSyncMenuItem;
	private MenuItem beforeformMenuItem;
	private MenuItem beforealertMenuItem;
	private MenuItem beforeexpressionMenuItem;
	private MenuItem beforedecisionMenuItem;
	private MenuItem beforefunctionMenuItem;
	private MenuItem beforesequenceMenuItem;
	private MenuItem beforeInvokeServiceAsyncMenuItem;
	
	private MenuItem beforevariableMenuItem;
	private MenuItem beforesimplevarMenuItem;
	private MenuItem beforecollectionvarMenuItem;
	
	private MenuItem beforepredecisionMenuItem;
	private MenuItem beforecommentMenuItem;
	private MenuItem beforemappingMenuItem;
	
	private MenuItem afterInvokeServiceSyncMenuItem;
	private MenuItem afterformMenuItem;
	private MenuItem afteralertMenuItem;
	private MenuItem afterexpressionMenuItem;
	private MenuItem afterdecisionMenuItem;
	private MenuItem afterfunctionMenuItem;
	private MenuItem aftersequenceMenuItem;
	private MenuItem afterInvokeServiceAsyncMenuItem;
	
	private MenuItem aftervariableMenuItem;
	private MenuItem aftersimplevarMenuItem;
	private MenuItem aftercollectionvarMenuItem;

	private MenuItem afterpredecisionMenuItem;
	private MenuItem aftercommentMenuItem;
	private MenuItem aftermappingMenuItem;
	
	private MenuItem beforePasteMenuItem;
	private MenuItem afterPasteMenuItem;
	
	private Text seqnameText;
	private Button makeglobal;
	
	private volatile TreeViewer sequenceTreeViewer;
	private StackLayout detailGrpStackLayout;
	
	private Button servFilterButton;
	private Button seqFilterButton;
	private Group detailRightPaneGrp;
	
	private Combo serviceCombo;
	private Button svcInputBindButton;
	private Button blockCheck;
	private Button secureCheck;
	
	private Button outputBindButton;
	private Button mapBindButton;
	
	//Variable
	private Text variableValueText;
	private Combo variableDataTypeCombo;
	private Combo variableValueTypeCombo;
	private Combo variableBooleanCombo;
	private Composite varComp;
	private Composite varNormalComp;
	private Composite varBooleanComp;
	private StackLayout varstack;
	
	private Button varTestButton;
	private Button varClearButton;

 	protected String[] userFuncArray = {KUtils.DEFAULT_NONE};
 	protected String[] formArray = {KUtils.DEFAULT_NONE};
 	protected List<String> userFuncList;
 	protected String[] busServiceArray;
 	
 	private List<String> busServiceList;
 	
 	protected List <String> formList;
    protected List<String> i18nkeysList;
    protected List<String> popupList;
    protected String[] i18n_array = {KUtils.DEFAULT_NONE};
    private KPage pageRoot;
 	private String eventType;
 	private KWidget widget;
 	private String projName;
 	private String errString = "";
 	private String globalerrString = "";
 	private String emptyseqerrString;
 	private NBinding globalBinding;
 	private String projectType = "lua";
 	
 	private NBinding dsstoreBinding;
	private NBinding i18nkeysBinding;
	private NBinding skinBinding;
	private NBinding themeBinding;
	
	private Composite variableExprBuilderComp;
	
	private EventRoot root = new EventRoot();
	private EventRoot unModified_Root = new EventRoot();
	
	private boolean dirty;
	
	private List<String> globalSeqList;
	private NBinding globalSeqBinding;
	
	private String formName;
	private IConfiguration configuration;
	private Composite selectedComposite;
	private Object selectedObject;
	
	private int category = KPage.MOBILE_CAT;

	
	public MappingScriptDialog(Shell activeShell,
			IConfiguration configuration, EventRoot root) {
		super(activeShell);
		this.configuration = configuration;
		this.formList = configuration.getListOfFormNames();
		this.userFuncList = configuration.getListOfFunctionNames();
		this.pageRoot = configuration.getPage();
		this.eventType = configuration.getEventType();
		this.widget = configuration.getWidget();
		this.projName = configuration.getProjectName();
		init(configuration, root);
	}

	private void init(IConfiguration configuration, EventRoot root) {
		//svcList = configuration.getListOfService();//KUtils.buildServiceList(projName);
		busServiceList = configuration.getServicesIDList();//KUtils.getServiceIdList(this.svcList);
		//busServiceArray = configuration.getBusServiceArray();//busServiceList.toArray(new String[busServiceList.size()]);
		formArray = formList.toArray(new String[formList.size()]);
		userFuncArray = userFuncList.toArray(new String[userFuncList.size()]);
		popupList = KUtils.getPopupNames(projName);
		i18nkeysList = KUtils.getI18KeyNames(projName,"");
		i18n_array = i18nkeysList.toArray(new String[i18nkeysList.size()]);
		globalBinding = configuration.getGlobalBindingVariables();//LMFactory.getNGobalVariables(projName);
		projectType = KNUtils.getProjectType(projName);
		
		this.dsstoreBinding = configuration.getDSKeyBindings();//buildDSKeys();
		this.i18nkeysBinding = configuration.getI18nKeyBindings();//buildI18NKeys();
		this.globalSeqList = LMFactory.getGlobalSequenceNames(projName);
		this.globalSeqBinding = LMFactory.getGlobalSequences(projName);
		skinBinding = LMFactory.getSkinsBinding(projName);
		themeBinding = LMFactory.getThemeBindings(projName);
		this.unModified_Root = root;
		this.root = unModified_Root.getCopy();
		
		if(widget == null) {
			formName = KUtils.DEFAULT_NONE;
		} else {
			formName = widget.getRoot().getFormName();
		}
		seqName = "";
		
		category = EventMappingUtil.getCategory(pageRoot, eventType);
	}
	
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Event Editor");
	}
	
	@Override
	protected int getShellStyle() {
		return super.getShellStyle() | SWT.RESIZE;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite compositeDialog = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		compositeDialog.setLayout(layout);
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.widthHint = 900;
		gridData.heightHint = 600;
		compositeDialog.setLayoutData(gridData);
		
		createGroup(compositeDialog);
		return compositeDialog;
	}
	
	public void createGroup(Composite compDialog) {
		Composite compGrp = new Composite(compDialog, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;
		compGrp.setLayout(layout);
		compGrp.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		//mainComp.setLayoutData(data);
		// createSearchGroup(mainComp);
		SashForm sash = new SashForm(compGrp, SWT.HORIZONTAL);
		GridLayout layout1 = new GridLayout(1, false);
		sash.setLayout(layout1);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		sash.setLayoutData(data);
		
		//createCoolBar(compGrp);
		createLeftPane(sash);
		createRightPane(sash);
		sash.setWeights(new int[]{1,2});
		
		Label noteLabel = new Label(compGrp, SWT.NONE);
		noteLabel.setText("Note: Any changes made to global sequences will be reflected across application");
		noteLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
		data = new GridData(GridData.FILL_HORIZONTAL);
		noteLabel.setLayoutData(data);
	}	
	
	public void createLeftPane(Composite compGrp) {
		Group actionGroup = new Group(compGrp, SWT.NONE);
		actionGroup.setText("Define Action Sequence");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		actionGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_VERTICAL);
		data.widthHint = ((int)(0.4 * 900));
		actionGroup.setLayoutData(data);
		
		sequenceTreeViewer = new TreeViewer(actionGroup, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		sequenceTreeViewer.setAutoExpandLevel(10);
		
		sequenceTreeViewer.setUseHashlookup(true);
	    data = new GridData(GridData.FILL_BOTH);
	    sequenceTreeViewer.getControl().setLayoutData(data);
		
	    sequenceTreeViewer.addSelectionChangedListener(new SequenceTreeViewerSelectionListener());
	    sequenceTreeViewer.setContentProvider(new SequenceTreeViewContentProvider());
	    sequenceTreeViewer.setLabelProvider(new SequenceTreeViewLabelProvider());
        
	    final Menu srcMenu = new Menu(sequenceTreeViewer.getTree()); 
	    sequenceTreeViewer.getTree().setMenu(srcMenu);
	    sequenceTreeViewer.setInput(root);
	    
        srcMenu.addMenuListener(new MenuAdapter() { 
           public void menuShown(MenuEvent e) { 
        	   
              // Get rid of existing menu items 
              MenuItem[] items = srcMenu.getItems(); 
              for (int i = 0; i < items.length; i++) { 
                 ((MenuItem) items[i]).dispose(); 
              }
              
              TreeItem varItem = sequenceTreeViewer.getTree().getSelection()[0];
              Object ele = (Object)varItem.getData();
              
              if(ele instanceof EventDecisionAction) {
            	  elseifMenuItem = new MenuItem(srcMenu, SWT.NONE);
            	  elseifMenuItem.setText(actionItems[9]);
            	  elseifMenuItem.addSelectionListener(new CustomSelectionAdapter());
              }
              
              if(ele instanceof EventSequence) {
            	  if(((EventSequence)ele).getActionList().size() > 0) {
            		  copyMenuItem = new MenuItem(srcMenu, SWT.NONE); 
                	  copyMenuItem.setText("Copy");
                	  copyMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.copyImage));
                	  copyMenuItem.addSelectionListener(new CustomSelectionAdapter() );
            	  }
            	  
            	  Object obj = Clipboard.getDefault().getContents();
        		  if(obj != null) {
        			  pasteMenuItem = new MenuItem(srcMenu, SWT.NONE); 
        			  pasteMenuItem.setText("Paste");
        			  pasteMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.pasteImage));
        			  pasteMenuItem.addSelectionListener(new CustomSelectionAdapter() );
        		  }
              }
              
              if(ele instanceof EventAction) {
            	  copyMenuItem = new MenuItem(srcMenu, SWT.NONE); 
            	  copyMenuItem.setText("Copy");
            	  copyMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.copyImage));
            	  copyMenuItem.addSelectionListener(new CustomSelectionAdapter() );
            	  
            	  if(((EventAction)ele).getName() != null &&("true".equals(((EventAction)ele).getName()) || "false".equals(((EventAction)ele).getName())) || CALLBACK_CONSTANT.equals(((EventAction)ele).getName())) {
        			  pasteMenuItem = new MenuItem(srcMenu, SWT.NONE); 
        			  pasteMenuItem.setText("Paste");
        			  pasteMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.pasteImage));
        			  pasteMenuItem.addSelectionListener(new CustomSelectionAdapter() );
            	  }
              }
              
              if (ele instanceof EventAction) {
            	  if ("true".equals(((EventAction)ele).getName()) || "false".equals(((EventAction)ele).getName()) 
            			  || CALLBACK_CONSTANT.equals(((EventAction)ele).getName()) ) {
            		      		
            	  } else {
	            	  deleteMenuItem = new MenuItem(srcMenu, SWT.NONE); 
	            	  deleteMenuItem.setText("Delete");
	            	  deleteMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.deleteImage));
	            	  deleteMenuItem.addSelectionListener(new CustomSelectionAdapter() );
            	  }
              } else if(ele instanceof EventSequence) {
            	  if(((EventSequence)ele).getActionList().size() > 0) {
            		  deleteMenuItem = new MenuItem(srcMenu, SWT.NONE); 
	            	  deleteMenuItem.setText("Delete");
	            	  deleteMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.deleteImage));
	            	  deleteMenuItem.addSelectionListener(new CustomSelectionAdapter() );
            	  }
              }
              
              if(srcMenu.getItemCount() >0) {
            	  new MenuItem(srcMenu, SWT.SEPARATOR);
              }
              
              if ( ( (ele instanceof EventAction) && ("true".equals(((EventAction)ele).getName()) || "false".equals(((EventAction)ele).getName())  || CALLBACK_CONSTANT.equals(((EventAction)ele).getName())))
            		  || ele instanceof EventSequence || ele instanceof EventSequenceAction) {
            	  
            	  buildActionMenu(srcMenu);
              }
              
              if(ele instanceof EventAction && !("true".equals(((EventAction)ele).getName()) || "false".equals(((EventAction)ele).getName()) || CALLBACK_CONSTANT.equals(((EventAction)ele).getName()))) {
            	  addbeforeMenuItem = new MenuItem(srcMenu, SWT.CASCADE); 
            	  addbeforeMenuItem.setText("Insert before");
            	  addbeforeMenuItem.addSelectionListener(new CustomSelectionAdapter() );

            	  Menu subMenu = new Menu(sequenceTreeViewer.getTree().getShell(), SWT.DROP_DOWN);
            	  addbeforeMenuItem.setMenu(subMenu);

            	  buildbeforeActionMenu(subMenu);

            	  addafterMenuItem = new MenuItem(srcMenu, SWT.CASCADE); 
            	  addafterMenuItem.setText("Insert after");
            	  addafterMenuItem.addSelectionListener(new CustomSelectionAdapter() );

            	  Menu sub1Menu = new Menu(sequenceTreeViewer.getTree().getShell(), SWT.DROP_DOWN);
            	  addafterMenuItem.setMenu(sub1Menu);

            	  buildafterActionMenu(sub1Menu);
            	  
            	  new MenuItem(srcMenu, SWT.SEPARATOR);
              }
              
              if ( ele instanceof EventAction && !("true".equals(((EventAction)ele).getName()) || "false".equals(((EventAction)ele).getName()) || CALLBACK_CONSTANT.equals(((EventAction)ele).getName()) )) {
            	  EventAction evtAction = (EventAction) ele;
            	  Object parent = evtAction.getParent();
            	  
            	  if (parent instanceof EventSequence) {
            		  EventSequence evtSeq = (EventSequence)parent;
            		  int idx  = evtSeq.getActionList().indexOf(evtAction);
            		  int size = evtSeq.getActionList().size();
            		  buildMoveMenu (idx, size, srcMenu);
            	  } else if (parent instanceof EventAction) {
            		  EventAction evtParent = (EventAction)parent;	  
            		  int idx  = evtParent.getActionList().indexOf(evtAction);
            		  int size = evtParent.getActionList().size();
            		  buildMoveMenu (idx, size, srcMenu);
            	  }
              }              
           } 
        });
		
	}
	
	public void createRightPane(Composite compGrp) {
		detailRightPaneGrp = new Group(compGrp, SWT.NONE);
		detailRightPaneGrp.setText("Details");
		
		detailRightPaneGrp.setLayoutData(new GridData(GridData.FILL_BOTH));
		detailGrpStackLayout = new StackLayout();
		detailRightPaneGrp.setLayout(detailGrpStackLayout);
	}
	
	private void buildActionMenu(Menu srcMenu) {
		invokeServiceSyncMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		invokeServiceSyncMenuItem.setText(actionItems[0]);
		invokeServiceSyncMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage));
		invokeServiceSyncMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		invokeServiceAsyncMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		invokeServiceAsyncMenuItem.setText(actionItems[13]);
		invokeServiceAsyncMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage));
		invokeServiceAsyncMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		if(!KUtils.isFormLoadEvent(eventType) || IPropertyNameConstants.PROP_NAME_POSTSHOW.equals(eventType)) {
			formMenuItem = new MenuItem(srcMenu, SWT.NONE); 
			formMenuItem.setText(actionItems[1]);
			formMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.formImage));
			formMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		}

		mappingMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		mappingMenuItem.setText(actionItems[10]);
		mappingMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.mapping16X16Image));
		mappingMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		alertMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		alertMenuItem.setText(actionItems[2]);
		alertMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.alertImage));
		alertMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		predecisionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		predecisionMenuItem.setText(actionItems[7]);
		predecisionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage));
		predecisionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		decisionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		decisionMenuItem.setText(actionItems[3]);
		decisionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage));
		decisionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		expressionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		expressionMenuItem.setText(actionItems[4]);
		expressionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.snippetImage));
		expressionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		functionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		functionMenuItem.setText(actionItems[5]);
		functionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.funcImage));
		functionMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		sequenceMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		sequenceMenuItem.setText(actionItems[12]);
		sequenceMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sequenceImage));
		sequenceMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		commentMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		commentMenuItem.setText(actionItems[8]);
		commentMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.commentImage));
		commentMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		variableMenuItem = new MenuItem(srcMenu, SWT.CASCADE); 
		variableMenuItem.setText(actionItems[6]);
		variableMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.globalVariablesImage));
		variableMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		Menu subMenu = new Menu(sequenceTreeViewer.getTree().getShell(), SWT.DROP_DOWN);
		variableMenuItem.setMenu(subMenu);

		simplevarMenuItem = new MenuItem(subMenu, SWT.NONE); 
		simplevarMenuItem.setText("Simple");
		simplevarMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.simpleVariableImage));
		simplevarMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		collectionvarMenuItem = new MenuItem(subMenu, SWT.NONE); 
		collectionvarMenuItem.setText("Collection");
		collectionvarMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.collectionVariableImage));
		collectionvarMenuItem.addSelectionListener(new CustomSelectionAdapter() );
	}
	
	private void buildbeforeActionMenu(Menu srcMenu) {
		beforeInvokeServiceSyncMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforeInvokeServiceSyncMenuItem.setText(actionItems[0]);
		beforeInvokeServiceSyncMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage));
		beforeInvokeServiceSyncMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforeInvokeServiceAsyncMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforeInvokeServiceAsyncMenuItem.setText(actionItems[13]);
		beforeInvokeServiceAsyncMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage));
		beforeInvokeServiceAsyncMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		if(!KUtils.isFormLoadEvent(eventType) || IPropertyNameConstants.PROP_NAME_POSTSHOW.equals(eventType)) {
			beforeformMenuItem = new MenuItem(srcMenu, SWT.NONE);
			beforeformMenuItem.setText(actionItems[1]);
			beforeformMenuItem.setText(actionItems[1]);
			beforeformMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.mapping16X16Image));
			beforeformMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		}
		
		beforemappingMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforemappingMenuItem.setText(actionItems[10]);
		beforemappingMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.mapping16X16Image));
		beforemappingMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforealertMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforealertMenuItem.setText(actionItems[2]);
		beforealertMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.alertImage));
		beforealertMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforepredecisionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforepredecisionMenuItem.setText(actionItems[7]);
		beforepredecisionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage));
		beforepredecisionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforedecisionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforedecisionMenuItem.setText(actionItems[3]);
		beforedecisionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage));
		beforedecisionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforeexpressionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforeexpressionMenuItem.setText(actionItems[4]);
		beforeexpressionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.exprImage));
		beforeexpressionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforefunctionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforefunctionMenuItem.setText(actionItems[5]);
		beforefunctionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.funcImage));
		beforefunctionMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		beforesequenceMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforesequenceMenuItem.setText(actionItems[12]);
		beforesequenceMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sequenceImage));
		beforesequenceMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforecommentMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		beforecommentMenuItem.setText(actionItems[8]);
		beforecommentMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.funcImage));
		beforecommentMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforevariableMenuItem = new MenuItem(srcMenu, SWT.CASCADE); 
		beforevariableMenuItem.setText(actionItems[6]);
		beforevariableMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.globalVariablesImage));
		beforevariableMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		Menu subMenu = new Menu(sequenceTreeViewer.getTree().getShell(), SWT.DROP_DOWN);
		beforevariableMenuItem.setMenu(subMenu);

		beforesimplevarMenuItem = new MenuItem(subMenu, SWT.NONE); 
		beforesimplevarMenuItem.setText("Simple");
		beforesimplevarMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.simpleVariableImage));
		beforesimplevarMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		beforecollectionvarMenuItem = new MenuItem(subMenu, SWT.NONE); 
		beforecollectionvarMenuItem.setText("Collection");
		beforecollectionvarMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.collectionVariableImage));
		beforecollectionvarMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		if(Clipboard.getDefault().getContents()!=null)
		{
			beforePasteMenuItem = new MenuItem(srcMenu, SWT.NONE); 
			beforePasteMenuItem.setText("Paste");
			beforePasteMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		}
	}
	
	private void buildafterActionMenu(Menu srcMenu) {
		afterInvokeServiceSyncMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afterInvokeServiceSyncMenuItem.setText(actionItems[0]);
		afterInvokeServiceSyncMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage));
		afterInvokeServiceSyncMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		afterInvokeServiceAsyncMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afterInvokeServiceAsyncMenuItem.setText(actionItems[13]);
		afterInvokeServiceAsyncMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage));
		afterInvokeServiceAsyncMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		if(!KUtils.isFormLoadEvent(eventType) || IPropertyNameConstants.PROP_NAME_POSTSHOW.equals(eventType)) {
			afterformMenuItem = new MenuItem(srcMenu, SWT.NONE); 
			afterformMenuItem.setText(actionItems[1]);
			afterformMenuItem.setText(actionItems[1]);
			afterformMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.mapping16X16Image));
			afterformMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		}
		
		aftermappingMenuItem = new MenuItem(srcMenu, SWT.NONE); 
	  	aftermappingMenuItem.setText(actionItems[10]);
	  	aftermappingMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.mapping16X16Image));
	  	aftermappingMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		afteralertMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afteralertMenuItem.setText(actionItems[2]);
		afteralertMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.alertImage));
		afteralertMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		afterpredecisionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afterpredecisionMenuItem.setText(actionItems[7]);
		afterpredecisionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage));
		afterpredecisionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		afterdecisionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afterdecisionMenuItem.setText(actionItems[3]);
		afterdecisionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage));
		afterdecisionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		afterexpressionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afterexpressionMenuItem.setText(actionItems[4]);
		afterexpressionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.exprImage));
		afterexpressionMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		afterfunctionMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		afterfunctionMenuItem.setText(actionItems[5]);
		afterfunctionMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.funcImage));
		afterfunctionMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		aftersequenceMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		aftersequenceMenuItem.setText(actionItems[12]);
		aftersequenceMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sequenceImage));
		aftersequenceMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		aftercommentMenuItem = new MenuItem(srcMenu, SWT.NONE); 
		aftercommentMenuItem.setText(actionItems[8]);
		aftercommentMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.funcImage));
		aftercommentMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		aftervariableMenuItem = new MenuItem(srcMenu, SWT.CASCADE); 
		aftervariableMenuItem.setText(actionItems[6]);
		aftervariableMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.globalVariablesImage));
		aftervariableMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		Menu subMenu = new Menu(sequenceTreeViewer.getTree().getShell(), SWT.DROP_DOWN);
		aftervariableMenuItem.setMenu(subMenu);

		aftersimplevarMenuItem = new MenuItem(subMenu, SWT.NONE); 
		aftersimplevarMenuItem.setText("Simple");
		aftersimplevarMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.simpleVariableImage));
		aftersimplevarMenuItem.addSelectionListener(new CustomSelectionAdapter() );

		aftercollectionvarMenuItem = new MenuItem(subMenu, SWT.NONE); 
		aftercollectionvarMenuItem.setText("Collection");
		aftercollectionvarMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.collectionVariableImage));
		aftercollectionvarMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		
		if(Clipboard.getDefault().getContents()!=null)
		{
			afterPasteMenuItem = new MenuItem(srcMenu, SWT.NONE); 
			afterPasteMenuItem.setText("Paste");
			afterPasteMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		}
	}
	
	private class SequenceTreeViewerSelectionListener implements ISelectionChangedListener {
		
		private EventAction prevEventAction = null;
		
    	public void selectionChanged(SelectionChangedEvent event) {
    		if(prevEventAction instanceof EventFormAction){
    			optimizeMapping((EventFormAction) prevEventAction);
    		}
    		if (event.getSelection()instanceof StructuredSelection) {
    			Object obj = ((StructuredSelection)event.getSelection()).getFirstElement();
    			selectedObject = obj;
				if (obj instanceof EventFunctionAction) {
					showFunctionTab((EventFunctionAction)obj);
				}  else if (obj instanceof EventAlertAction) {
					showAlertTab((EventAlertAction)obj);
				} else if (obj instanceof EventExpressionAction) {
					showExpressionTab((EventExpressionAction)obj);
				} else if (obj instanceof EventDecisionAction) {
					showDecisionTab((EventDecisionAction)obj);
				} else if (obj instanceof EventVariableAction) {
					EventVariableAction varAction = (EventVariableAction)obj;
					if(KUtils.COLLECTION_TYPE.equals(varAction.getVType())) {
						showCollectionVariableTab((EventVariableAction)obj);
					} else {
						showVariableTab((EventVariableAction)obj);
					}
				} else if (obj instanceof EventInvokeServiceAction) {
					EventInvokeServiceAction evtInvServAct = (EventInvokeServiceAction)obj;
					if(evtInvServAct.getIsAsynchronous()) {
						showAsyncServiceTab(evtInvServAct);						
					} else {
						showSyncServiceTab(evtInvServAct);
					}
					//showServiceTab((EventInvokeServiceAction)obj);
				} else if (obj instanceof EventFormAction) {
					showFormTab((EventFormAction)obj);
					
				} else if (obj instanceof EventPreprocessDecisionAction) {
					showPreDecisionTab((EventPreprocessDecisionAction)obj);
				} else if(obj instanceof EventCommentAction) {
					showCommentTab((EventCommentAction)obj);
				} else if(obj instanceof EventElseifAction) {
					showElseifTab((EventElseifAction)obj);
				} else if(obj instanceof EventMappingAction) {
					showMappingTab((EventMappingAction)obj);
				} else if(obj instanceof EventSequenceAction) {
					showSequenceTab((EventSequenceAction) obj);
				} else if (obj instanceof EventAction) {
					showEmptyTab();
				} else if(obj instanceof EventSequence) {
					showTopLevelTab();
				}
				
				if(obj instanceof EventAction){
					//Storing the previous event.
					prevEventAction = (EventAction)obj;
				}
				
    		}
    	}
    }
	
	private void buildMoveMenu (int idx, int size, Menu srcMenu) {
		
		if (idx == 0 && size == 1) {
			
		} else if (idx == 0) {	  
	      	moveDownMenuItem = new MenuItem(srcMenu, SWT.NONE); 
	      	moveDownMenuItem.setText("Move Down");
	      	moveDownMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.downImage));
	      	moveDownMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		} else if (idx == size -1) {
			moveUPMenuItem = new MenuItem(srcMenu, SWT.NONE); 
			moveUPMenuItem.setText("Move Up");
			moveUPMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.upImage));
			moveUPMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		} else {
			moveUPMenuItem = new MenuItem(srcMenu, SWT.NONE); 
			moveUPMenuItem.setText("Move Up");
			moveUPMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.upImage));
			moveUPMenuItem.addSelectionListener(new CustomSelectionAdapter() );
			
			moveDownMenuItem = new MenuItem(srcMenu, SWT.NONE); 
	      	moveDownMenuItem.setText("Move Down");
	      	moveDownMenuItem.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.downImage));
	      	moveDownMenuItem.addSelectionListener(new CustomSelectionAdapter() );
		}
	}
	
	
	private class SequenceTreeViewContentProvider implements ITreeContentProvider {
		
        public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
        
		public void dispose() {
		}
        
		public Object[] getElements(Object parent) {
			return getChildren(parent);
		}
        
		public Object getParent(Object child) {
			Object parent = null;
			if (child instanceof EventAction) {
				parent = ((EventAction)child).getParent();
			} else if (child instanceof EventSequence) {
				parent = ((EventSequence)child).getParent();
			} 
			return parent;
		}
        
		public Object[] getChildren(Object parent) {
			Object[] obj =  null;
			
			if (parent instanceof EventRoot) {
        		obj = new Object[] {((EventRoot)parent).getEvtSequence()};
			} else if (parent instanceof EventSequence) {
        		obj = ((EventSequence)parent).toArray();
        	} else if(parent instanceof EventSequenceAction) {
        		EventSequenceAction seqAction = (EventSequenceAction)parent;
        		EventSequence seq = getGlobalSequence(seqAction.getSequence());
        		if(seq == null) {
        			return new Object[]{};
        		} else {
        			return ((EventSequence)seq).toArray();
        		}
        	} else if (parent instanceof EventAction) {
        		obj = ((EventAction)parent).toArray();
        	}
			return obj;
		}

        public boolean hasChildren(Object parent) {
        	Object[] obj = getChildren(parent) ;
        	return obj == null ? false : obj.length > 0 ;
		}
	}
	
	private EventSequence getGlobalSequence(String seqName) {
		EventSequence seq = null;
		for(Iterator itr = globalSeqBinding.getBindingElements().iterator(); itr.hasNext();) {
			NBinding binding = (NBinding) itr.next();
			EventRoot eRoot = (EventRoot) binding.getCompoment();
			if(seqName.equals(eRoot.getEvtSequence().getSeqname())) {
				seq = eRoot.getEvtSequence();
				break;
			}
		}
		return seq;
	}
	
	private class SequenceTreeViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			if (obj instanceof EventAction) {
				EventAction evtAction = (EventAction)obj;
				switch(evtAction.getType()) {
					case 0: 
						 return evtAction.getName();
					case IEventActionTypes.FORM_ACTION: 
						 String name = "Form/Popup";
						 String id = ((EventFormAction)evtAction).getNavigateTo();
						 int platform = ((EventFormAction)evtAction).getPlatform();
						 String type = ((EventFormAction)evtAction).getDestType() == 0? "Form":"Popup";
						 
						 if (id != null && id.trim().length() > 0 && !KUtils.DEFAULT_NONE.equals(id)) {
							 if(platform == 0) {
								 name =  type+" : "+id;
							 } else {
								 name = type+" : "+MobileChannels.getMobileChannel(platform).getName()+"-"+id;
							 }
						 }
						 //if(platform != 0) return type+":"+getPlatform(platform)+"-"+name;
						 return name;
					case IEventActionTypes.ALERT_ACTION :
						 name = "Alert";
						 id = ((EventAlertAction)evtAction).getAlertID();
						 
						 if (id != null && id.trim().length() > 0 && !KUtils.DEFAULT_NONE.equals(id)) {
							 name =  id;
						 }
						 
						 return name;
					case IEventActionTypes.DECISION_ACTION :
						 name = "Decision";
						 id = ((EventDecisionAction)evtAction).getExprByProjType(projectType);
						 
						 if (id != null && id.trim().length() > 0) {
							 if(id.length() <= 10) {
								 name = name+" : "+id;
							 } else {
								 name = name+" : "+id.substring(0, 9)+"...";
							 }
						 }
						 
						 return name;
					case IEventActionTypes.VARIABLE_ACTION :
						 name = "Variable";
						 id = ((EventVariableAction)evtAction).getName();
						 /*if("collection".equals(((EventVariableAction)evtAction).getVType())) {
							 id = ((EventVariableAction)evtAction).getName();
						 } else {
							 id = ((EventVariableAction)evtAction).getName() + "=" + ((EventVariableAction)evtAction).getValue();;
						 }*/
						 
						 if (id != null && id.trim().length() > 0 && !"=".equals(id)) {
							 name =  id;
						 }
						 
						 return name;
					case IEventActionTypes.EXPRESSION_ACTION :
						 name = "Snippet";
						 id = ((EventExpressionAction)evtAction).getExprByProjType(projectType);
						 
						 if (id != null && id.trim().length() > 0) {
							 if(id.length() <= 10) {
								 name = name+" : "+id;
							 } else {
								 name = name+" : "+id.substring(0, 9)+"...";
							 }
						 }
						 
						 return name;
					case IEventActionTypes.FUNCTION_ACTION :
						 name = "Function";
						 id = ((EventFunctionAction)evtAction).getFunctionName(projectType);
						 
						 if (id != null && id.trim().length() > 0 && !KUtils.DEFAULT_NONE.equals(id)) {
							 name = "Function : "+id;
						 }
						 
						 return name;
					case IEventActionTypes.SERVICE_ACTION :
						 id  =((EventInvokeServiceAction)evtAction).getServiceId();
						 String src = ((EventInvokeServiceAction)evtAction).getSrcForm();
						 if(((EventInvokeServiceAction)evtAction).getIsAsynchronous()) {
							 if (!((EventInvokeServiceAction)evtAction).getIsOffline()) {
								 name = "Asynchronous Service: " ;
							 } else {
								 name = "Asynchronous Offline Service: " ;
							 }
						 } else {
							 name = "Synchronous Service: " ;
						 }
						 if (id != null && id.trim().length() > 0  && !KUtils.DEFAULT_NONE.equals(id)) {
							 name = name+id;
						 }
						 return name;
					case IEventActionTypes.PREPROCESS_DECISION_ACTION :
						 name = "Preprocess Decision";
						 return name;
					case IEventActionTypes.COMMENT_ACTION:
						 name = "Comment";
						 id = ((EventCommentAction)evtAction).getComment();
						 
						 if (id != null && id.trim().length() > 0) {
							 if(id.length() <= 10) {
								 name = name+" : "+id;
							 } else {
								 name = name+" : "+id.substring(0, 9)+"...";
							 }
						 }						 
						 return name;
					case IEventActionTypes.ELSEIF_ACTION :
						 name = "Condition";
						 id = ((EventElseifAction)evtAction).getExprByProjType(projectType);
						 
						 if (id != null && id.trim().length() > 0) {
							 if(id.length() <= 10) {
								 name = name+" : "+id;
							 } else {
								 name = name+" : "+id.substring(0, 9)+"...";
							 }
						 }						 
						 return name;
					case IEventActionTypes.MAPPING_ACTION: 
						 name = "Add Mappings";
						 return name;
					case IEventActionTypes.SEQUENCE_ACTION:
						id = ((EventSequenceAction)evtAction).getSequence();
						if(((EventSequenceAction)evtAction).getGlobal()) {
							name = "Global sequence: "+id;
						} else {
							name = "Local sequence";
						}
						 return name;
					default: 
						return null;
				} 
			} else if (obj instanceof EventSequence) {
				return "Action Sequence";
			}
			return null;
		}
		
		
		public Image getImage(Object obj) {
			if (obj instanceof EventAction) {
				EventAction evtAction = (EventAction)obj;
				switch(evtAction.getType()) {
					case 0: 
						 if("true".equals(evtAction.getName())) {
							 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.trueImage);
						 } else if("false".equals(evtAction.getName())) {
							 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.falseImage);
						 } else if(CALLBACK_CONSTANT.equals(evtAction.getName())) {
							 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.callbackImage);
						 } else {
							 return null;
						 }
					case IEventActionTypes.FORM_ACTION: 
						 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.formImage);
					case IEventActionTypes.ALERT_ACTION :
						 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.alertImage);
					case IEventActionTypes.DECISION_ACTION :
						 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage);
					case IEventActionTypes.VARIABLE_ACTION :
						if(KUtils.COLLECTION_TYPE.equals(((EventVariableAction)evtAction).getVType())) {
							return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.collectionVariableImage);
						}
						return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.simpleVariableImage);
					case IEventActionTypes.EXPRESSION_ACTION:
						 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.snippetImage);
					case IEventActionTypes.FUNCTION_ACTION :
						 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.funcImage);
					case IEventActionTypes.SERVICE_ACTION :
						 return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceInvokeImage);
					case IEventActionTypes.PREPROCESS_DECISION_ACTION:
						return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage);
					case IEventActionTypes.COMMENT_ACTION:
						return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.commentImage);
					case IEventActionTypes.ELSEIF_ACTION:
						return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.decisionImage);
					case IEventActionTypes.MAPPING_ACTION:
						return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.mapping16X16Image);
					case IEventActionTypes.SEQUENCE_ACTION://Global Sequence
						return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.globalSequenceImage);
					default: 
						return null;
				} 
			} else if (obj instanceof EventSequence) {
				return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sequenceImage);
			}
			return null;
		}
	}
	
	private IInputValidator validator = new IInputValidator() {
        public String isValid(String nName) {
           if (nName == null || nName.trim().length() == 0) {
        	   return "Variable name can't be empty";
           } 
           if(KUtils.checkBinding(globalBinding, nName) != null) {
        	   return "Global Variable Already exists";
           }
           
           if(!nName.matches("^[a-zA-Z][a-zA-Z0-9_]*")) {
        	   return "Variable names must start with an alphabetic character and may only contain alphabetic characters, decimal digits or underscores.";
           }
           
           if(KUtils.isVarExists(root.getEvtSequence(), nName) != null) {
        	   return "Variable Name Already Exists";
           }
           return null;
        }
    };

	private TableViewer seqParamsTableViewer;
	
    
    private void addActionByIndex(EventAction evtAct, EventAction tobeadded, int add) {
    	Object parent = evtAct.getParent();
    	int idx = 0;
		if(parent instanceof EventSequence) {
			idx = ((EventSequence)parent).getActionList().indexOf(evtAct);
			((EventSequence)parent).addActionAt(tobeadded, idx+add);
		} else {
			idx = ((EventAction)parent).getActionList().indexOf(evtAct);
			((EventAction)parent).addActionAt(tobeadded, idx+add);
		}
    }
    
    private int getIndex(ArrayList<FunctionParamValueData> pData, String name) {
    	int idx = -1;
    	for(Iterator itr = pData.iterator(); itr.hasNext();) {
    		FunctionParamValueData data = (FunctionParamValueData) itr.next();
    		if(name.equals(data.getParamName())) {
    			idx = pData.indexOf(data);
    			break;
    		}
    	}
    	return idx;
    }
	
	private class CustomSelectionAdapter extends SelectionAdapter {
        public void widgetSelected(SelectionEvent event) {
        	//1 - form navigation | 2- alert | 3- decision | 4 - variable | 5 - Expression | 6 - function | 7 - service | 8 - Preprocess decision | 9 - comment | 
        	
	        if(event.widget == sequenceMenuItem || event.widget == beforesequenceMenuItem || event.widget == aftersequenceMenuItem) {
	        	Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventSequenceAction seqAction = new EventSequenceAction();
        		//seqAction.setType(12);
        		if (event.widget == sequenceMenuItem) {
        			if (object instanceof EventAction) {
            			EventAction evtAct = (EventAction)object;
            			evtAct.addAction(seqAction);
            		} else {
            			root.getEvtSequence().addAction(seqAction);
            		}
        		} else if (event.widget == beforesequenceMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, seqAction, 0);
        			
        		} else if (event.widget == aftersequenceMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, seqAction, 1);
        		}
        		sequenceTreeViewer.reveal(seqAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(seqAction, sequenceTreeViewer.getTree().getItems());
        		showSequenceTab(seqAction);
        		markDirty();
	        } else if(event.widget == globalseqCombo) {
        		EventSequenceAction seqAction = (EventSequenceAction) globalseqCombo.getData("key");
        		seqAction.setSequence(globalseqCombo.getText());
        		seqAction.getParamData().clear();
        		if(!KUtils.DEFAULT_NONE.equals(globalseqCombo.getText())) {
        			EventSequence seq = getGlobalSequence(globalseqCombo.getText());
        			if(seq != null) {
        				ArrayList<FunctionParamValueData> globalParamList = seq.getParamData();
        				ArrayList<FunctionParamValueData> currentParamList = seqAction.getParamData();
        				for(int i=0; i< globalParamList.size(); i++) {
        					int idx = getIndex(currentParamList, globalParamList.get(i).getParamName());
        					if(idx == -1) {
        						globalParamList.get(i).setValue("",projectType);
        						currentParamList.add(i, globalParamList.get(i));
        					} else if(idx != i) {
        						FunctionParamValueData dta = currentParamList.get(idx);
        						currentParamList.add(i, dta);
        						currentParamList.remove(idx+1);
        					}
        				}
        				ArrayList<Integer> tobeRemoved = new ArrayList<Integer>();
        				for(int i=0; i< currentParamList.size(); i++) {
        					int idx = getIndex(globalParamList, currentParamList.get(i).getParamName());
        					if(idx == -1) {
        						tobeRemoved.add(i);
        					}
        				}
        				
        				for(int i = tobeRemoved.size()-1; i>=0; i--) {
        					currentParamList.remove(tobeRemoved.get(i));
        				}
        			}
        		}
        		seqParamsTableViewer.refresh(true);
        		markDirty();
        		sequenceTreeViewer.refresh(true);
        	} else if(event.widget == localRadio && localRadio.getSelection()) {
	        	EventSequenceAction seqAction = (EventSequenceAction) localRadio.getData("key");
	        	Object parent = seqAction.getParent();
	        	if(parent instanceof EventSequence) {
	        		EventSequence seqParent = (EventSequence) parent;
	        		int idx = seqParent.getActionList().indexOf(seqAction);
	        		seqParent.removeAction(seqAction);
	        		EventSequence seq = getGlobalSequence(seqAction.getSequence());
	        		if(seq != null) {
	        			for(int i=0; i<seq.getActionList().size(); i++) {
		        			EventAction subAction = seq.getActionList().get(i);
		        			seqParent.addActionAt(subAction.getCopy(), idx+i);
		        		}
	        		}
	        	} else {
	        		EventAction actionParent = (EventAction) parent;
	        		int idx = actionParent.getActionList().indexOf(seqAction);
	        		actionParent.removeAction(seqAction);
	        		EventSequence seq = getGlobalSequence(seqAction.getSequence());
	        		if(seq != null) {
	        			for(int i=0; i<seq.getActionList().size(); i++) {
		        			EventAction subAction = seq.getActionList().get(i);
		        			actionParent.addActionAt(subAction.getCopy(), idx+i);
		        		}
	        		}
	        	}
	        	sequenceTreeViewer.refresh(true);
	        	showEmptyTab();
	        	markDirty();
	        } else if(event.widget == globalRadio && globalRadio.getSelection()) {
	        	globalseqCombo.setEnabled(true);
	        	EventSequenceAction seqAction = (EventSequenceAction) localRadio.getData("key");
	        	seqAction.setGlobal(true);
	        	seqAction.setSequence(globalseqCombo.getText());
	        } else if(event.widget == seqFilterButton) {
	        	String txt = handleFilterButton(globalseqCombo.getText(), globalSeqList, "Service");
	        	globalseqCombo.setText(txt);
	        	EventSequenceAction seqAction = (EventSequenceAction) globalseqCombo.getData("key");
	        	seqAction.setSequence(globalseqCombo.getText());
	        	seqAction.getParamData().clear();
        		if(!KUtils.DEFAULT_NONE.equals(globalseqCombo.getText())) {
        			EventSequence seq = getGlobalSequence(globalseqCombo.getText());
        			if(seq != null) {
        				ArrayList<FunctionParamValueData> globalParamList = seq.getParamData();
        				
        				ArrayList<FunctionParamValueData> currentParamList = seqAction.getParamData();
        				
        				for(int i=0; i< globalParamList.size(); i++) {
        					int idx = getIndex(currentParamList, globalParamList.get(i).getParamName());
        					if(idx == -1) {
        						globalParamList.get(i).setValue("",projectType);
        						currentParamList.add(i, globalParamList.get(i));
        					} else if(idx != i) {
        						FunctionParamValueData dta = currentParamList.get(idx);
        						currentParamList.add(i, dta);
        						currentParamList.remove(idx+1);
        					}
        				}
        				
        				ArrayList<Integer> tobeRemoved = new ArrayList<Integer>();
        				for(int i=0; i< currentParamList.size(); i++) {
        					int idx = getIndex(globalParamList, currentParamList.get(i).getParamName());
        					if(idx == -1) {
        						tobeRemoved.add(i);
        					}
        				}
        				
        				for(int i = tobeRemoved.size()-1; i>=0; i--) {
        					currentParamList.remove(tobeRemoved.get(i));
        				}
        			}
        		}
    			markDirty();
    			seqParamsTableViewer.refresh(true);
    			sequenceTreeViewer.refresh(true);
	        } else if(event.widget == elseifMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventElseifAction elseifAction = new EventElseifAction();
        		//elseifAction.setType(10);
        		if (object instanceof EventAction) {
        			EventAction evtAct = (EventAction)object;
        			evtAct.getActionList().add(evtAct.getActionList().size()-1,elseifAction);
        			elseifAction.setParent(evtAct);
        		}
        		sequenceTreeViewer.reveal(elseifAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(elseifAction, sequenceTreeViewer.getTree().getItems());
        		showElseifTab(elseifAction);
        		markDirty();
        	} else if (event.widget == invokeServiceSyncMenuItem || event.widget == beforeInvokeServiceSyncMenuItem ||event.widget == afterInvokeServiceSyncMenuItem
        		/*	|| event.widget == invokeServiceAsyncMenuItem */) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventInvokeServiceAction evtInvokeServiceAction = new EventInvokeServiceAction();
        		evtInvokeServiceAction.setIsAsynchronous(false);
        		//evtInvokeServiceAction.setType(7);
        		evtInvokeServiceAction.setSrcForm(formName);
        		if (event.widget == invokeServiceSyncMenuItem) {
        			//evtInvokeServiceAction.setIsSynchronous(true);
        			if(object instanceof EventSequenceAction) {
        				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
        				if(seq != null) {
        					seq.addAction(evtInvokeServiceAction);
        				}
        			} else if (object instanceof EventAction) {
            			EventAction evtAct = (EventAction)object;
            			evtAct.addAction(evtInvokeServiceAction);
            		} else {
            			root.getEvtSequence().addAction(evtInvokeServiceAction);
            		}
        		} else if (event.widget == beforeInvokeServiceSyncMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtInvokeServiceAction, 0);
        			
        		} else if (event.widget == afterInvokeServiceSyncMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtInvokeServiceAction, 1);
        		}        		
        		
        		sequenceTreeViewer.reveal(evtInvokeServiceAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtInvokeServiceAction, sequenceTreeViewer.getTree().getItems());
        		showSyncServiceTab(evtInvokeServiceAction);
        		markDirty();
        	} else if (event.widget == invokeServiceAsyncMenuItem || event.widget == beforeInvokeServiceAsyncMenuItem ||event.widget == afterInvokeServiceAsyncMenuItem
        		/*	|| event.widget == invokeServiceAsyncMenuItem */) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventInvokeServiceAction evtInvokeServiceAction = new EventInvokeServiceAction();
        		evtInvokeServiceAction.setIsAsynchronous(true);
        		//evtInvokeServiceAction.setType(7);
        		EventAction eventAction = new EventAction();
        		eventAction.setName(CALLBACK_CONSTANT);
        		evtInvokeServiceAction.addAction(eventAction);
        		evtInvokeServiceAction.setSrcForm(formName);
        		if (event.widget == invokeServiceAsyncMenuItem) {
        			//evtInvokeServiceAction.setIsSynchronous(true);
        			if(object instanceof EventSequenceAction) {
        				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
        				if(seq != null) {
        					seq.addAction(evtInvokeServiceAction);
        				}
        			} else if (object instanceof EventAction) {
            			EventAction evtAct = (EventAction)object;
            			evtAct.addAction(evtInvokeServiceAction);
            		} else {
            			root.getEvtSequence().addAction(evtInvokeServiceAction);
            		}
        		} else if (event.widget == beforeInvokeServiceAsyncMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtInvokeServiceAction, 0);
        			
        		} else if (event.widget == afterInvokeServiceAsyncMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtInvokeServiceAction, 1);
        		}        		
        		
        		sequenceTreeViewer.reveal(evtInvokeServiceAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtInvokeServiceAction, sequenceTreeViewer.getTree().getItems());
        		showAsyncServiceTab(evtInvokeServiceAction);
        		markDirty();
        	} else if (event.widget == formMenuItem || event.widget == beforeformMenuItem || event.widget == afterformMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventFormAction evtFormAction = new EventFormAction();
        		//evtFormAction.setType(1);
        		evtFormAction.setNavigateFrom(formName);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtFormAction);
    				}
    			} else if (event.widget == formMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtFormAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtFormAction);
	        		}
        		} else if (event.widget == beforeformMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtFormAction, 0);
        		} else if (event.widget == afterformMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtFormAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtFormAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtFormAction, sequenceTreeViewer.getTree().getItems());
        		showFormTab(evtFormAction);
        		markDirty();
        	} else if (event.widget == mappingMenuItem || event.widget == beforemappingMenuItem || event.widget == aftermappingMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventMappingAction evtMappingAction = new EventMappingAction();
        		//evtMappingAction.setType(11);
        		
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtMappingAction);
    				}
    			} else if (event.widget == mappingMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtMappingAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtMappingAction);
	        		}
        		} else if (event.widget == beforemappingMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtMappingAction, 0);
        		} else if (event.widget == aftermappingMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtMappingAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtMappingAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtMappingAction, sequenceTreeViewer.getTree().getItems());
        		showMappingTab(evtMappingAction);
        		markDirty();
        	} else if (event.widget == alertMenuItem || event.widget == beforealertMenuItem || event.widget == afteralertMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventAlertAction evtAlertAction = new EventAlertAction();
        		//evtAlertAction.setType(2);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtAlertAction);
    				}
    			} else if (event.widget == alertMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtAlertAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtAlertAction);
	        		}
        		} else if (event.widget == beforealertMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtAlertAction, 0);
        		} else if (event.widget == afteralertMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtAlertAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtAlertAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtAlertAction, sequenceTreeViewer.getTree().getItems());
        		showAlertTab(evtAlertAction);
        		markDirty();
        	} else if (event.widget == expressionMenuItem || event.widget == beforeexpressionMenuItem || event.widget == afterexpressionMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventExpressionAction evtExprAction = new EventExpressionAction();
        		//evtExprAction.setType(5);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtExprAction);
    				}
    			} else if (event.widget == expressionMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtExprAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtExprAction);
	        		}
        		} else if (event.widget == beforeexpressionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtExprAction, 0);
        		} else if (event.widget == afterexpressionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtExprAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtExprAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtExprAction, sequenceTreeViewer.getTree().getItems());
        		showExpressionTab(evtExprAction);
        		markDirty();
        	} else if (event.widget == decisionMenuItem || event.widget == beforedecisionMenuItem || event.widget == afterdecisionMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventDecisionAction evtDecisionAction = new EventDecisionAction();
        		//evtDecisionAction.setType(3);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtDecisionAction);
    				}
    			} else if (event.widget == decisionMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtDecisionAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtDecisionAction);
	        		}
        		} else if (event.widget == beforedecisionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtDecisionAction, 0);
        		} else if (event.widget == afterdecisionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtDecisionAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtDecisionAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtDecisionAction, sequenceTreeViewer.getTree().getItems());
        		showDecisionTab(evtDecisionAction);
        		markDirty();
        	} else if (event.widget == predecisionMenuItem || event.widget == beforepredecisionMenuItem || event.widget == afterpredecisionMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		EventPreprocessDecisionAction evtpreDecisionAction = new EventPreprocessDecisionAction();
        		//evtpreDecisionAction.setType(8);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtpreDecisionAction);
    				}
    			} else if (event.widget == predecisionMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtpreDecisionAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtpreDecisionAction);
	        		}
        		} else if (event.widget == beforepredecisionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtpreDecisionAction, 0);
        		} else if (event.widget == afterpredecisionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtpreDecisionAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtpreDecisionAction);
        		sequenceTreeViewer.refresh(true);
        		showSelection(evtpreDecisionAction, sequenceTreeViewer.getTree().getItems());
        		showPreDecisionTab(evtpreDecisionAction);
        		markDirty();
        	} else if (event.widget == functionMenuItem || event.widget == beforefunctionMenuItem || event.widget == afterfunctionMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventFunctionAction evtFunctionAction = new EventFunctionAction();
        		//evtFunctionAction.setType(6);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtFunctionAction);
    				}
    			} else if (event.widget == functionMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtFunctionAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtFunctionAction);
	        		}
        		} else if (event.widget == beforefunctionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtFunctionAction, 0);
        		} else if (event.widget == afterfunctionMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtFunctionAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtFunctionAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtFunctionAction, sequenceTreeViewer.getTree().getItems());
        		
        		showFunctionTab(evtFunctionAction);
        		markDirty();
        	} else if (event.widget == commentMenuItem || event.widget == beforecommentMenuItem || event.widget == aftercommentMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		
        		EventCommentAction evtCommentAction = new EventCommentAction();
        		//evtCommentAction.setType(9);
        		
        		if(object instanceof EventSequenceAction) {
    				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
    				if(seq != null) {
    					seq.addAction(evtCommentAction);
    				}
    			} else if (event.widget == commentMenuItem) {
	        		if (object instanceof EventAction) {
	        			EventAction evtAct = (EventAction)object;
	        			evtAct.addAction(evtCommentAction);
	        		} else {
	        			root.getEvtSequence().addAction(evtCommentAction);
	        		}
        		} else if (event.widget == beforecommentMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtCommentAction, 0);
        		} else if (event.widget == aftercommentMenuItem) {
        			EventAction evtAct = (EventAction)object;
        			addActionByIndex(evtAct, evtCommentAction, 1);
        		}
        		
        		sequenceTreeViewer.reveal(evtCommentAction);
        		sequenceTreeViewer.refresh(true);
        		
        		showSelection(evtCommentAction, sequenceTreeViewer.getTree().getItems());
        		
        		showCommentTab(evtCommentAction);
        		markDirty();
        	}  else if (event.widget == simplevarMenuItem || event.widget == beforesimplevarMenuItem || event.widget == aftersimplevarMenuItem) {
        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
        		InputDialog inBox = new InputDialog(
        				Display.getCurrent().getActiveShell(), 
        				"Local Variable ", 
        				"Name:", 
        				"", 
        				validator);
        		inBox.open();
        		if (inBox.getReturnCode() == Dialog.OK) {
        			String nName = inBox.getValue();           		
            		EventVariableAction evtVariableAction = new EventVariableAction();
            		//evtVariableAction.setType(4);
            		evtVariableAction.setName(nName);
            		evtVariableAction.setScope(1);
            		
            		if(object instanceof EventSequenceAction) {
        				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
        				if(seq != null) {
        					seq.addAction(evtVariableAction);
        				}
        			} else if (event.widget == simplevarMenuItem) {
	            		if (object instanceof EventAction) {
	            			EventAction evtAct = (EventAction)object;
	            			evtAct.addAction(evtVariableAction);
	            		} else {
	            			root.getEvtSequence().addAction(evtVariableAction);
	            		}
            		} else if (event.widget == beforesimplevarMenuItem) {
            			EventAction evtAct = (EventAction)object;
            			addActionByIndex(evtAct, evtVariableAction, 0);
            		} else if (event.widget == aftersimplevarMenuItem) {
            			EventAction evtAct = (EventAction)object;
            			addActionByIndex(evtAct, evtVariableAction, 1);
            		}
            		
            		sequenceTreeViewer.reveal(evtVariableAction);
            		sequenceTreeViewer.refresh(true);
            		
            		showSelection(evtVariableAction, sequenceTreeViewer.getTree().getItems());
            		
            		showVariableTab(evtVariableAction);
            		markDirty();
        		}        		
        	} else if (event.widget == collectionvarMenuItem || event.widget == beforecollectionvarMenuItem || event.widget == aftercollectionvarMenuItem) {
        		
        		//write code to take collection name & scope
        		
        		AddVariableWindow win = new AddVariableWindow("Add Local Variable", "", "string", projName);
        		win.setVarMode(2);
        		win.setCheckVar(true);
        		win.setEvtSequence(root.getEvtSequence());
        		win.setGlobalBinding(globalBinding);
        		win.open();
        		if(win.getState() == 1) {
        			NBinding varBinding = new NBinding();
        			varBinding.setCompType(KUtils.VAR_TYPE);
        			NBinding globalBind = new NBinding();
        			globalBind.setCompoment(win.getVarName());
        			globalBind.setCompType(KUtils.COLLECTION_TYPE);
        			globalBind.setScope(1);
        			globalBind.setParent(varBinding);
        			varBinding.addChild(globalBind);
        			
        			Dataset dset = new Dataset();
        			dset.setId(win.getVarName());
        			
        			Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
            		
            		EventVariableAction evtVariableAction = new EventVariableAction();
            		//evtVariableAction.setType(4);
            		evtVariableAction.setVarBinding(varBinding);
            		evtVariableAction.setDset(dset);
            		evtVariableAction.setVType(KUtils.COLLECTION_TYPE);
            		evtVariableAction.setScope(1);
            		evtVariableAction.setName(win.getVarName());
            		
            		if(object instanceof EventSequenceAction) {
        				EventSequence seq = getGlobalSequence(((EventSequenceAction)object).getSequence());
        				if(seq != null) {
        					seq.addAction(evtVariableAction);
        				}
        			} else if (event.widget == collectionvarMenuItem) {
	            		if (object instanceof EventAction) {
	            			EventAction evtAct = (EventAction)object;
	            			evtAct.addAction(evtVariableAction);
	            		} else {
	            			root.getEvtSequence().addAction(evtVariableAction);
	            		}
            		} else if (event.widget == beforecollectionvarMenuItem) {
            			EventAction evtAct = (EventAction)object;
            			addActionByIndex(evtAct, evtVariableAction, 0);
            		} else if (event.widget == aftercollectionvarMenuItem) {
            			EventAction evtAct = (EventAction)object;
            			addActionByIndex(evtAct, evtVariableAction, 1);
            		}
            		
            		sequenceTreeViewer.reveal(evtVariableAction);
            		sequenceTreeViewer.refresh(true);
            		
            		showSelection(evtVariableAction, sequenceTreeViewer.getTree().getItems());
            		
            		showCollectionVariableTab(evtVariableAction);
            		markDirty();
        		}
        	} else if (event.widget == deleteMenuItem) {
        		Object selobj = sequenceTreeViewer.getTree().getSelection()[0].getData();
        		if(selobj instanceof EventAction) {
	        		EventAction evtAction = (EventAction)selobj;
	        		Object obj = evtAction.getParent();
	        		
	        		if (obj instanceof EventAction) {
	        			((EventAction)obj).removeAction(evtAction);
	        		} else if (obj instanceof EventSequence) {
	        			((EventSequence)obj).removeAction(evtAction);
	        		}
        		} else if(selobj instanceof EventSequence) {
        			((EventSequence)selobj).getActionList().clear();
        		}
        		showEmptyTab();
        		sequenceTreeViewer.refresh(true);
        		markDirty();
	        } else if(event.widget == copyMenuItem) {
	        	Object obj = sequenceTreeViewer.getTree().getSelection()[0].getData();
	        	if(obj instanceof EventAction) {
	        		Clipboard.getDefault().setContents(((EventAction)obj).getCopy());
	        	} else if(obj instanceof EventSequence) {
	        		Clipboard.getDefault().setContents(((EventSequence)obj).getCopy());
	        	}
	        	
	        } else if(event.widget == pasteMenuItem || event.widget == beforePasteMenuItem || event.widget==afterPasteMenuItem) {
	        	Object clipobj = Clipboard.getDefault().getContents();
	        	if(event.widget==pasteMenuItem)
	        	{
	        	if(clipobj instanceof EventAction) {
	        		Object evtobj = sequenceTreeViewer.getTree().getSelection()[0].getData();
	        		EventAction tobePastedAction = (EventAction)clipobj;
	        		EventAction evtAction = null;
	        		if(evtobj instanceof EventAction) {
	        			evtAction = (EventAction)evtobj;
	        			if("true".equals(tobePastedAction.getName()) || "false".equals(tobePastedAction.getName())) {
		        			for(Iterator itr = tobePastedAction.getActionList().iterator(); itr.hasNext();) {
		        				EventAction action = (EventAction)itr.next();
		        				evtAction.addAction(action);
		        			}
		        			//tobePastedAction = tobePastedAction.getActionList().get(0);
		        		} else {
		        			evtAction.addAction(tobePastedAction);
		        		}
		        		//sequenceTreeViewer.reveal(tobePastedAction);
		        		
		        		//showSelection(tobePastedAction, sequenceTreeViewer.getTree().getItems());
	        		} else if(evtobj instanceof EventSequence) {
	        			if("true".equals(tobePastedAction.getName()) || "false".equals(tobePastedAction.getName())) {
		        			for(Iterator itr = tobePastedAction.getActionList().iterator(); itr.hasNext();) {
		        				EventAction action = (EventAction)itr.next();
		        				root.getEvtSequence().addAction(action);
		        			}
		        			//tobePastedAction = tobePastedAction.getActionList().get(0);
		        		} else {
		        			root.getEvtSequence().addAction(tobePastedAction);
		        		}
		        		//sequenceTreeViewer.reveal(tobePastedAction);
		        		
		        		//showSelection(tobePastedAction, sequenceTreeViewer.getTree().getItems());
	        		}

	        	} else if(clipobj instanceof EventSequence) {
	        		Object evtobj = sequenceTreeViewer.getTree().getSelection()[0].getData();
	        		EventSequence eSeq = (EventSequence)clipobj;
	        		EventAction evtAction = null;
	        		if(evtobj instanceof EventAction) {
	        			evtAction = (EventAction)evtobj;

		        		for(Iterator itr = eSeq.getActionList().iterator();itr.hasNext();) {
		        			EventAction eAction = (EventAction)itr.next();
		        			evtAction.addAction(eAction);
		        		}
		        		//sequenceTreeViewer.reveal(tobePastedAction);
		        		
		        		//showSelection(tobePastedAction, sequenceTreeViewer.getTree().getItems());
	        		} else if(evtobj instanceof EventSequence) {

		        		for(Iterator itr = eSeq.getActionList().iterator();itr.hasNext();) {
		        			EventAction eAction = (EventAction)itr.next();
		        			root.getEvtSequence().addAction(eAction);
		        		}
		        		//sequenceTreeViewer.reveal(tobePastedAction);
		        		
		        		//showSelection(tobePastedAction, sequenceTreeViewer.getTree().getItems());
	        		}
	        		

	        	}
	        	}
	        	else if(event.widget==beforePasteMenuItem)
	        	{
	        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
	        		EventAction evtAct = (EventAction)object;
	        		if(clipobj instanceof EventSequence)
	        		{
	        			EventSequence eSeq = (EventSequence)clipobj;
	        			for(Iterator itr= eSeq.getActionList().iterator();itr.hasNext();)
	        			{
	        				EventAction eAction = (EventAction)itr.next();
	        				addActionByIndex(evtAct, eAction,0);
	        				
	        			}
	        		}
	        		else
	        		{
        			addActionByIndex(evtAct, (EventAction) clipobj, 0);
	        		}
	        	}
	        	
	        	else if(event.widget==afterPasteMenuItem)
	        	{
	        		Object object = (Object)sequenceTreeViewer.getTree().getSelection()[0].getData();
	        		EventAction evtAct = (EventAction)object;
	        		if(clipobj instanceof EventSequence)
	        		{
	        			EventSequence eSeq = (EventSequence)clipobj;
	        			for(ListIterator itr= eSeq.getActionList().listIterator(eSeq.getActionList().size());itr.hasPrevious();)
	        			{
	        				EventAction eAction = (EventAction)itr.previous();
	        				addActionByIndex(evtAct, eAction,1);
	        				
	        			}
	        		}
	        		else
	        		{
        			addActionByIndex(evtAct, (EventAction) clipobj, 1);
	        		}
	        	}
        		sequenceTreeViewer.refresh(true);
        		markDirty();
	        } else if (event.widget == moveUPMenuItem) {
	    		EventAction evtAction = (EventAction)sequenceTreeViewer.getTree().getSelection()[0].getData();
	    		Object obj = evtAction.getParent();
	    		int idx = 0;
	    		
	    		if (obj instanceof EventAction) {
        			idx = ((EventAction)obj).getActionList().indexOf(evtAction);
        			((EventAction)obj).getActionList().remove(idx);
        			((EventAction)obj).getActionList().add(idx-1, evtAction);
        		} else if (obj instanceof EventSequence) {
        			idx = ((EventSequence)obj).getActionList().indexOf(evtAction);
        			((EventSequence)obj).getActionList().remove(idx);
        			((EventSequence)obj).getActionList().add(idx-1, evtAction);
        		}
	    		
	    		refreshTab(evtAction);
	    		sequenceTreeViewer.reveal(evtAction);
	    		sequenceTreeViewer.refresh(true);
	    		showSelection(evtAction, sequenceTreeViewer.getTree().getItems());
	    		markDirty();
	    	} else if (event.widget == moveDownMenuItem) {
	    		EventAction evtAction = (EventAction)sequenceTreeViewer.getTree().getSelection()[0].getData();
	    		Object obj = evtAction.getParent();
	    		int idx = 0;
	    		
	    		if (obj instanceof EventAction) {
        			idx = ((EventAction)obj).getActionList().indexOf(evtAction);
        			((EventAction)obj).getActionList().remove(idx);
        			((EventAction)obj).getActionList().add(idx+1, evtAction);
        		} else if (obj instanceof EventSequence) {
        			idx = ((EventSequence)obj).getActionList().indexOf(evtAction);
        			((EventSequence)obj).getActionList().remove(idx);
        			((EventSequence)obj).getActionList().add(idx+1, evtAction);
        		}
	    		
	    		refreshTab(evtAction);
	    		sequenceTreeViewer.reveal(evtAction);
	    		sequenceTreeViewer.refresh(true);
	    		showSelection(evtAction, sequenceTreeViewer.getTree().getItems());
	    		markDirty();
	    	}  else if(event.widget == servFilterButton) {
        		String txt = handleFilterButton(serviceCombo.getText(), busServiceList, "Service");
        		serviceCombo.setText(txt);
        		EventInvokeServiceAction evtAction = (EventInvokeServiceAction) serviceCombo.getData("key");
        		evtAction.setServiceId(serviceCombo.getText());
    			markDirty();
    			sequenceTreeViewer.refresh(true);
    			
    			if (KUtils.DEFAULT_NONE.equals(serviceCombo.getText())) {
    				svcInputBindButton.setEnabled(false);
        		} else {
        			svcInputBindButton.setEnabled(true);
        		}
        	} else if(event.widget == variableDataTypeCombo) {
        		EventVariableAction evtAction = (EventVariableAction) variableDataTypeCombo.getData("key");
        		evtAction.setVType(variableDataTypeCombo.getText());
        		
        		if("boolean".equals(variableDataTypeCombo.getText()) && evtAction.getValueType() == 0) {
        			varstack.topControl = varBooleanComp;
        			variableBooleanCombo.setText("false");
        			evtAction.setValue("false");
	    			varComp.layout();
        		} else {
	        		varstack.topControl = varNormalComp;
	        		if("number".equals(variableDataTypeCombo.getText())) {
	        			variableValueText.setText("0");
	        		}
	        		evtAction.setValue(variableValueText.getText());
	    			varComp.layout();
        		}
        		
        		sequenceTreeViewer.refresh(true);
    			markDirty();
        	} else if(event.widget == variableValueTypeCombo) {
        		EventVariableAction evtAction = (EventVariableAction) variableValueTypeCombo.getData("key");
        		evtAction.setValueType(variableValueTypeCombo.getSelectionIndex());
        		if("boolean".equals(evtAction.getVType()) && evtAction.getValueType() == 0) {
        			varstack.topControl = varBooleanComp;
        			variableBooleanCombo.setText("false");
        			evtAction.setValue("false");
	    			varComp.layout();
        		} else {
	        		varstack.topControl = varNormalComp;
	        		if("number".equals(variableDataTypeCombo.getText())) {
	        			variableValueText.setText("0");
	        		}
	        		evtAction.setValue(variableValueText.getText());
	    			varComp.layout();
        		}
        		toggleVariableExpressionComp(variableValueTypeCombo.getSelectionIndex());
    			markDirty();
        	} else if(event.widget == variableBooleanCombo) {
        		EventVariableAction evtAction = (EventVariableAction) variableBooleanCombo.getData("key");
        		evtAction.setValue(variableBooleanCombo.getText());
        		sequenceTreeViewer.refresh(true);
        		markDirty();
        	} else if(event.widget == serviceCombo) {
        		EventInvokeServiceAction evtAction = (EventInvokeServiceAction) serviceCombo.getData("key");
        		evtAction.setServiceId(serviceCombo.getText());
    			markDirty();
    			sequenceTreeViewer.refresh(true);
    			
    			if (KUtils.DEFAULT_NONE.equals(serviceCombo.getText())) {
    				svcInputBindButton.setEnabled(false);
        		} else {
        			svcInputBindButton.setEnabled(true);
        		}
        	} else if(event.widget == blockCheck) {
        		EventInvokeServiceAction evtAction = (EventInvokeServiceAction) blockCheck.getData("key");
        		evtAction.setBlocking(blockCheck.getSelection());
    			markDirty();
    			sequenceTreeViewer.refresh(true);
        	} else if(event.widget == secureCheck) {
        		EventInvokeServiceAction evtAction = (EventInvokeServiceAction) secureCheck.getData("key");
        		evtAction.setSecure(secureCheck.getSelection());
    			markDirty();
    			sequenceTreeViewer.refresh(true);
        	} else if(event.widget == svcInputBindButton) {
        		EventInvokeServiceAction evtAction = (EventInvokeServiceAction) svcInputBindButton.getData("key");
        		KPage srcForm = KNUtils.getForm(projName,evtAction.getSrcForm(), 0, category);
        		if(srcForm == null) {
        			srcForm = pageRoot;
        		}
                NMappingWizard wizard = new NMappingWizard(srcForm, root, evtAction, eventType, widget, KUtils.MAPPING_IN, projectType, dsstoreBinding.getCopy(), i18nkeysBinding.getCopy(), skinBinding.getCopy(), themeBinding.getCopy(),projName, formList);
                CustomWizardDialog adialog = new CustomWizardDialog(Display.getCurrent().getActiveShell(), wizard);
                adialog.setMinimumPageSize(800, 600);
                if (adialog.open() == CustomWizardDialog.OK) {
                	updateLocalVariables(wizard.getLocalVariables(), evtAction);
                	sequenceTreeViewer.refresh(true);
                	if (wizard.isDirty()) markDirty();
                }
        	} else if(event.widget == outputBindButton) {
        		EventFormAction evtAction = (EventFormAction) outputBindButton.getData("key");
        		KPage srcForm = KNUtils.getForm(projName,evtAction.getNavigateFrom(), evtAction.getPlatform(), category);
        		if(srcForm == null) {
        			srcForm = pageRoot;
        		}
                NMappingWizard wizard = new NMappingWizard(srcForm, root, evtAction, eventType, widget,  KUtils.MAPPING_OUT, projectType, dsstoreBinding.getCopy(), i18nkeysBinding.getCopy(), skinBinding.getCopy(),themeBinding.getCopy(), projName, formList);
                
                CustomWizardDialog adialog = new CustomWizardDialog(Display.getCurrent().getActiveShell(), wizard);
                adialog.setMinimumPageSize(800, 600);
                if (adialog.open() == CustomWizardDialog.OK) {
                	updateLocalVariables(wizard.getLocalVariables(), evtAction);
                	sequenceTreeViewer.refresh(true);
                	if (wizard.isDirty()) markDirty();
                }
        	} else if(event.widget == mapBindButton) {
        		EventMappingAction evtAction = (EventMappingAction) mapBindButton.getData("key");
                NMappingWizard wizard = new NMappingWizard(pageRoot, root, evtAction, eventType, widget,  KUtils.MAPPING_OUT, projectType, dsstoreBinding.getCopy(), i18nkeysBinding.getCopy(), skinBinding.getCopy(),themeBinding.getCopy(), projName, formList);
                
                CustomWizardDialog adialog = new CustomWizardDialog(Display.getCurrent().getActiveShell(), wizard);
                adialog.setMinimumPageSize(800, 600);
                if (adialog.open() == CustomWizardDialog.OK) {
                	updateLocalVariables(wizard.getLocalVariables(), evtAction);
                	sequenceTreeViewer.refresh(true);
                	if (wizard.isDirty()) markDirty();
                }
        	} else if(event.widget == varClearButton) {
        		variableValueText.setText("");
        	} else if(event.widget == varTestButton) {
        		
        		if (variableValueText.getText() != null && variableValueText.getText().trim().length() > 0) {
        			String test = variableValueText.getText();

        			if("boolean".equals(variableDataTypeCombo.getText())) {
        				test =  "if ( " + test + " ) then end";
        			} else {
        				test = "local testvariable = " + test;
        			}
        			
	    			ExpressionEditorUtils.compile(false, test, projectType);
        		}
        	} else if(event.widget == makeglobal) {
        		isGlobal = makeglobal.getSelection();
        		seqnameText.setEnabled(isGlobal);
        		if(!isGlobal) {
        			emptyseqerrString = "";
        		}
        		markDirty();
        	}
        }
    }
	
	private NBindingElement getparamBinding(String name, NBinding localVariables) {
		NBindingElement binding = null;
		for(Iterator itr = localVariables.getBindingElements().iterator(); itr.hasNext();) {
			NBindingElement bEle = (NBindingElement) itr.next();
			if(bEle instanceof NBinding && name.equals(((NBinding)bEle).getCompoment())) {
				return bEle;
			} else if(name.equals(bEle.getAttr())) {
				return bEle;
			}
		}
		return binding;
	}
	
	private void updateLocalVariables(NBinding localVariables, EventAction evtAction) {
    	if(localVariables != null) {
    		EventSequence seq = KUtils.getEventSequence(evtAction);
    		if(seq != null) {
    			ArrayList<FunctionParamValueData> paramData = seq.getParamData();
    			for(Iterator itr = paramData.iterator(); itr.hasNext();) {
    				String pName = ((FunctionParamValueData)itr.next()).getParamName();
    				NBindingElement pEle = getparamBinding(pName, localVariables);
    				if(pEle != null) {
    					localVariables.removeChild(pEle);
    				}
    			}
    		}
    		
    		EventAction asyncCallBackSequence = KUtils.getAsyncCallBackSequence(evtAction);
    		if(asyncCallBackSequence != null) {
    			NBindingElement pEle = getparamBinding("status", localVariables);
				if(pEle != null) {
					localVariables.removeChild(pEle);
				}
    		}
    		
    		Object parent = evtAction.getParent();
    		for(Iterator itr = localVariables.getBindingElements().iterator(); itr.hasNext();) {
    			Object obj = itr.next();
    			if(obj instanceof NBinding) {
    				EventVariableAction varAction = KUtils.isVarExists(root.getEvtSequence(), ((NBinding)obj).getCompoment().toString());
    				if(varAction == null) {
    					EventVariableAction eAction = new EventVariableAction();
    					eAction.setType(4);
    					eAction.setName(((NBinding)obj).getCompoment().toString());
    					eAction.setVType(KUtils.COLLECTION_TYPE);
    					eAction.setScope(1);
    					NBinding varBinding = new NBinding();
            			varBinding.setCompType(KUtils.VAR_TYPE);
            			varBinding.addChild((NBinding)obj);
            			((NBinding)obj).setParent(varBinding);
            			eAction.setVarBinding(varBinding);
            			if(parent instanceof EventAction) {
    						int idx = ((EventAction)parent).getActionList().indexOf(evtAction);
    						if(idx == 0) {
    							((EventAction)parent).getActionList().add(0, eAction);
    							eAction.setParent(((EventAction)parent));
    						} else {
    							((EventAction)parent).getActionList().add(idx, eAction);
    							eAction.setParent(((EventAction)parent));
    						}
    					} else {
    						int idx = ((EventSequence)parent).getActionList().indexOf(evtAction);
    						if(idx == 0) {
    							root.getEvtSequence().getActionList().add(0, eAction);
    							eAction.setParent(root.getEvtSequence());
    						} else {
    							root.getEvtSequence().getActionList().add(idx, eAction);
    							eAction.setParent(root.getEvtSequence());
    						}
    					}
            			markDirty();
            			varAction = eAction;
    				}
    				validateVariable(varAction, (NBinding)obj);
    				
    			} else {
    				EventVariableAction varAction = KUtils.isVarExists(root.getEvtSequence(), ((NBindingElement)obj).getAttr());
    				if(varAction == null) {
    					EventVariableAction eAction = new EventVariableAction();
    					eAction.setType(4);
    					eAction.setName(((NBindingElement)obj).getAttr());
    					eAction.setVType(((NBindingElement)obj).getAttrType());
    					eAction.setScope(1);
    					if(parent instanceof EventAction) {
    						int idx = ((EventAction)parent).getActionList().indexOf(evtAction);
    						if(idx == 0) {
    							((EventAction)parent).getActionList().add(0, eAction);
    							eAction.setParent(((EventAction)parent));
    						} else {
    							((EventAction)parent).getActionList().add(idx, eAction);
    							eAction.setParent(((EventAction)parent));
    						}
    					} else {
    						int idx = ((EventSequence)parent).getActionList().indexOf(evtAction);
    						if(idx == 0) {
    							root.getEvtSequence().getActionList().add(0, eAction);
    							eAction.setParent(root.getEvtSequence());
    						} else {
    							root.getEvtSequence().getActionList().add(idx, eAction);
    							eAction.setParent(root.getEvtSequence());
    						}
    					}
    					markDirty();
    				}
    			}
    		}
    		sequenceTreeViewer.refresh(true);
    	}
	}
	
	private void validateVariable(EventVariableAction vAction, NBinding binding) {
		NBinding cbinding = (NBinding)vAction.getVarBinding().getBindingElements().get(0);
		Dataset dataset = vAction.getDset();
		for(Iterator itr = binding.getBindingElements().iterator(); itr.hasNext();) {
			Object obj = itr.next();
			if(obj instanceof NBinding) {
				NBinding bind = (NBinding)obj;
				NBinding foundBinding = (NBinding)KUtils.checkBinding(cbinding, bind.getCompoment().toString());
				if(foundBinding == null) {
					NBinding newBinding = new NBinding();
					newBinding.setCompoment(bind.getCompoment());
					newBinding.setCompType(bind.getCompType());
					newBinding.setScope(vAction.getScope());				
					for(Iterator jtr=bind.getBindingElements().iterator(); jtr.hasNext();) {
						NBindingElement bEle = (NBindingElement)jtr.next();
						NBindingElement nEle = new NBindingElement();
						nEle.setAttr(bEle.getAttr());
						nEle.setAttrType(bEle.getAttrType());
						nEle.setScope(vAction.getScope());
						newBinding.addChild(nEle);
						nEle.setParent(newBinding);
					}
					cbinding.addChild(newBinding);
					newBinding.setParent(cbinding);
					for(int i=0; i<dataset.getRecords().size();i++) {
						Record record = dataset.getRecords().get(i);
						Dataset dset = new Dataset();
						dset.setId(bind.getCompoment().toString());
						record.setDataset(dset);
					}
					markDirty();
				} else {
					validateBinding(foundBinding, bind, dataset);
				}
			} else {
				NBindingElement bEle = (NBindingElement)obj;
				if(KUtils.checkBinding(cbinding, bEle.getAttr()) == null) {
					NBindingElement ele = new NBindingElement();
					ele.setAttr(bEle.getAttr());
					ele.setAttrType(bEle.getAttrType());
					ele.setScope(vAction.getScope());
					cbinding.addChild(ele);
					ele.setParent(cbinding);
					for(int i=0; i<dataset.getRecords().size();i++) {
						Record record = dataset.getRecords().get(i);
						Param param = new Param();
						param.setName(bEle.getAttr());
						if("boolean".equals(bEle.getAttrType())) {
							param.setValue("false");
						} else {
							param.setValue("");
						}
						record.setParam(param);
					}
					markDirty();
				}				
			}
		}
		ArrayList tobeRemoved = new ArrayList();
		
		for(Iterator itr=cbinding.getBindingElements().iterator(); itr.hasNext();) {
			Object obj = itr.next();
			if(obj instanceof NBinding) {
				NBinding bind = (NBinding)obj;
				if(KUtils.checkBinding(binding, bind.getCompoment().toString()) == null) {
					tobeRemoved.add(bind);
					for(int i=0; i<dataset.getRecords().size();i++) {
						Record record = dataset.getRecords().get(i);
						Dataset dset = record.getDatasetById(bind.getCompoment().toString());
						if(dset != null) {
							record.getDatasets().remove(dset);
						}
					}
				}
			} else {
				NBindingElement bEle = (NBindingElement)obj;
				if(KUtils.checkBinding(binding, bEle.getAttr()) == null) {
					tobeRemoved.add(bEle);
					for(int i=0; i<dataset.getRecords().size();i++) {
						Record record = dataset.getRecords().get(i);
						Param param = record.getParam(bEle.getAttr());
						if(param != null) {
							record.getParams().remove(param);
						}
					}
				}				
			}
		}
		for(Iterator itr = tobeRemoved.iterator(); itr.hasNext();) {
			Object obj = itr.next();
			cbinding.getBindingElements().remove(obj);
		}
		if(tobeRemoved.size() > 0) {
			markDirty();
		}
	}
	
	private void validateBinding(NBinding cbinding, NBinding binding, Dataset dataset) {
		for(Iterator itr = binding.getBindingElements().iterator(); itr.hasNext();) {
			Object obj = itr.next();
			if(obj instanceof NBinding) {
				
			} else {
				NBindingElement bEle = (NBindingElement)obj;
				if(KUtils.checkBinding(cbinding, bEle.getAttr()) == null) {
					NBindingElement ele = new NBindingElement();
					ele.setAttr(bEle.getAttr());
					ele.setAttrType(bEle.getAttrType());
					ele.setScope(cbinding.getScope());
					cbinding.addChild(ele);
					ele.setParent(cbinding);
					for(int i=0; i<dataset.getRecords().size();i++) {
						Record record = dataset.getRecords().get(i);
						Param param = new Param();
						param.setName(bEle.getAttr());
						if("boolean".equals(bEle.getAttrType())) {
							param.setValue("false");
						} else {
							param.setValue("");
						}
						Dataset dset = record.getDatasetById(cbinding.getCompoment().toString());
						for(int j=0; j<dset.getRecords().size(); j++) {
							Record rec = dset.getRecords().get(j);
							rec.setParam(param);
						}						
					}
					markDirty();
				}
			}
		}
		
		ArrayList tobeRemoved = new ArrayList();
		
		for(Iterator itr = cbinding.getBindingElements().iterator(); itr.hasNext();) {
			Object obj = itr.next();
			if(obj instanceof NBinding) {
				
			} else {
				NBindingElement bEle = (NBindingElement)obj;
				if(KUtils.checkBinding(binding, bEle.getAttr()) == null) {
					tobeRemoved.add(bEle);
					for(int i=0; i<dataset.getRecords().size();i++) {
						Record record = dataset.getRecords().get(i);
						Dataset dset = record.getDatasetById(cbinding.getCompoment().toString());
						for(int j=0; j<dset.getRecords().size(); j++) {
							Record rec = dset.getRecords().get(j);
							Param param = record.getParam(bEle.getAttr());
							rec.getParams().remove(param);
						}	
					}
				}				
			}
		}
		for(Iterator itr = tobeRemoved.iterator(); itr.hasNext();) {
			Object obj = itr.next();
			cbinding.getBindingElements().remove(obj);
		}
		if(tobeRemoved.size() >0) {
			markDirty();
		}
	}
	
	private void showSelection(EventAction eventAction, TreeItem [] items) {
		if (items != null && items.length > 0) {
			int idx = 0;
			Object obj = null;

			for (; idx < items.length; idx++) {
				obj = items[idx].getData();

				if (obj instanceof EventSequence) {
					showSelection(eventAction, items[idx].getItems());
				}

				if (obj == eventAction) {
					sequenceTreeViewer.getTree().setSelection(items[idx]);
					break;		    			
				} else {
					showSelection(eventAction, items[idx].getItems());
				}
			}
		}
	}
	 
	private String handleFilterButton(String sel, List<String> namesList, String type) {
    	String result = sel;
    	FilterListWindow win = new FilterListWindow(getShell(), "Select ", namesList, type);
    	win.setSelectedItem(sel);
    	int open = win.open();
    	if(open == Window.OK) {
    		result = win.getSelectedItem();
    	}
    	return result;
    }
	
	private void showSequenceTab(EventSequenceAction seqAction) {
		Composite seqGrp = new Composite(detailRightPaneGrp, SWT.NULL);
		detailRightPaneGrp.setText("Invoke Global Sequence");
		GridLayout layout = new GridLayout(3, false);
		seqGrp.setLayout(layout);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		seqGrp.setLayoutData(data);
		
		Composite radioComp = new Composite(seqGrp, SWT.NONE);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 3;
		radioComp.setLayoutData(data);
		layout = new GridLayout(2, false);
		radioComp.setLayout(layout);
		
		globalRadio = new Button(radioComp,SWT.RADIO);
		globalRadio.setText("Use as global");
		data = new GridData(GridData.FILL_HORIZONTAL);
		globalRadio.setLayoutData(data);
		globalRadio.setData("key", seqAction);
		globalRadio.addSelectionListener(new CustomSelectionAdapter());
		
		localRadio = new Button(radioComp,SWT.RADIO);
		localRadio.setText("Use as local");
		data = new GridData(GridData.FILL_HORIZONTAL);
		localRadio.setLayoutData(data);
		localRadio.setData("key", seqAction);
		localRadio.addSelectionListener(new CustomSelectionAdapter());

		Label seqLabel = new Label(seqGrp, SWT.NONE);
		seqLabel.setText("Sequence: ");
		
		globalseqCombo = new Combo(seqGrp, SWT.SINGLE | SWT.READ_ONLY);
		globalseqCombo.setItems(globalSeqList.toArray(new String[globalSeqList.size()]));
		data = new GridData(GridData.FILL_HORIZONTAL);
		globalseqCombo.setLayoutData(data);
		globalseqCombo.setData("key", seqAction);
		globalseqCombo.addSelectionListener(new CustomSelectionAdapter());
		
		globalseqCombo.setText(seqAction.getSequence());
		
		seqFilterButton = new Button(seqGrp, SWT.PUSH);
		seqFilterButton.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.searchImage));
		seqFilterButton.setToolTipText("Search");
		data = new GridData();
		data.widthHint = 35;
		seqFilterButton.setLayoutData(data);
		seqFilterButton.addSelectionListener(new CustomSelectionAdapter());
		
		if(seqAction.getGlobal()) {
			globalseqCombo.setEnabled(true);
		} else {
			globalseqCombo.setText("None");
			globalseqCombo.setEnabled(false);
		}
		
		globalRadio.setSelection(seqAction.getGlobal());
		localRadio.setSelection(!seqAction.getGlobal());
		
		Group paramGroup = new Group(seqGrp, SWT.NONE);
		paramGroup.setText("Input parameters");
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		paramGroup.setLayoutData(data);
		layout = new GridLayout();
		layout.numColumns = 2;
		paramGroup.setLayout(layout);
		
		Table table = new Table(paramGroup, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION | SWT.V_SCROLL);
		
		seqParamsTableViewer = new TableViewer(table);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		data.heightHint = 80;
		seqParamsTableViewer.getTable().setLayoutData(data);
		seqParamsTableViewer.setData("key", root.getEvtSequence());
		
		TableViewerColumn col1 = new TableViewerColumn(seqParamsTableViewer, SWT.NONE);
		col1.getColumn().setText("Param name");
		col1.getColumn().setWidth(100);
		
		TableViewerColumn col2 = new TableViewerColumn(seqParamsTableViewer, SWT.NONE);
		col2.getColumn().setText("Value");
		col2.getColumn().setWidth(100);
		col2.setEditingSupport(new ParamValueEditing(seqParamsTableViewer,seqAction));
		
		seqParamsTableViewer.setContentProvider(new IStructuredContentProvider() {
			public Object[] getElements(Object input) {
				if (input instanceof ArrayList) {
					ArrayList list = (ArrayList) input;
					return list.toArray(new FunctionParamValueData[list.size()]);
				}
				return null;
			}
			
			public void dispose() {}
			
			public void inputChanged (Viewer viewer, Object obj, Object obj1) {	}
		});
		//seqParamsTableViewer.setContentProvider(new ArrayContentProvider());
		seqParamsTableViewer.setLabelProvider(new TableLabelProvider());
		seqParamsTableViewer.getTable().setHeaderVisible(true);
		seqParamsTableViewer.getTable().setLinesVisible(true);
		
		EventSequence seq = getGlobalSequence(globalseqCombo.getText());
		if(seq != null) {
			ArrayList<FunctionParamValueData> globalParamList = seq.getParamData();
			
			ArrayList<FunctionParamValueData> currentParamList = seqAction.getParamData();
			
			boolean changed = false;
			for(int i=0; i< globalParamList.size(); i++) {
				int idx = getIndex(currentParamList, globalParamList.get(i).getParamName());
				if(idx == -1) {
					globalParamList.get(i).setValue("", projectType);
					currentParamList.add(i, globalParamList.get(i));
					changed = true;
				} else if(idx != i) {
					FunctionParamValueData dta = currentParamList.get(idx);
					currentParamList.add(i, dta);
					currentParamList.remove(idx+1);
					changed = true;
				}
			}
			
			ArrayList<Integer> tobeRemoved = new ArrayList<Integer>();
			for(int i=0; i< currentParamList.size(); i++) {
				int idx = getIndex(globalParamList, currentParamList.get(i).getParamName());
				if(idx == -1) {
					tobeRemoved.add(i);
				}
			}
			
			for(int i = tobeRemoved.size()-1; i>=0; i--) {
				currentParamList.remove(tobeRemoved.get(i).intValue());
				changed = true;
			}
			//seqAction.setParamData(currentParamList);
			if(changed) markDirty();
		}
		
		seqParamsTableViewer.setInput(seqAction.getParamData());
		//attachCellEditors(seqParamsTableViewer, table);
		detailGrpStackLayout.topControl = seqGrp;
		detailRightPaneGrp.layout();
	}
	

	private void showFunctionTab(EventFunctionAction fnAction) {
	 	//Composite fnGroup = new Composite(detailRightPaneGrp, SWT.NONE);
	 	FunctionComposite fnGroup = new FunctionComposite(detailRightPaneGrp, SWT.NONE,fnAction,configuration);
	 	fnGroup.addUpdateStatusListener(this);
	 	detailRightPaneGrp.setText("User Defined Functions");
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		fnGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		fnGroup.setLayoutData(data);
	
		detailGrpStackLayout.topControl = fnGroup;
		selectedComposite = fnGroup;
		detailRightPaneGrp.layout();
	}
	
	
	
	private void showExpressionTab(EventExpressionAction exprAction) {
		ExpressionComposite exprGroup = new ExpressionComposite(detailRightPaneGrp, SWT.NONE,exprAction,configuration);
		exprGroup.addUpdateStatusListener(this);
	 	//Composite exprGroup = new Composite(detailRightPaneGrp, SWT.NONE);
	 	detailRightPaneGrp.setText("Enter Snippet");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		exprGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		exprGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = exprGroup;
		selectedComposite = exprGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showCommentTab(EventCommentAction commentAction) {
		CommentComposite cmtGroup = new CommentComposite(detailRightPaneGrp,SWT.NONE,commentAction);
	 	detailRightPaneGrp.setText("Enter Comment");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		cmtGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		cmtGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = cmtGroup;
		selectedComposite = cmtGroup;
		detailRightPaneGrp.layout();		 	
	}
	
	private void showDecisionTab(EventDecisionAction decisionAction) {
		DecisionComposite decisionGroup = new DecisionComposite(detailRightPaneGrp, SWT.NONE,decisionAction,configuration);
	 	//Composite decisionGroup = new Composite(detailRightPaneGrp, SWT.NONE);
		decisionGroup.addUpdateStatusListener(this);
	 	detailRightPaneGrp.setText("Enter expression which should evaluate to true/false");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		decisionGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		decisionGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = decisionGroup;
		selectedComposite = decisionGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showElseifTab(EventElseifAction elseifAction) {
	 	//Composite decisionGroup = new Composite(detailRightPaneGrp, SWT.NONE);
		ElseIfConditionComposite decisionGroup = new ElseIfConditionComposite(detailRightPaneGrp, SWT.NONE,elseifAction,configuration);
		decisionGroup.addUpdateStatusListener(this);
	 	detailRightPaneGrp.setText("Enter expression which should evaluate to true");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		decisionGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		decisionGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = decisionGroup;
		selectedComposite = decisionGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showPreDecisionTab(EventPreprocessDecisionAction predecisionAction) {
		PreDecisionComposite predecisionGroup = new PreDecisionComposite (detailRightPaneGrp, predecisionAction,configuration,category,projName);
		//Composite predecisionGroup = new Composite(detailRightPaneGrp, SWT.NONE);
	 	detailRightPaneGrp.setText("Select platforms");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		predecisionGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		predecisionGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = predecisionGroup;
		selectedComposite = predecisionGroup;
		detailRightPaneGrp.layout();
	}

	private void showAlertTab(EventAlertAction alertAction) {
		//Composite alertComp = new Composite(detailRightPaneGrp, SWT.NONE);
		AlertComposite alertComp = new AlertComposite(detailRightPaneGrp, SWT.NONE,alertAction,configuration);
		GridData data = new GridData(GridData.FILL_BOTH);
		GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        alertComp.setLayout(layout);
        alertComp.setLayoutData(data);
        detailGrpStackLayout.topControl = alertComp;
        selectedComposite = alertComp;
        detailRightPaneGrp.layout();
	}
	
	private void showVariableTab(EventVariableAction variableAction) {
		//Composite variableGroup = new Composite(detailRightPaneGrp, SWT.NONE);
		SimpleVariableComposite variableGroup = new SimpleVariableComposite(
				detailRightPaneGrp, SWT.NONE, variableAction, configuration);
	 	detailRightPaneGrp.setText("Define Variable");
	 	variableGroup.addUpdateStatusListener(this);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		variableGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		variableGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = variableGroup;
		selectedComposite = variableGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showCollectionVariableTab(EventVariableAction variableAction) {
		CollectionVariableComposite variableGroup = new CollectionVariableComposite(detailRightPaneGrp, SWT.NONE, variableAction, configuration);
		//Composite variableGroup = new Composite(detailRightPaneGrp, SWT.NONE);
		variableGroup.addUpdateStatusListener(this);
	 	detailRightPaneGrp.setText("Define Variable");
	    detailGrpStackLayout.topControl = variableGroup;
	    selectedComposite = variableGroup;
		detailRightPaneGrp.layout();		
	}
	
	private void showEmptyTab() {
		Composite emptyGroup = new Composite(detailRightPaneGrp, SWT.NONE);
	 	detailRightPaneGrp.setText("");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		emptyGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		emptyGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = emptyGroup;
		selectedComposite = emptyGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showTopLevelTab() {
		Composite topGroup = new Composite(detailRightPaneGrp, SWT.NONE);
	 	detailRightPaneGrp.setText("Sequence");
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		topGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		topGroup.setLayoutData(data);
		
		Label spaceLabel = new Label(topGroup, SWT.NONE);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		spaceLabel.setLayoutData(data);

	    makeglobal = new Button(topGroup, SWT.CHECK);
	    makeglobal.setText("Make sequence as global");
	    data = new GridData(GridData.FILL_HORIZONTAL);
	    data.horizontalSpan = 2;
	    makeglobal.setLayoutData(data);
	    makeglobal.addSelectionListener(new CustomSelectionAdapter());
	    makeglobal.setSelection(isGlobal);
		
		Label seqLabel = new Label(topGroup, SWT.NONE);
		seqLabel.setText("Sequence Name:");
		
		seqnameText = new Text(topGroup, SWT.BORDER);
		data = new GridData(GridData.FILL_HORIZONTAL);
		seqnameText.setLayoutData(data);
		if(isGlobal) {
			seqnameText.setText(seqName);
		}
		seqnameText.addListener(SWT.Modify, NameModifyListener);
		if(makeglobal.getSelection()) {
			seqnameText.setEnabled(true);
		} else {
			seqnameText.setEnabled(false);
		}
		
		detailGrpStackLayout.topControl = topGroup;
		selectedComposite = topGroup;
		detailRightPaneGrp.layout();
	}
	
	//Blocking and Secure Button is part of synchronous service. 
	private void showSyncServiceTab(final EventInvokeServiceAction serviceAction) {
		ServiceComposite svcGroup = new ServiceComposite(detailRightPaneGrp, SWT.NONE,serviceAction,configuration);
		svcGroup.addUpdateStatusListener(this);
	 	detailRightPaneGrp.setText("Invoke Service (Synchronous)");
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		svcGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		svcGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = svcGroup;
		selectedComposite = svcGroup;
		detailRightPaneGrp.layout();
	}

	private void showAsyncServiceTab(EventInvokeServiceAction serviceAction) {
		//Composite svcGroup = new Composite(detailRightPaneGrp, SWT.NONE);
		long l1 = System.currentTimeMillis();
		ServiceComposite svcGroup = new ServiceComposite(detailRightPaneGrp, SWT.NONE,serviceAction,configuration);
	 	detailRightPaneGrp.setText("Invoke Service (Asynchronous)");
	 	long l2 = System.currentTimeMillis();
		System.out.println("Time Taken :"+(l2-l1) + " " + KConstants.MILLISECOND_TIME_UNIT);
		selectedComposite = svcGroup;
		detailGrpStackLayout.topControl = svcGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showFormTab(EventFormAction formAction) {
		FormPopupNavigationComposite frmGroup = new FormPopupNavigationComposite(detailRightPaneGrp,SWT.NONE,formAction,configuration);
		frmGroup.addUpdateStatusListener(this);
		detailRightPaneGrp.setText("Form/Popup Navigation");
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		frmGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		frmGroup.setLayoutData(data);
		detailGrpStackLayout.topControl = frmGroup;
		selectedComposite = frmGroup;
		detailRightPaneGrp.layout();
	}
	
	private void showMappingTab(EventMappingAction formAction) {
		Composite mapGroup = new Composite(detailRightPaneGrp, SWT.NONE);
		//TODO show mapping composite here only... but need to extract
		//code NMappingWizardPage
		//MappingComposite mapGroup = new MappingComposite(detailRightPaneGrp, SWT.NONE,formAction,configuration);
	 	detailRightPaneGrp.setText("Form Mappings");
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		mapGroup.setLayout(layout);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		mapGroup.setLayoutData(data);
		
		mapBindButton = new Button (mapGroup, SWT.PUSH);
		mapBindButton.setText("Open Mapping Editor");
		mapBindButton.addSelectionListener(new CustomSelectionAdapter());
		data = new GridData(GridData.FILL_HORIZONTAL);
		//data.horizontalSpan = 3;
		data.horizontalAlignment = SWT.CENTER;
		mapBindButton.setLayoutData(data);
		mapBindButton.setData("key", formAction);
		
		
		detailGrpStackLayout.topControl = mapGroup;
		selectedComposite = mapGroup;
		detailRightPaneGrp.layout();
	}

	
	private class ParamValueEditing extends EditingSupport {
		private CellEditor cellEditor;
		
		public ParamValueEditing( TableViewer viewer, EventAction evtAction) {
			super(viewer);
			cellEditor = new FunctionParamValueCellEditor(viewer.getTable(), evtAction, "Param Value", formList, userFuncList, pageRoot, eventType, widget, projName);
		}
		
		protected boolean canEdit(Object element) {
			return true;
		}
		
		protected CellEditor getCellEditor(Object element) {
			return cellEditor;
		}

		protected Object getValue(Object element) {
			if (element instanceof FunctionParamValueData) {
				FunctionParamValueData param = ((FunctionParamValueData)element);
				return param;
			}
			return null;
		}

		protected void setValue(Object element, Object value) {
			if (element instanceof FunctionParamValueData) {
				FunctionParamValueData param = (FunctionParamValueData)element;
				String val = ((FunctionParamValueData)value).getValue(projectType);
				param.setValue(val, projectType);
				param.setValType(((FunctionParamValueData)value).getValType());
			} else if (element instanceof HttpHeaderData){
				((HttpHeaderData) element).setValue(((FunctionParamValueData) value).getValue(projectType));
				((HttpHeaderData) element).setValType(((FunctionParamValueData) value).getValType());
			}
			getViewer().update(element, null);
			markDirty();
		}
	}
	
	
	private Listener NameModifyListener = new Listener() {
		public void handleEvent(Event e) {
			if (e.widget == seqnameText) {
				seqName = seqnameText.getText();
				if ("".equals(seqName)) {
					emptyseqerrString = "Sequence name can't be empty";
				} else {
					emptyseqerrString = "";
				}
				markDirty();
			}
		}
	};
	

	@Override
	protected void okPressed() {
		errString = ""; 
		globalerrString = "";
		if(emptyseqerrString == null) 
			emptyseqerrString = "";
		checkAlertMessage();
		if(selectedObject instanceof EventFormAction){
			optimizeMapping((EventFormAction) selectedObject);
		}
		if (isDirty() || actnPerfrm) {
			if(pageRoot != null) {
				updateToMappings();
			}
			actnPerfrm = false;
			//set src form for all eventForm actions and invokeservice actions
			ArrayList<EventFormAction> eFormList = new ArrayList<EventFormAction>();
			ArrayList<EventInvokeServiceAction> eSvcList = new ArrayList<EventInvokeServiceAction>();
			for(Iterator itr = root.getEvtSequence().getActionList().iterator(); itr.hasNext();) {
				EventAction action = (EventAction) itr.next();
				if(action instanceof EventFormAction) {
					eFormList.add((EventFormAction) action);
				} else if(action instanceof EventInvokeServiceAction) {
					eSvcList.add((EventInvokeServiceAction) action);
				} else if(action instanceof EventDecisionAction || action instanceof EventAlertAction) {
					getEventFormSvcList(eFormList, eSvcList, action);
				}
			}
			
			String formName = KUtils.DEFAULT_NONE;
			if(widget != null) {
				formName = widget.getRoot().getFormName();
			}
			for(Iterator itr = eFormList.iterator(); itr.hasNext();) {
				EventFormAction formAction = (EventFormAction) itr.next();
				formAction.setNavigateFrom(formName);
			}
			for(Iterator itr = eSvcList.iterator(); itr.hasNext();) {
				EventInvokeServiceAction svcAction = (EventInvokeServiceAction) itr.next();
				svcAction.setSrcForm(formName);
			}
			//Is mark as global checked??
			boolean madeGlobal = false;
			if(makeglobal.getSelection()) {
				if(seqnameText.getText().isEmpty()) {
					emptyseqerrString = "Sequence name can't be empty. Please provide a sequence name";
				} else if(!KUtils.isValidVariableName(seqnameText.getText())){
					emptyseqerrString = "Sequence name cannot start with a number and cannot contain special characters";
				} else {
					EventSequence seq = null;
					for(Iterator itr1 = globalSeqBinding.getBindingElements().iterator(); itr1.hasNext();) {
						NBinding binding = (NBinding) itr1.next();
						EventRoot eRoot = (EventRoot) binding.getCompoment();
						if(seqnameText.getText().equals(eRoot.getEvtSequence().getSeqname())) {
							seq = eRoot.getEvtSequence();
							break;
						}
					}
					if(seq == null) {
						NBinding newSeq = new NBinding();
						EventRoot eRoot = new EventRoot();
						EventSequence sequence = root.getEvtSequence();
						sequence.setSeqname(seqnameText.getText());
						eRoot.setEvtSequence(sequence);
						newSeq.setCompoment(eRoot);
						newSeq.setCompType("collection");
						globalSeqBinding.addChild(newSeq);
						newSeq.setParent(globalSeqBinding);
						madeGlobal = true;
					} else {
						globalerrString = "Global sequence with name '"+seqnameText.getText()+"' already exists. Do you want to override?";
						MessageDialog msgbox = new MessageDialog(Display.getCurrent().getActiveShell(), "Error", null, globalerrString, MessageDialog.ERROR, new String[] {"OK", "Cancel"}, 1);
						if (msgbox.open() == Dialog.OK) {
							for(Iterator itr1 = globalSeqBinding.getBindingElements().iterator(); itr1.hasNext();) {
								NBinding binding = (NBinding) itr1.next();
								EventRoot eRoot = (EventRoot) binding.getCompoment();
								if(seqnameText.getText().equals(eRoot.getEvtSequence().getSeqname())) {
									seq = eRoot.getEvtSequence();
									seq = root.getEvtSequence();
									seq.setSeqname(seqnameText.getText());
									eRoot.setEvtSequence(seq);
									binding.setCompoment(eRoot);
									break;
								}
							}
							madeGlobal = true;
							globalerrString = "";
						}
					}
				}
			}
			
			System.out.println("Writing to globals");
			KNUtils.storeSequences(globalSeqBinding, projName);
			if(madeGlobal) {
				EventSequenceAction newAction = new EventSequenceAction();
				newAction.setType(12);
				newAction.setSequence(seqnameText.getText());
				root.getEvtSequence().getActionList().clear();
				root.getEvtSequence().addAction(newAction);
			}
			
		} else {			
			root =  unModified_Root;
		}
		if(errString.length() == 0 && globalerrString.length() == 0 && emptyseqerrString.length() == 0) {
			setReturnCode(OK);
			close();
		} else {
			if(errString.length() > 0) {
				MessageDialog msgbox = new MessageDialog(Display.getCurrent().getActiveShell(), "Error", null, errString, MessageDialog.ERROR, new String[] {"OK"}, 0);
				if (msgbox.open() == Dialog.OK) {
					
				}
			} else if(emptyseqerrString.length() > 0) {
				MessageDialog msgbox = new MessageDialog(Display.getCurrent().getActiveShell(), "Error", null, emptyseqerrString, MessageDialog.ERROR, new String[] {"OK"}, 0);
				if (msgbox.open() == Dialog.OK) {
					
				}
			} 
		}		
	}
	
	@Override
	protected void cancelPressed() {
		root =  unModified_Root;
		super.cancelPressed();
	}
	
	private void getEventFormSvcList(ArrayList<EventFormAction> eFormList, ArrayList<EventInvokeServiceAction> eSvcList, EventAction evtaction) {
		for(Iterator itr = evtaction.getActionList().iterator(); itr.hasNext();) {
			EventAction action = (EventAction) itr.next();
			if(action instanceof EventFormAction) {
				eFormList.add((EventFormAction) action);
			} else if(action instanceof EventInvokeServiceAction) {
				eSvcList.add((EventInvokeServiceAction) action);
			} else if(action instanceof EventDecisionAction || action instanceof EventAlertAction|| "true".equals(action.getName()) || "false".equals(action.getName())) {
				getEventFormSvcList(eFormList, eSvcList, action);
			}
		}
	}
	
	private ArrayList<EventSequenceAction> getSeqActions() {
		ArrayList<EventSequenceAction> seqList = new ArrayList<EventSequenceAction>();
    	for(Iterator itr = root.getEvtSequence().getActionList().iterator(); itr.hasNext();) {
    		Object obj = itr.next();
    		if(obj instanceof EventSequenceAction && !KUtils.DEFAULT_NONE.equals(((EventSequenceAction)obj).getSequence())) {
    			seqList.add((EventSequenceAction)obj);
    		} else if(obj instanceof EventAlertAction || obj instanceof EventDecisionAction){
    			getEventSequenceActionList(seqList, (EventAction)obj);
    		}
    	}
		return seqList;
	}
	
	private void getEventSequenceActionList(ArrayList<EventSequenceAction> seqActionList, EventAction eAction) {
    	for(Iterator itr = eAction.getActionList().iterator(); itr.hasNext();) {
    		EventAction action = (EventAction)itr.next();
    		if(action instanceof EventSequenceAction && !KUtils.DEFAULT_NONE.equals(((EventSequenceAction)action).getSequence())) {
    			seqActionList.add((EventSequenceAction)action);
    			getEventSequenceActionList(seqActionList, action);
    		} else if(action instanceof EventAlertAction || action instanceof EventDecisionAction|| "true".equals(action.getName()) || "false".equals(action.getName())){
    			getEventSequenceActionList(seqActionList, (EventAction)action);
    		}
    	}
    }
	
	public EventRoot getEventRoot() {
		return root;
	}
	
	private void refreshTab(EventAction evtAction) {
		if (evtAction instanceof EventDecisionAction) {
			showDecisionTab((EventDecisionAction)evtAction);
		} else if (evtAction instanceof EventExpressionAction) {
			showExpressionTab((EventExpressionAction)evtAction);
		}
	}
	
	private void toggleVariableExpressionComp(int valueType) {
		if (valueType == 0 ) {
			variableExprBuilderComp.setVisible(false);
			varTestButton.setEnabled(false);
		} else {
			variableExprBuilderComp.setVisible(true);
			varTestButton.setEnabled(true);
		}
	}
	
	private void checkAlertMessage() {
		ArrayList<EventAlertAction> alertList = new ArrayList<EventAlertAction>();
		for(Iterator itr = root.getEvtSequence().getActionList().iterator(); itr.hasNext();) {
			EventAction action = (EventAction)itr.next();
			if(action instanceof EventAlertAction) {
				alertList.add((EventAlertAction)action);
				getAlertActionList(action, alertList);
			} else if(action instanceof EventDecisionAction){
				getAlertActionList(action, alertList);
			}
		}
		if(alertList.size() > 0) {
			for(Iterator itr = alertList.iterator(); itr.hasNext();) {
				EventAlertAction alertAction = (EventAlertAction)itr.next();
				if( KUtils.EMPTY_STRING.equals(alertAction.getMessage()) && KUtils.DEFAULT_NONE.equals(alertAction.getI18nMessage()) ) {
					errString = "Alert Message or I18N key is Mandatory";
					break;
				}
			}
		}
	}
	
	private void getAlertActionList(EventAction evtaction, ArrayList<EventAlertAction> alertList) {
		for(Iterator itr = evtaction.getActionList().iterator(); itr.hasNext();) {
			EventAction action = (EventAction)itr.next();
			if(action instanceof EventAlertAction) {
				alertList.add((EventAlertAction)action);
				getAlertActionList(action, alertList);
			} else if(action instanceof EventDecisionAction || "true".equals(action.getName()) || "false".equals(action.getName())){
				getAlertActionList(action, alertList);
			}
		}
	}
	
	private void updateToMappings() {
		ArrayList<EventFormAction> formActionList = new ArrayList<EventFormAction>();
    	for(Iterator itr = root.getEvtSequence().getActionList().iterator(); itr.hasNext();) {
    		Object obj = itr.next();
    		if(obj instanceof EventFormAction) {
    			formActionList.add((EventFormAction)obj);
    		} else {
    			getEventActionList(formActionList, (EventAction)obj);
    		}
    	}
    	/*if(formActionList.size() > 0) {
    		if(!isFormExists(formActionList, pageRoot.getFormName())) {
    			saveAllEditors();
    		}
    	}*/
    	List<IFile> changedfiles = new ArrayList<IFile>();
    	for(Iterator itr = formActionList.iterator(); itr.hasNext();) {
    		EventFormAction fAction = (EventFormAction)itr.next();
    		NMapping mapping = fAction.getOutputMapping();
    		ArrayList<NBindingElement> bindings = mapping.getBinding();
    		for(Iterator jtr = bindings.iterator(); jtr.hasNext();) {
    			NBinding formBinding = (NBinding) jtr.next();
    			if("Form".equals(formBinding.getObjType())) {
    				storeFormBinding(formBinding, changedfiles);
    			}
    		}
    	}
    	if(changedfiles.size() > 0) {
    		refreshEditor(changedfiles); //---remove comment when all forms changed to kl format
    	}
	}
	
	private void storeFormBinding(NBinding formBinding, List<IFile> changedfiles) {
		String formName = pageRoot.getFormName();
		try {
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projName);
			if(project != null) {
				IFolder folder = project.getFolder("forms");
				IResource [] resources = folder.members();
	
				if (resources != null && resources.length > 0) {
					String name = null;
					for (int idx = 0; idx < resources.length; idx++) {
						String ext = resources[idx].getFileExtension();
						if (("kf".equals(ext) ||"kl".equals(ext)) && !KUtils.isHidden(resources[idx])) {
							IFile file = (IFile)resources[idx];
							name = resources[idx].getName();
							name = name.substring(0, name.length()-3);
							if(!formName.equals(name) && name.equals(formBinding.getCompoment())) {
								KEditor editor = KUtils.getEditor(file);
								if(editor != null && editor.isDirty()) editor.doSave(null);
							}
						}
					}
				}
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
				folder = project.getFolder("forms");
				resources = folder.members();
				resources = KUtils.getUniqueForms(resources, folder);
				
				if (resources != null && resources.length > 0) {
					String name = null;
					for (int idx = 0; idx < resources.length; idx++) {
						String ext = resources[idx].getFileExtension();
						if (("kf".equals(ext) ||"kl".equals(ext)) && !KUtils.isHidden(resources[idx])) {
							IFile file = (IFile)resources[idx];
							name = resources[idx].getName();
							name = name.substring(0, name.length()-3);
							if(!formName.equals(name) && name.equals(formBinding.getCompoment())) {
								KPage page = KUtils.getPagewithExt(ext, file);
								ArrayList<String> toList = page.getToList();
								if(!toList.contains(formName)) {
									toList.add(formName);
									page.setToList(toList);
									IFile curform = folder.getFile(resources[idx].getName()); 
									if(!curform.isReadOnly()) {          //readonly
										KLFileManager.saveForm(curform, page);
										changedfiles.add(folder.getFile(resources[idx].getName()));
									}
								} 
							}
						}
					}
				}
			}
		} catch (Exception e) {
			KEditorPlugin.logError(e.getMessage(), e);
		}
	}
	
	private void refreshEditor(List<IFile> activefiles) {
		try {
			IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			if( activeWorkbenchWindow != null ) {
				IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
				if( activePage != null ) {
					IViewPart view = activePage.findView(NavigationView.ID);
					if(view != null) {
						NavigationView navigationView = (NavigationView)view;
				    	boolean open = false;
				    	KEditor editor = null;
				    	IFile file = null;
				    	for(Iterator itr = activefiles.iterator();itr.hasNext();) {
				    		open = false;
				    		file = (IFile)itr.next();
					    	editor =  (KEditor)navigationView.getSite().getPage().findEditor(new FileEditorInput(file));
							if (editor != null) {
								navigationView.getSite().getPage().closeEditor(editor, true);
								open = true;
							}
							if (open) {
								try {
									IDE.openEditor(navigationView.getSite().getPage(), file, true);
								} catch (PartInitException e) {
									KEditorPlugin.logError(e.getMessage(), e);
								}
							}
				    	}
					}
				}
			}			
		} catch (Exception ex) {
			KEditorPlugin.logError(ex.getMessage(), ex);
		}
    }
	
	private void getEventActionList(ArrayList<EventFormAction> formActionList, EventAction eAction) {
    	for(Iterator itr=eAction.getActionList().iterator(); itr.hasNext();) {
    		Object obj = itr.next();
    		if(obj instanceof EventFormAction) {
    			formActionList.add((EventFormAction)obj);
    		} else {
    			getEventActionList(formActionList, (EventAction)obj);
    		}
    	}
    }
	
	private void markDirty() {
		dirty = true;
	}

	public boolean isDirty() {
		return dirty;
	}
	
	private void saveState() {
		if(isDirty()) {
			if(pageRoot != null) {
				updateToMappings();
			}
		} else {
			root = unModified_Root;
		}
		setReturnCode(OK);
		close();
	}
	
	@Override
	public void updateStatus(IStatus status,boolean closeDialog) {
		getButton(Window.OK).setEnabled(true);
		sequenceTreeViewer.getControl().setEnabled(true);
		if (status.getSeverity() == IStatus.ERROR) {
			getButton(Window.OK).setEnabled(false);
			sequenceTreeViewer.getControl().setEnabled(false);
		}else if(status.getSeverity() == IStatus.OK){
			sequenceTreeViewer.refresh(true);
			dirty = true;
			if(closeDialog){
				saveState();
			}
		}
	}
	
	
	private void optimizeMapping(EventFormAction eventFormAction) {
		NBindingRoot targetRoot = null;;
		if(selectedComposite instanceof FormPopupNavigationComposite){
			FormPopupNavigationComposite cComposite = (FormPopupNavigationComposite) selectedComposite;
			targetRoot = cComposite.getTargetRoot();
		}
		if(targetRoot != null){
			NMapping mapping = eventFormAction.getOutputMapping();
			ArrayList<NBindingElement> optimizedBinding = NMappingWizard.getOptimizedBindings(targetRoot.getBindingElements());			
			mapping.setBinding(optimizedBinding);
		}
	}

	private static class TableLabelProvider extends LabelProvider implements ITableLabelProvider{

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}
		
		@Override
		public String getColumnText(Object element, int columnIndex) {
			if(element instanceof FunctionParamValueData){
				FunctionParamValueData param = (FunctionParamValueData) element;
				switch(columnIndex){
				case 0:
					return param.getParamName();
				case 1:
					return param.toString();
				}
			} else if(element instanceof HttpHeaderData){
				HttpHeaderData data = (HttpHeaderData) element;
				switch(columnIndex){
				case 0:
					return data.getName();
				case 1:
					return data.getValue();
				}
			}
			return "";
		}
	}
}
