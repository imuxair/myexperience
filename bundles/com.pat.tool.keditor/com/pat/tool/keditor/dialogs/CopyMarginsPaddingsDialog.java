package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.actions.CopyMarginsPaddingsAction;
import com.pat.tool.keditor.cache.FilterPlatformCache;
import com.pat.tool.keditor.model.IWidget;
import com.pat.tool.keditor.model.WidgetUtil.Widget;
import com.pat.tool.keditor.platformselection.PlatformSelectionComposite;
import com.pat.tool.keditor.portability.MarginPaddingPercentageComposite;
import com.pat.tool.keditor.propertyDescriptor.MarginUnit;
import com.pat.tool.keditor.utils.RendererHashMapData;
import com.pat.tool.keditor.widgets.DesktopKioskChannel;
import com.pat.tool.keditor.widgets.IMobileChannel;
import com.pat.tool.keditor.widgets.MobileChannels;
import com.pat.tool.keditor.widgets.RichClientChannel;
import com.pat.tool.keditor.widgets.TabletRichClientChannel;
import com.pat.tool.keditor.widgets.TabletSPAChannel;
import com.pat.tool.keditor.widgets.ThinClientChannel;
public class CopyMarginsPaddingsDialog extends TitleAreaDialog{
	
	private static final String RC_CHANNELS = "---" + MobileChannels.MOBILE_RICH_CLIENT + "---";
	private static final String TC_CHANNELS = "---" + MobileChannels.MOBILE_THIN_CLIENT + "---";
	private static final String TABLET_RC_CHANNELS = "---" + MobileChannels.TABLET_RICH_CLIENT + "---";
	private static final String DESKTOP_KIOSK_CHANNELS = "---" + MobileChannels.DESKTOP_KIOSK_CLIENT + "---";
	private static final String TABLET_SPA_CHANNELS = "---" + MobileChannels.TABLET_THIN_CLIENT + "---";
	private Combo platCombo;

	private Button selectWidget;
	private Button marginButton;
	private Button paddingButton;
	private PlatformSelectionComposite platSelComp;
	private Map<IMobileChannel, MarginUnit> marginMulitplierMap = new HashMap<IMobileChannel, MarginUnit>();
	private Map<IMobileChannel, MarginUnit> paddingMulitplierMap = new HashMap<IMobileChannel, MarginUnit>();
	private String projName ="";

	
	private IMobileChannel clonePlatform;
	private RendererHashMapData renderData = new RendererHashMapData(false);
	private ArrayList<IWidget> widgetsList = new ArrayList<IWidget>();

	private int propertyName;
	private Button multiplierButton;

	public CopyMarginsPaddingsDialog(Shell parentShell,String pName) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE); 
		setHelpAvailable(false);
		this.projName = pName;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Copy Margins/Paddings in Project");
		getShell().setText("Copy Margins/Paddings to Selected Platforms");
		
		Composite control = (Composite) super.createDialogArea(parent);
		
		final ScrolledComposite scrolledSplashComposite = new ScrolledComposite(control, SWT.V_SCROLL | SWT.BORDER);
		scrolledSplashComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Composite comp = new Composite(scrolledSplashComposite, SWT.NONE);
		comp.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		GridLayoutFactory.fillDefaults().numColumns(2).margins(5, 5).applyTo(comp);
		
		Group catComp = new Group(comp, SWT.NONE);
		catComp.setText("Criteria");
		GridDataFactory.fillDefaults().grab(true, false).span(2,1).applyTo(catComp);
		GridLayoutFactory.fillDefaults().numColumns(4).margins(5,5).applyTo(catComp);
		
		Label spacer = new Label(catComp, SWT.NONE);
		spacer.setText("Property:");
		
		marginButton = new Button(catComp, SWT.CHECK);
		marginButton.setText("Margins");
		GridDataFactory.swtDefaults().applyTo(marginButton);
		marginButton.setSelection(true);
		marginButton.addSelectionListener(customAdapter);
		
		paddingButton = new Button(catComp, SWT.CHECK);
		paddingButton.setText("Paddings");
		GridDataFactory.swtDefaults().applyTo(paddingButton);
		paddingButton.addSelectionListener(customAdapter);
		
		selectWidget = new Button(catComp, SWT.PUSH);
		selectWidget.setText("Select Widgets");
		GridDataFactory.swtDefaults().grab(true, false).align(SWT.RIGHT, SWT.CENTER).applyTo(selectWidget);
		selectWidget.addSelectionListener(customAdapter);
		selectWidget.setData(new ArrayList<IWidget>());
		
		Label platNameLabel = new Label(comp, SWT.NONE);
		platNameLabel.setText("Source platform: ");
		
		platCombo = new Combo(comp, SWT.DROP_DOWN | SWT.READ_ONLY);
		platCombo.setLayoutData(new GridData(SWT.FILL,SWT.BEGINNING,true,false));
		platCombo.setVisibleItemCount(10);
		platCombo.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkComboText();
				updateComp();
			}
		});
		List<IMobileChannel> disabledChannels =  FilterPlatformCache.getAllDisabledChannelsCache(projName); 
		int platformSize = 0;
		
		  if(!MobileChannels.isRichClientDisabled(projName)){
			  platformSize++;
				for(IMobileChannel platform : RichClientChannel.values()){
					if (!disabledChannels.contains(platform)) {
						 platformSize++;
					}
				}
		  }
		  if(!MobileChannels.isTabletRichClientDisabled(projName)){
			  platformSize++;
				for(IMobileChannel platform : TabletRichClientChannel.values()){
					if (!disabledChannels.contains(platform)) {
						 platformSize++;
					}
				}
		  }
		  if(!MobileChannels.isTabletSPADisabled(projName)){
			  platformSize++;
				for(IMobileChannel platform : TabletSPAChannel.values()){
					if (!disabledChannels.contains(platform)) {
						 platformSize++;
					}
				}
		  }
		 
		  if(!MobileChannels.isThinClientDisabled(projName)){
			  platformSize++;
				for(IMobileChannel platform : ThinClientChannel.values()){
					if (!disabledChannels.contains(platform)) {
						 platformSize++;
					}
				}
		  }
		  if(!MobileChannels.isDesktopKioskDisabled(projName)){
			  platformSize++;
				for(IMobileChannel platform : DesktopKioskChannel.values()){
					if (!disabledChannels.contains(platform)) {
						 platformSize++;
					}
				}
		  }
		  
		
		String[] platNames = new String[platformSize];
		RichClientChannel[] rc_platforms = RichClientChannel.values();
		TabletRichClientChannel[] trc_platforms = TabletRichClientChannel.values();
		ThinClientChannel[] tc_platforms = ThinClientChannel.values();
		DesktopKioskChannel[] desktop_platforms = DesktopKioskChannel.values();
		TabletSPAChannel[] tspa_platforms = TabletSPAChannel.values();
		int i = 0;
		
		if (!MobileChannels.isRichClientDisabled(projName)) {
		platNames[i++] = RC_CHANNELS;
			for(int j = 0 ; j < rc_platforms.length; j++){
				if(!disabledChannels.contains(rc_platforms[j])){
					platNames[i++] = rc_platforms[j].getDisplayName();
				}
			}
		}
		if (!MobileChannels.isTabletRichClientDisabled(projName)) {
				platNames[i++] = TABLET_RC_CHANNELS;
				for(int j = 0 ; j < trc_platforms.length; j++){
					if(!disabledChannels.contains(trc_platforms[j])){
						platNames[i++] = trc_platforms[j].getDisplayName();
					}
				}
		}
		if (!MobileChannels.isThinClientDisabled(projName)) {
			platNames[i++] = TC_CHANNELS;
			for(int j = 0 ; j < tc_platforms.length; j++){
				if(!disabledChannels.contains(tc_platforms[j])){
					platNames[i++] = tc_platforms[j].getDisplayName();
				}
			}
		}
		if (!MobileChannels.isDesktopKioskDisabled(projName)) {
			platNames[i++] = DESKTOP_KIOSK_CHANNELS;
			for(int j = 0 ; j < desktop_platforms.length; j++){
				if(!disabledChannels.contains(desktop_platforms[j])){
					platNames[i++] = desktop_platforms[j].getDisplayName();
				}
			}
		}
		if (!MobileChannels.isTabletSPADisabled(projName)) {
		platNames[i++] = TABLET_SPA_CHANNELS;
		for(int j = 0 ; j < tspa_platforms.length; j++){
			if(!disabledChannels.contains(tspa_platforms[j])){
				platNames[i++] = tspa_platforms[j].getDisplayName();
			}
		 }
		}
		platCombo.setItems(platNames);
		platCombo.select(0);
		checkComboText();
		
		Label targetPlatformLabel = new Label(comp, SWT.NONE);
		targetPlatformLabel.setText("Target Platforms:");
		
		RendererHashMapData rendererData = new RendererHashMapData(false);
		platSelComp = new PlatformSelectionComposite(comp, SWT.NONE, rendererData) {
			
			protected Composite createExtendedComposite(Composite parent) {
				Composite dialogsComposite = new Composite(parent, SWT.NONE);
				GridDataFactory.fillDefaults().grab(true, false).span(2, 1).applyTo(dialogsComposite);
				GridLayoutFactory.fillDefaults().numColumns(1).margins(5, 5).applyTo(dialogsComposite);

				multiplierButton = new Button(dialogsComposite, SWT.PUSH);
				multiplierButton.setText("Multiplier (%)");
				GridDataFactory.fillDefaults().grab(false, false).applyTo(multiplierButton);
				multiplierButton.addSelectionListener(customAdapter);
				multiplierButton.setEnabled(false);
				return dialogsComposite;
			};
		};
		GridDataFactory.fillDefaults().grab(true, true).span(2,1 ).applyTo(platSelComp);
		platSelComp.init();
		platSelComp.addValidationEventListener(new Listener() {
			@Override
			public void handleEvent(Event event) {
				validateAndUpdateDialog();
			}
			
		});
		
		
		updateComp();
		scrolledSplashComposite.setContent(comp);
		scrolledSplashComposite.setExpandVertical(true);
		scrolledSplashComposite.setExpandHorizontal(true);
		scrolledSplashComposite.setMinSize(comp.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		return control;
	}
	
	private void checkComboText() {
		if(platCombo.getText().equals(RC_CHANNELS) ||
				platCombo.getText().equals(TC_CHANNELS) ||
				platCombo.getText().equals(TABLET_RC_CHANNELS) ||
				MobileChannels.getMobileChannelByDisplayName(platCombo.getText())==null)
			platCombo.select(platCombo.getSelectionIndex()+1);
	}
	
	private SelectionAdapter customAdapter = new SelectionAdapter() {
        @SuppressWarnings("unchecked")
		public void widgetSelected(SelectionEvent event) {
        	if(event.widget == selectWidget) {
        		WidgetSelectionDialog wsel = new WidgetSelectionDialog(Display.getCurrent().getActiveShell());
        		wsel.setInitialSelwidgetList((ArrayList<IWidget>) selectWidget.getData());
        		int result = wsel.open();
        		if(result != Window.CANCEL) {
        			widgetsList = wsel.getWidgetList();
        			selectWidget.setData(widgetsList);
        		}
        	} else if (event.widget == multiplierButton) {
        		RendererHashMapData hashMapData = platSelComp.getUpdatedRendererHashMapData();
        		if (hashMapData.equals(new RendererHashMapData(false))) {
        			
        		} else {
        			new MarginPaddingPercentageDialog().open();
        		}
        	} 
        	validateAndUpdateDialog();
        }
	};
	
	

	
	@Override
	protected void okPressed() {
		clonePlatform = MobileChannels.getMobileChannelByDisplayName(platCombo.getText());
		if (!validateAndUpdateDialog()) {
			return;
		}
		
		if(marginButton.getSelection() && paddingButton.getSelection()) {
			propertyName = CopyMarginsPaddingsAction.PROP_ALL;
		} else if(marginButton.getSelection()){
			propertyName = CopyMarginsPaddingsAction.PROP_MARGINS;
		} else {
			propertyName = CopyMarginsPaddingsAction.PROP_PADDINGS;
		}
		super.okPressed();
	}

	private boolean validateAndUpdateDialog() {
		String errorMessage = null;
		if (widgetsList.isEmpty()) {
			errorMessage = "At least one widget should be selected";
		}

		renderData = platSelComp.getUpdatedRendererHashMapData();
		if (renderData.equals(new RendererHashMapData(false))) {
			errorMessage = "At least one target platform should be selected";
		}
		if (!(marginButton.getSelection() || paddingButton.getSelection())) {
			errorMessage = "Margins/Paddings should be selected";
		}
		setErrorMessage(errorMessage);
		boolean valid = errorMessage == null;
		getButton(IDialogConstants.OK_ID).setEnabled(valid);
		boolean enabled = !platSelComp.getUpdatedRendererHashMapData().equals(new RendererHashMapData(false))
				&& (marginButton.getSelection() || paddingButton.getSelection());
		multiplierButton.setEnabled(enabled);
		return valid;
	}

	/**
	 * Updates the Platform Selection composite
	 */
	private void updateComp() {
		IMobileChannel platform = MobileChannels.getMobileChannelByDisplayName(platCombo.getText());
		platSelComp.setSelection(new RendererHashMapData(false));
		platSelComp.updateButtonsEnabled(new RendererHashMapData(true));
		platSelComp.updateButtonsEnabled(platform, false);
		
		setMessage("Copy Margins/Paddings of "+platform.getDisplayName()+" to selected destination platforms");
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				validateAndUpdateDialog();
			}
		});
	}

	public RendererHashMapData getRenderData() {
		return renderData;
	}
	
	public ArrayList<IWidget> getWidgetsList() {
		return widgetsList;
	}

	public int getPropertyName() {
		return propertyName;
	}
	
	public IMobileChannel getClonePlatform() {
		return clonePlatform;
	}
	
	public Map<IMobileChannel, MarginUnit> getMarginMulitplierMap() {
		return marginMulitplierMap;
	};
	
	public Map<IMobileChannel, MarginUnit> getPaddingMulitplierMap() {
		return paddingMulitplierMap;
	}
	

	private class MarginPaddingPercentageDialog extends Dialog {
		private Map<IMobileChannel, MarginUnit> marginMulitplierMap;
		private Map<IMobileChannel, MarginUnit> paddingMulitplierMap;		

		protected MarginPaddingPercentageDialog() {
			super(Display.getDefault().getActiveShell());
			setShellStyle(getShellStyle() | SWT.RESIZE); 
		}
		
		@Override
		protected Control createDialogArea(Composite parent) {
			getShell().setText("Margins And Paddings Percentage");
			if (marginButton.getSelection()) {
				marginMulitplierMap = getCopy(CopyMarginsPaddingsDialog.this.marginMulitplierMap);
			} 
			if (paddingButton.getSelection()) {
				paddingMulitplierMap = getCopy(CopyMarginsPaddingsDialog.this.paddingMulitplierMap);
			}
			
			return new MarginPaddingPercentageComposite(parent, platSelComp.getUpdatedRendererHashMapData(), marginMulitplierMap, paddingMulitplierMap);
		}
		
		@Override
		protected void okPressed() {
			if (marginButton.getSelection()) {
				CopyMarginsPaddingsDialog.this.marginMulitplierMap = marginMulitplierMap;
			}
			if (paddingButton.getSelection()) {
				CopyMarginsPaddingsDialog.this.paddingMulitplierMap = paddingMulitplierMap;
			}
			super.okPressed();
		}
		
		private Map<IMobileChannel, MarginUnit> getCopy(Map<IMobileChannel, MarginUnit> multiplier) {
			Map<IMobileChannel, MarginUnit> copy = new HashMap<IMobileChannel, MarginUnit>();
			for (Entry<IMobileChannel, MarginUnit> entry : multiplier.entrySet()) {
				copy.put(entry.getKey(), entry.getValue().getCopy());
			}
			return multiplier;
		}
		
	}
	
}
