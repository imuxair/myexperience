package com.pat.tool.keditor.dialogs;

import static com.pat.tool.keditor.constants.NavigationViewConstants.FOLDER_DESKTOP;
import static com.pat.tool.keditor.constants.NavigationViewConstants.FOLDER_GRIDS;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.swt.widgets.Composite;

import com.pat.tool.keditor.cellEditors.TemplateDataCellEditor;
import com.pat.tool.keditor.constants.NavigationViewConstants;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;

public class ColumnTemplateEditingSupport extends EditingSupport {
	
	private int columnIndex;
	private TemplateDataCellEditor templateDataCellEditor;
	private TableDataDialog tableDataDialog;
	private IProject project;
	private int platform;
	private int formCategory;
	
	public ColumnTemplateEditingSupport(TableDataDialog tableDataDialog, ColumnViewer viewer, int columnIndex,
			IProject project,int platform,int formCategory) {
		super(viewer);
		this.tableDataDialog = tableDataDialog;
		this.columnIndex = columnIndex;
		this.project = project;
		this.formCategory = formCategory;
		this.platform = platform;
		templateDataCellEditor = new TemplateDataCellEditor((Composite)viewer.getControl(), project,KPage.GRID_MODE,formCategory, platform);
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		if(element instanceof ColumnData) {
			ColumnData colData = (ColumnData) element;
			if(columnIndex == TableDataDialog.COLUMN_HEADER_DATA_INDEX){
				String templateName = colData.colHeader;
				templateDataCellEditor.setTemplateName(templateName);
				return templateDataCellEditor;
			}
		}
		return null;
	}

	@Override
	protected boolean canEdit(Object element) {
		ColumnData cData = (ColumnData) element;
		if(columnIndex == TableDataDialog.COLUMN_HEADER_DATA_INDEX){
			if(cData.colHeaderType != 1){
				return false;
			}
			String templateName = cData.colHeader;
			if(templateName.isEmpty() || templateName.equals(KUtils.DEFAULT_NONE)) {
				tableDataDialog.setMessage("Please define a header template for " + "'" + cData.getId() + "'",IMessageProvider.INFORMATION);
				return false;
			} else {
				IFolder folder = project.getFolder(NavigationViewConstants.FOLDER_TEMPLATES+File.separator+ FOLDER_GRIDS + File.separator + FOLDER_DESKTOP);
				IFile templateFile = folder.getFile(KUtils.getFormNameWithExtension(templateName));
				if(!templateFile.exists()) {
					tableDataDialog.setMessage("The header template" + "'" + templateName + "'" + " does not exist",IMessageProvider.ERROR);
					return false;
				}
			}
		}
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		if(element instanceof ColumnData) {
			ColumnData colData = (ColumnData) element;
			if(columnIndex == TableDataDialog.COLUMN_HEADER_DATA_INDEX){
				Object colHeaderData = colData.getColHeaderData();
				if(colHeaderData instanceof TemplateData) {
					return colData.getColHeaderData();
				} else {
					return new TemplateData();
				}
			}
		}
		return "";
	}

	@Override
	protected void setValue(Object element, Object value) {
		if(element instanceof ColumnData) {
			ColumnData colData = (ColumnData) element;
			if(value instanceof TemplateData){
				TemplateData data = (TemplateData)value;
				colData.setColHeaderData(value);
				tableDataDialog.validatePage(true);
				tableDataDialog.updateRowsData();
				getViewer().refresh(colData);
			}
		}	

	}

}
