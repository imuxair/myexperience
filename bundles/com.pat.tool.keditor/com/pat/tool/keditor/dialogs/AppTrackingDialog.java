package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.pat.tool.keditor.utils.KUtils;

/**
 * @author KH1826
 *
 */
public class AppTrackingDialog extends Dialog {
	
	/**
	 * Table viewer
	 */
	CheckboxTableViewer tableViewer;
	
	/**
	 * Values,which should be checked in the tracking dialog
	 */
	private static String checkedValues = KUtils.EMPTY_STRING;
	
	public AppTrackingDialog(Shell parentShell, String appTrackingLevelsValues) {
		super(parentShell);
		updateCheckedValues(appTrackingLevelsValues);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		 Composite shell = (Composite) super.createDialogArea(parent);
		 Composite mainComp = new Composite(shell, SWT.NONE);
		 mainComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		 mainComp.setLayout(new GridLayout(1, false));
		 
		 createUI(mainComp);
		 return shell;
	}
	

	
	private void createUI(Composite parent){		
			
		Composite tableComp = new  Composite(parent, SWT.NONE);
		
		GridData data = new GridData(SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		
		tableComp.setLayoutData(data);
		tableComp.setLayout(layout);
		
		/// creating a swt table 
		
		Table table = new Table(tableComp, SWT.BORDER | SWT.MULTI| SWT.H_SCROLL | SWT.V_SCROLL |SWT.CHECK );
		
		data = new GridData(GridData.FILL_BOTH);
		data.widthHint = 200;
		data.heightHint = 230;
		table.setLayoutData(data);
		
		/// initialize table viewer
		
		tableViewer = new CheckboxTableViewer(table);
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		
		tableViewer.setLabelProvider(new ITableLabelProvider() {
			
			@Override
			public void removeListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isLabelProperty(Object element, String property) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void dispose() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void addListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public String getColumnText(Object element, int columnIndex) {
				// TODO Auto-generated method stub
				if( element instanceof TrackingValues){
					TrackingValues val = (TrackingValues)element;
					return val.toString();
				}
				return null;
			}
			
			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				// TODO Auto-generated method stub
				return null;
			}
		});
		
		
		tableViewer.setInput(TrackingValues.values());
		
		if( checkedValues != null && !checkedValues.isEmpty()){
			tableViewer.setCheckedElements(createCheckedElement());			
		}
		
	}
	
	/**
	 * Updating the model based on checkedValues variable, which should be used to
	 * set as checked elements in the tableViewer
	 * @return TrackingValues[] - updated elemenst array
	 */
	private TrackingValues[] createCheckedElement(){
		
		String[] valuesArray = checkedValues.split(",");		
		TrackingValues[] elements =  new TrackingValues[valuesArray.length];
		
		for (int i = 0; i < valuesArray.length; i++) {
			elements[i] = TrackingValues.valueOf(valuesArray[i]);			
		}	
		
		return elements;
		
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("App Tracking Events");
	}
	
	@Override
	protected void okPressed() {
		Object[] checkedElements = tableViewer.getCheckedElements();
		StringBuilder sb = new StringBuilder();
		
		for (Object element : checkedElements) {
			if ( element instanceof TrackingValues){
				String val = ((TrackingValues)element).toString();
				
				sb.append(val).append(",");
			}
		}
		
		updateCheckedValues( KUtils.removeCommaAtEndofString(sb.toString()));
		super.okPressed();
		
	}

	public static String getCheckedValues() {
		return checkedValues;
	}

	public void updateCheckedValues(String appTrackingValue) {
		AppTrackingDialog.checkedValues = appTrackingValue;
	}

	
	/**
	 * @author KH1826
	 * 
	 */
	private enum TrackingValues {
		
	    FormEntry, FormExit,  ServiceRequest, ServiceResponse, Error, Exception, Touch, Gesture,Orientation,HandledException, Crash;

	    
	    
	    
	}
}
