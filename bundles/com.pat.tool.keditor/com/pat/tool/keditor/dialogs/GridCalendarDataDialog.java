package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.konylabs.middleware.dataobject.Dataset;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.pat.tool.keditor.cellEditors.TemplateDataCellEditor;
import com.pat.tool.keditor.constants.MobileChannelConstants;
import com.pat.tool.keditor.mapping.model.IPropertyConstants;
import com.pat.tool.keditor.mapping.model.NBinding;
import com.pat.tool.keditor.mapping.model.NBindingElement;
import com.pat.tool.keditor.model.KDateField;
import com.pat.tool.keditor.model.KMenuBar;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.propertyDescriptor.CalendarDialogCellEditor;
import com.pat.tool.keditor.propertyDescriptor.TreeContentProvider;
import com.pat.tool.keditor.table.AddColumnDataAction;
import com.pat.tool.keditor.table.DeleteRow;
import com.pat.tool.keditor.table.InsertAfterAction;
import com.pat.tool.keditor.table.InsertBeforeAction;
import com.pat.tool.keditor.table.MoveDownAction;
import com.pat.tool.keditor.table.MoveUpAction;
import com.pat.tool.keditor.table.PotentialNewRowData;
import com.pat.tool.keditor.table.TableUtils;
import com.pat.tool.keditor.table.TableWithButtons;
import com.pat.tool.keditor.table.TableWithButtonsDeleteRowAction;
import com.pat.tool.keditor.table.TableWithButtonsInsertAfterAction;
import com.pat.tool.keditor.table.TableWithButtonsInsertBeforeAction;
import com.pat.tool.keditor.table.TableWithButtonsNewRowAction;
import com.pat.tool.keditor.utils.AppMenuFeature;
import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.EditableBrowserItem;
import com.pat.tool.keditor.utils.EditableTableItem;
import com.pat.tool.keditor.utils.GridCalendarData;
import com.pat.tool.keditor.utils.GridCalendarRowData;
import com.pat.tool.keditor.utils.ImageData;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.MenuBarData;
import com.pat.tool.keditor.utils.MenuData;
import com.pat.tool.keditor.utils.MenuItemData;
import com.pat.tool.keditor.utils.NScriptDialog;
import com.pat.tool.keditor.utils.KeyValuePair.EntryPair;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;
import com.pat.tool.service.model.Parameter;

public class GridCalendarDataDialog extends TitleAreaDialog {

	private TableViewer tableViewer;
	private final GridCalendarData gridCalendarData;
	private final IProject project;
	private int formCategory;
	private final KDateField widget;
	private int platform;
	private String propId;
	
	
	public static final int DATE_INDEX = 0;
	public static final int TEMPLATEDATA_INDEX = 1;
	
	public GridCalendarDataDialog(Shell parentShell, GridCalendarData gridCalendarData, KDateField widget, IProject project, int formCategory, int platform, String propId) {
		super(parentShell);
		this.gridCalendarData = gridCalendarData;
		this.widget = widget;
		this.project = project;
		this.formCategory = formCategory;
		this.platform = platform;
		this.propId = propId;
		setShellStyle(getShellStyle() | SWT.RESIZE); 
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Master data");
		newShell.setSize(500, 350);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Master Data for GridCalendar");
		setMessage("Choose a gridCalendar template for each date and define the data for it");
		Composite createDialogArea = (Composite) super.createDialogArea(parent);
		createTable(createDialogArea);
		setHelpAvailable(false);
		return createDialogArea;
	}

	private void createTable(final Composite composite) {

		Composite createDialogArea = new Composite(composite, SWT.NONE);
		createDialogArea.setLayout(new GridLayout(2,false));
		GridData data =new GridData(SWT.FILL, SWT.FILL, true, true);
		data.horizontalSpan = 2;
		createDialogArea.setLayoutData(data);
		TableWithButtons tableWithBtns = new TableWithButtons(createDialogArea, SWT.FULL_SELECTION | SWT.BORDER | SWT.MULTI);
		tableWithBtns.createControl();
		tableViewer = tableWithBtns.getViewer();
		//tableViewer = new TableViewer(createDialogArea, SWT.FULL_SELECTION | SWT.BORDER | SWT.MULTI);
		
		createActionsForTableWithButtons(tableWithBtns);
		
		Table table = tableViewer.getTable();
		
		TableLayout tablelayout = new TableLayout();
		
		GridData gridData = new GridData(GridData.FILL_BOTH);
		table.setLayoutData(gridData);
		table.setLayout(tablelayout);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		createColumnViewers();
		
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setInput(gridCalendarData.getListOfGridCalendarData());
		
		tableWithBtns.getActionsManager().update(true);
		
	}

	private void createActionsForTableWithButtons(TableWithButtons tableWithBtns) {
		tableWithBtns.getActionsManager().add(new TableWithButtonsNewRowAction(tableViewer,"Add") {

			@Override
			protected Object createNewRow(String firstCellVal) {
				GridCalendarRowData data = new GridCalendarRowData();
				StructuredViewer viewer = getViewer();
				ArrayList<GridCalendarRowData> rowsList  = (ArrayList)viewer.getInput();
				rowsList.add((GridCalendarRowData) data);
				viewer.setInput(rowsList);
				((TableViewer) viewer).getTable().select(rowsList.size()-1);
		    	return data;
			}

			@Override
			protected String getNewRowFirstCellVal() {
				return null;
			}
			
		}) ;
		tableWithBtns.getActionsManager().add(new DeleteRow(tableViewer,"Delete"));
		tableWithBtns.getActionsManager().add(new MoveUpAction(tableViewer,"MoveUp"));
		tableWithBtns.getActionsManager().add(new MoveDownAction(tableViewer,"MoveDown"));
		tableWithBtns.getActionsManager().add(new TableWithButtonsInsertBeforeAction(tableViewer,"Insert Before") {
			
			@Override
			protected void insertBefore(int i) {

				tableViewer.setSorter(null);
				Object data = null;
				if (tableViewer.getInput() instanceof ArrayList) {
					ArrayList rowsList = (ArrayList) tableViewer.getInput();
					if (rowsList.get(i) instanceof GridCalendarRowData)
						data = new GridCalendarRowData();
					rowsList.add(i, data);
					tableViewer.setInput(rowsList);
				} 
				tableViewer.getTable().select(i);
			
			}
		});
		tableWithBtns.getActionsManager().add(new TableWithButtonsInsertAfterAction(tableViewer,"Insert After") {
			
			@Override
			public void insertAfter(int idx) {

				tableViewer.setSorter(null);
				Object data = null; 
				if(tableViewer.getInput() instanceof ArrayList){
					ArrayList  choicesList  = (ArrayList)tableViewer.getInput();
					if(choicesList.get(idx) instanceof GridCalendarRowData)
						data = new GridCalendarRowData();
					choicesList.add(idx+1, data);
					tableViewer.setInput(choicesList);
				}
		    	tableViewer.getTable().select(idx+1);
			}
		});
	}

	private void createColumnViewers() {
		TableViewerColumn dateColumnViewer = new TableViewerColumn(tableViewer,SWT.NONE);
		TableColumn column = dateColumnViewer.getColumn();
		column.setText("Date");
		column.setWidth(200);
		column.setResizable(true);
		dateColumnViewer.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof GridCalendarRowData){
		           return ((GridCalendarRowData)element).getDate();
		        } 
				return "..";
			}	
		});
		
		dateColumnViewer.setEditingSupport(new GridCalendarEditingSupport(this,tableViewer,DATE_INDEX));
		
		TableViewerColumn templateDataColumnViewer = new TableViewerColumn(tableViewer,SWT.NONE);
		TableColumn templateDataColumn = templateDataColumnViewer.getColumn();
		templateDataColumn.setText("Template Data");
		templateDataColumn.setWidth(200);
		templateDataColumn.setResizable(true);
		templateDataColumnViewer.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof GridCalendarRowData){
					GridCalendarRowData rData = ((GridCalendarRowData)element);
					if(rData.getTemplateData() != null) {
						int size = rData.getTemplateData().getWidgetDataList().size();
						if(size > 0) {
							return "Defined";
						}
						else
							return "Not Defined";
					}
					else
						return "Not Defined";
		        } 
				return "..";
			}	
		});
		
		templateDataColumnViewer.setEditingSupport(new GridCalendarEditingSupport(this,tableViewer,TEMPLATEDATA_INDEX));
	}

	private class GridCalendarEditingSupport extends EditingSupport{
		
		GridCalendarDataDialog gridCalendarDataDialog;
		TableViewer viewer;
		int index = DATE_INDEX;

		public GridCalendarEditingSupport(GridCalendarDataDialog gridCalendarDataDialog, TableViewer viewer, int index) {
			super(viewer);
			this.gridCalendarDataDialog = gridCalendarDataDialog;
			this.viewer = viewer;
			this.index = index;
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			// TODO Auto-generated method stub
			if(index == DATE_INDEX)
				return new CalendarDialogCellEditor(tableViewer.getTable(), new KDateField(), false, false, KUtils.EMPTY_STRING, false);
			else 
			if(index == TEMPLATEDATA_INDEX) {
				TemplateDataCellEditor templateDataCellEditor = new TemplateDataCellEditor(tableViewer.getTable(), project, KPage.GRIDCALENDAR_MODE, formCategory, platform);
				templateDataCellEditor.setTemplateName(getCellTemplate(widget,platform,formCategory,propId));
				return templateDataCellEditor;
			}
			return null;
		}

		@Override
		protected boolean canEdit(Object element) {
			if(element instanceof GridCalendarRowData)
			{
				
				if(index == TEMPLATEDATA_INDEX){
					if(KUtils.EMPTY_STRING.equals((((GridCalendarRowData)element)).getDate())){
						return false;
					}
				}
				return true;
			}
			else
				return false;
		}

		@Override
		protected Object getValue(Object element) {
			if(element instanceof GridCalendarRowData) {
				if(index == DATE_INDEX)
					return ((GridCalendarRowData)element).getDate();
				else
				if(index == TEMPLATEDATA_INDEX)
					return ((GridCalendarRowData)element).getTemplateData();
			}
			
			return null;
		}

		@Override
		protected void setValue(Object element, Object value) {
			if(element instanceof GridCalendarRowData && value instanceof String) {
				((GridCalendarRowData)element).setDate((String)value);
				tableViewer.refresh();
				
			}
			else
			if(element instanceof GridCalendarRowData && value instanceof TemplateData) {
				((GridCalendarRowData)element).setTemplateData((TemplateData)value);
				tableViewer.refresh();
			}
		}
		
	}
	private String getCellTemplate(KDateField widget,int platform, int formCategory,String propId){
		if(formCategory == KPage.MOBILE_CAT){
			if(platform == MobileChannelConstants.IPHONE_ID || KDateField.IPHONE_DATA_PROP.equals(propId)){
				return widget.getIphone_cellTemplate();
			}else if(platform == MobileChannelConstants.ANDROID_ID || KDateField.ANDROID_DATA_PROP.equals(propId)){
				return widget.getAndroid_cellTemplate();
			}
		}else if(formCategory == KPage.TABLET_CAT){
			if(platform == MobileChannelConstants.IPAD_ID || KDateField.IPAD_DATA_PROP.equals(propId)){
				return widget.getIpad_cellTemplate();
			}else if(platform == MobileChannelConstants.ANDROID_RC_TABLET_ID || KDateField.ANDROID_DATA_PROP.equals(propId)){
				return widget.getAndroid_cellTemplate();
			}
		}
		return KUtils.EMPTY_STRING;
	}

}

