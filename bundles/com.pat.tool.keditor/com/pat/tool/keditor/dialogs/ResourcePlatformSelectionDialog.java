package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.platformselection.PlatformSelectionDialog;
import com.pat.tool.keditor.utils.RendererHashMapData;
import com.pat.tool.keditor.utils.StoreLevel;

public class ResourcePlatformSelectionDialog extends PlatformSelectionDialog {

	private SelectionListener radioListener = new RadioSelectionListener();
	private Button project;
	private Button workspace;
	private StoreLevel storeLevel;
	
	public ResourcePlatformSelectionDialog(Shell shell,
			RendererHashMapData renderData, String title, String desc, Image image) {
		super(shell, renderData, title, desc, image);
	}

	@Override
	protected Control createPlatformSelectionUI(Composite parent) {
		createWorkspaceRadio(parent);
		super.createPlatformSelectionUI(parent);
		return parent;
	}

	protected void createWorkspaceRadio(Composite parent) {
		Composite workspaceComp = new Composite(parent,SWT.NONE);
		workspaceComp.setLayout(new GridLayout(3,false));
		
		Label storeAt = new Label(workspaceComp, SWT.NONE);
		storeAt.setText("Store at : ");
		
		project = new Button(workspaceComp, SWT.RADIO);
		project.setText("Project Level");
		project.addSelectionListener(radioListener );
		
		workspace = new Button(workspaceComp, SWT.RADIO);
		workspace.setText("Workspace Level");
		workspace.addSelectionListener(radioListener);
		
		project.setSelection(true);
		storeLevel = StoreLevel.PROJECT_LEVEL;
	}
	
	public StoreLevel getStoreLevel() {
		if(storeLevel == null)
			storeLevel = StoreLevel.PROJECT_LEVEL;
		return storeLevel;
	}
	
	
	private class RadioSelectionListener extends SelectionAdapter{
		@Override
		public void widgetSelected(SelectionEvent e) {
			if(project.getSelection())
				storeLevel = StoreLevel.PROJECT_LEVEL;
			else if(workspace.getSelection())
				storeLevel = StoreLevel.WORKSPACE_LEVEL;
		}
	}
	
	@Override
	protected void validatePage() {
		getButton(Window.OK).setEnabled(true);
	}
}
