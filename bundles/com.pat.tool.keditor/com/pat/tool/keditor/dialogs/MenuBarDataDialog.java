/**
 * 
 */
package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.cellEditors.TemplateDataCellEditor;
import com.pat.tool.keditor.mapping.ui.dialogs.GridTemplateSelectionDialog;
import com.pat.tool.keditor.model.KComponent;
import com.pat.tool.keditor.model.KContainer;
import com.pat.tool.keditor.model.KMenu;
import com.pat.tool.keditor.model.KMenuBar;
import com.pat.tool.keditor.model.KMenuItem;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.model.KWidget;
import com.pat.tool.keditor.model.WidgetUtil.Widget;
import com.pat.tool.keditor.parts.KMenuItemEditPart;
import com.pat.tool.keditor.propertyDescriptor.CustomCheckboxCellEditor;
import com.pat.tool.keditor.propertyDescriptor.TreeContentProvider;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.MenuBarData;
import com.pat.tool.keditor.utils.MenuData;
import com.pat.tool.keditor.utils.MenuItemData;
import com.pat.tool.keditor.utils.ValidationUtil;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;

/**
 * @author Ram Anvesh
 * @since 4.5
 * @date Feb 16, 2012
 */
public class MenuBarDataDialog extends TitleAreaDialog {

	private TreeViewer treeViewer;
	private final MenuBarData menuBarData;
	private final IProject project;
	private int formCategory;
	private MenuManager contextMenu;
	private final KMenuBar widget;
    private Map<MenuItemData, MenuData> parentCache = new HashMap<MenuItemData, MenuData>();
    private String defaultTemplate;
	private int platform;
	
	public MenuBarDataDialog(Shell parentShell, MenuBarData menuBarData, KMenuBar widget, IProject project, int formCategory,String defaultTemplate, int platform) {
		super(parentShell);
		this.menuBarData = menuBarData;
		this.widget = widget;
		this.project = project;
		this.formCategory = formCategory;
		this.defaultTemplate = defaultTemplate;
		this.platform = platform;
		setShellStyle(getShellStyle() | SWT.RESIZE); 
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Master data");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Master Data for MenuBar");
		setMessage("Choose a menu template for each menu/menuitem and define the data for it");
		Composite createDialogArea = (Composite) super.createDialogArea(parent);
		createTree(createDialogArea);
		return createDialogArea;
	}

	/**
	 * @param parent
	 */
	private void createTree(Composite parent) {
		treeViewer = new TreeViewer(parent, SWT.FULL_SELECTION | SWT.BORDER | SWT.MULTI);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).applyTo(treeViewer.getTree());
		treeViewer.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		treeViewer.getTree().setLinesVisible(true);
		treeViewer.getTree().setHeaderVisible(true);
		initContextMenu();
		
		addColumns();
		treeViewer.setContentProvider(new TreeContentProvider(){
			@Override
			public Object[] getElements(Object inputElement) {
				if(inputElement instanceof MenuBarData){
					return new Object[]{((MenuBarData) inputElement).getMenuItemDataList()};
					/*List<MenuItemData> menuItemDataList = ((MenuBarData) inputElement).getMenuItemDataList();
					if(menuItemDataList != null)
						return menuItemDataList.toArray();*/
				}
				return super.getElements(inputElement);
			}
			
			@Override
			public Object[] getChildren(Object parentElement) {
				if(parentElement instanceof MenuData){
					List<MenuItemData> childrenItems = ((MenuData) parentElement).getChildrenItems();
					for (MenuItemData menuItemData : childrenItems) {
						parentCache.put(menuItemData, (MenuData) parentElement);
					}
					return childrenItems.toArray();
				}
				return super.getChildren(parentElement);
			}
		});
		
		treeViewer.setInput(menuBarData);
		treeViewer.expandAll();
	}

	private void initContextMenu() {
		contextMenu = new MenuManager("ContextMenu");
		contextMenu.setRemoveAllWhenShown(true);
		Menu menu = contextMenu.createContextMenu(treeViewer.getTree());
		contextMenu.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu();
			}
		});
		treeViewer.getTree().setMenu(menu);
	}

	
	private void fillContextMenu() {
		ITreeSelection selection = (ITreeSelection) treeViewer.getSelection();
		List<?> list = selection.toList();
		
		if(list.size() == 1){
			Object object = list.get(0);
			if(object instanceof List || object instanceof MenuData){// || object instanceof MenuItemData){
				KWidget target = null;
				if(object instanceof List)
					target = widget;
				else if(object instanceof MenuItemData){
					target = getWidgetWithId(widget, ((MenuItemData) object).getId());
				}
				/*addMenuAction.setTarget(target);
				contextMenu.add(addMenuAction);*/
				
				addMenuItemAction.setTarget(target);
				contextMenu.add(addMenuItemAction);
			}
		}
		
		if(list.size() > 0){
			List<KWidget> targetList =  new ArrayList<KWidget>(list.size());
			for (Object object : list) {
				if(object instanceof MenuItemData){
					KWidget widgetWithId = getWidgetWithId(widget, ((MenuItemData) object).getId());
					if(widgetWithId != null)
						targetList.add(widgetWithId);
				}
			}
			
			
			deleteAction.setTargetList(targetList);
			contextMenu.add(deleteAction);
		}
	}			

	/**
	 * Adds columns to the treeViewer
	 */
	private void addColumns() {
		TreeViewerColumn widgetIdCol = new TreeViewerColumn(treeViewer, SWT.LEFT);
		widgetIdCol.getColumn().setText("Widget Id");
		widgetIdCol.getColumn().setWidth(250);
		widgetIdCol.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				if(element instanceof List){
					return "MenuBar Data";
				} else if(element instanceof MenuItemData){
					MenuItemData menuItemData = (MenuItemData) element;
					return menuItemData.getId();
				}
				return "";
			}
			
			@Override
			public Image getImage(Object element) {
				if(element instanceof MenuItemData){
					MenuItemData menuItemData = (MenuItemData) element;
					/*if(menuItemData instanceof MenuData){
						return Widget.MENU_BUTTON.getIconImage();
					} else {*/
						return Widget.MENU_ITEM.getIconImage();
//					}
				}
				return null;
			}
		});
		/*widgetIdCol.setEditingSupport(new EditingSupport(treeViewer) {
			
			@Override
			protected void setValue(Object element, Object value) {

			}
			
			@Override
			protected Object getValue(Object element) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				// TODO Auto-generated method stub
				return false;
			}
		});*/
		
		
		TreeViewerColumn templateCol = new TreeViewerColumn(treeViewer, SWT.LEFT);
		templateCol.getColumn().setText("Template Id");
		templateCol.getColumn().setWidth(150);
		templateCol.setEditingSupport(new EditingSupport(treeViewer) {
			
			private CellEditor cellEditor;

			@Override
			protected void setValue(Object element, Object value) {
				if(element instanceof MenuItemData && value instanceof String){
					((MenuItemData) element).setTemplateId((String) value);
				}
				treeViewer.refresh(true);
			}
			
			@Override
			protected Object getValue(Object element) {
				if(element instanceof MenuItemData){
					String templateId = ((MenuItemData) element).getTemplateId();
					if(templateId == null)
						templateId = KUtils.DEFAULT_NONE;
					return templateId;
				}
				return "";
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				if(element instanceof MenuItemData){
					if(cellEditor == null)
						cellEditor = new DialogCellEditor(treeViewer.getTree()) {
						
							private String oldValue;

							protected void updateContents(Object value) {
								if(value instanceof String){
									oldValue = (String) value;
								}
							};
							
							@Override
							protected Object openDialogBox(Control cellEditorWindow) {
								GridTemplateSelectionDialog dialog = new GridTemplateSelectionDialog(project, KPage.MENU_MODE);
								int open = dialog.open();
								if(open == Window.OK){
									Object firstResult = dialog.getFirstResult();
									if(firstResult instanceof IResource){
										IResource resource = (IResource) firstResult;
										return KUtils.getFormNameWithoutExtension(resource.getName());
									}
									return firstResult;
								} else {
									return oldValue;
								}
								
							}
						};
					return cellEditor;
				}
				return null;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
		templateCol.setLabelProvider(new ColumnLabelProvider(){
		
			@Override
			public String getText(Object element) {
				if(element instanceof MenuItemData){
					return ((MenuItemData) element).getTemplateId();
				}
				return "";
			}
		});
		
		TreeViewerColumn dataCol = new TreeViewerColumn(treeViewer, SWT.LEFT);
		dataCol.getColumn().setText("Data");
		dataCol.getColumn().setWidth(150);
		dataCol.setLabelProvider(new ColumnLabelProvider(){
			@Override
			public String getText(Object element) {
				if(element instanceof MenuItemData){
					/*TemplateData data = ((MenuItemData) element).getTemplateData();
					if(data == null){
						return "Not Defined";
					}*/
					return "Template Data";
				}
				return "";
			}
		});
		dataCol.setEditingSupport(new EditingSupport(treeViewer) {
			
			@Override
			protected void setValue(Object element, Object value) {
				if(element instanceof MenuItemData && value instanceof TemplateData){
					MenuItemData menuItemData = (MenuItemData) element;
					menuItemData.setTemplateData((TemplateData) value);
					treeViewer.refresh();
				}
			}
			
			@Override
			protected Object getValue(Object element) {
				if(element instanceof MenuItemData ){
					return ((MenuItemData) element).getTemplateData();
				}
				return null;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				if(element instanceof MenuItemData){
					MenuItemData menuItemData = (MenuItemData) element;
					TemplateDataCellEditor cellEditor = new TemplateDataCellEditor(treeViewer.getTree(), project, KPage.MENU_MODE, formCategory, platform);
					cellEditor.setTemplateName(menuItemData.getTemplateId());
					return cellEditor;
				}
				return null;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
		
		
		TreeViewerColumn hasChildColumn = new TreeViewerColumn(treeViewer, SWT.LEFT);
		hasChildColumn.getColumn().setText("subMenu");
		hasChildColumn.getColumn().setWidth(150);
		
		
		hasChildColumn.setEditingSupport(new EditingSupport(treeViewer) {
			
			private CellEditor cellEditor;
			@Override
			protected void setValue(Object element, Object value) {
				if(element instanceof MenuItemData && value instanceof Boolean){
					MenuItemData menuItemData = (MenuItemData) element;
					MenuData parent = parentCache.get(element);
					KContainer target = null;
					if (parent != null) {
						target = (KContainer) getWidgetWithId(widget, parent.getId());
					} else  {
						target = widget;
					}
					KWidget oldMenu = getWidgetWithId(widget, menuItemData.getId());
					KWidget newMenu = null;
					int index = target.getChildren().indexOf(oldMenu);
					target.removeChild(oldMenu);
					oldMenu.setParent(null);
					if (oldMenu instanceof KMenu) {
						newMenu = new KMenuItem();
					} else {
						newMenu = new KMenu();
					}
					newMenu.setSize(oldMenu.getSize());
					newMenu.setLocation(oldMenu.getLocation());
					newMenu.setParent(target);
					newMenu.init();
					target.addChild(newMenu, index);
					menuBarData.validateChildren(widget,defaultTemplate);
					treeViewer.refresh();
					treeViewer.expandAll();
				}
 			}
			
			@Override
			protected Object getValue(Object element) {
				/*// TODO Auto-generated method stub
				if(element instanceof MenuItemData){
					Boolean hasChild = ((MenuItemData) element).getHasChildren();
					if(hasChild == null)
						hasChild = new Boolean(false);
					return hasChild;
				}*/
				return element instanceof MenuData;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				if(element instanceof MenuItemData){
					MenuItemData menuItemData = (MenuItemData) element;
				cellEditor = new CheckboxCellEditor(treeViewer.getTree());
				cellEditor.setValue(menuItemData instanceof MenuData);
					return cellEditor;
				}
				return null;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		hasChildColumn.setLabelProvider(new ColumnLabelProvider(){
			
			@Override
			public String getText(Object element) {
				if(element instanceof MenuItemData){
					return String.valueOf(element instanceof MenuData);
				}
				return "";
			}
		});
	}
	
	private AddAction addMenuAction = new AddAction(AddAction.MENU_TYPE);
	private AddAction addMenuItemAction = new AddAction(AddAction.MENU_ITEM_TYPE);
	private DeleteAction deleteAction = new DeleteAction();
	
	private class AddAction extends Action{
		
		private Object target;
		public static final int MENU_ITEM_TYPE = 1;
		public static final int MENU_TYPE = 2;
		private final int type;
		
		public AddAction(int type) {
			super();
			this.type = type;
			if(type == MENU_ITEM_TYPE)
				setText("Add " + KMenuItem.DISPLAY_NAME);
			else if(type == MENU_TYPE)
				setText("Add " + KMenu.DISPLAY_NAME);
		}

		public void setTarget(Object object){
			this.target = object;
		}
		
		@Override
		public boolean isEnabled() {
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			
			return true;
			
		}
		
		@Override
		public void run() {
			if(target instanceof KMenuBar || target instanceof KMenu){
				KContainer parent = (KContainer) target;
				List<KWidget> children = new ArrayList<KWidget>(parent.getChildren());
				Collections.sort(children);
				KWidget menu  = null;
				if(type == MENU_TYPE)
					menu = new KMenu();
				else if(type == MENU_ITEM_TYPE)
					menu = new KMenuItem();
				if(menu == null)
					return;
				if(children.size() > 0){
					Rectangle bounds = ((KWidget) children.get(children.size()-1)).getBounds();
					menu.setLocation(new Point(bounds.x,bounds.y+bounds.height));
				} else {
					menu.setLocation(new Point(0, 0));
				}
				menu.setSize(new Dimension(parent.getSize().width, KMenuItemEditPart.MENU_ITEM_FIGURE_HEIGHT));
				menu.setParent(parent);
				//Call init before addint child to parent.
				//Or else firePropertyChange method is called by model which causes warning dialog to be opened when is is changed
				menu.init();
				parent.addChild(menu);
				menuBarData.validateChildren(widget,defaultTemplate);
				treeViewer.refresh();
				treeViewer.expandAll();
			}
			
		}
	}
	
	private class DeleteAction extends Action{
		
		private List<KWidget> targetList;

		public void setTargetList(List<KWidget> widgetList){
			this.targetList = widgetList;
			setText("Delete");
		}
		
		@Override
		public void run() {
			if(targetList == null)
				return;
			for (KWidget target : targetList) {
				KWidget widget = target.getParent();
				if(widget instanceof KContainer){
					KContainer parent = (KContainer) widget;
					parent.removeChild(target);
					target.setParent(null);
				}
				menuBarData.validateChildren(MenuBarDataDialog.this.widget,defaultTemplate);
				treeViewer.refresh();
				treeViewer.expandAll();
			}
		}
	}
	
	private static KWidget getWidgetWithId(KWidget widget, String widgetId){
		if(widget != null && ValidationUtil.nullSafeEquals(widgetId, widget.getID())){
			return widget;
		}
		if(widget instanceof KContainer){
			List<?> children = ((KContainer) widget).getChildren();
			for (Object object : children) {
				if(object instanceof KWidget){
					KWidget widgetWithId = getWidgetWithId((KWidget) object, widgetId);
					if(widgetWithId != null)
						return widgetWithId;
				}
			}
		}
		return null;
	}
}
