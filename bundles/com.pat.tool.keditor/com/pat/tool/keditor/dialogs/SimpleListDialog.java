/* @copyright Kony Solutions Inc.(c) 2010 All Rights are reserved.*/
package com.pat.tool.keditor.dialogs;

import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;

/**
 * Read only dialog showing information about service templates 
 * which are defined but not mapped to any connector.
 * 
 */

public class SimpleListDialog extends TrayDialog {
	
	private static final int PLUGIN_COLUMN_INDEX = 0;
	private static final String PLUGIN_COLUMN = "Services";
	private static String[] COLUMN_NAMES = new String[]{PLUGIN_COLUMN};
	
	private List<String> inputList;
	
	public SimpleListDialog(Shell shell,List<String> inputList) {
		super(shell);
		this.inputList = inputList;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Unmapped service templates");
	}
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite childComposite = (Composite) super.createDialogArea(parent);
		GridData data = new GridData(SWT.FILL,SWT.FILL,true,true);
		//data.widthHint = 440;
		childComposite.setLayoutData(data);
		childComposite.setLayout(new GridLayout());
		createTableViewer(childComposite);
		return childComposite;
	}

	private void createTableViewer(Composite parent) {
		TableViewer tViewer = new TableViewer(parent, SWT.V_SCROLL| SWT.BORDER);
		for(int i = 0; i<COLUMN_NAMES.length;i++){
			TableViewerColumn tColumn = new TableViewerColumn(tViewer,SWT.NONE);
			tColumn.getColumn().setText(COLUMN_NAMES[i]);
			if(i == PLUGIN_COLUMN_INDEX)
				tColumn.getColumn().setWidth(150);
		}
		tViewer.setContentProvider(new ArrayContentProvider());
		tViewer.setLabelProvider(new TableLabelProvider());
		tViewer.getTable().setHeaderVisible(true);
		tViewer.getTable().setLinesVisible(true);
		tViewer.setInput(inputList);
		
		Label label = new Label(parent, SWT.NONE);
		label.setText("Above list of services are defined in service definition template, \n" +
				"but are not imported into any connectors like XML/Soap. \nSo these services will not be published. Do you want to proceed? ");
		label.setForeground(ColorConstants.blue);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		label.setLayoutData(data);
		
	}
	
	/**
	 * Override this method to for having just OK dialog.
	 */
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL,
				false);
	}
	
	private static class TableLabelProvider extends LabelProvider implements ITableLabelProvider{

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if(element instanceof String){
				switch(columnIndex){
				case PLUGIN_COLUMN_INDEX:
					return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceImage);
				}
			}
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if(element instanceof String){
				switch(columnIndex){
				case PLUGIN_COLUMN_INDEX:
					return (String)element;
				}
			}
			return "";
		}
	}
}
