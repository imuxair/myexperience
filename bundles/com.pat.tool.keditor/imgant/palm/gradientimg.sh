export PATH=$PATH:$7/bin
export DYLD_LIBRARY_PATH="$7/lib/"
P1=$1
shift
k=${P1//\\/\/}
cd $k
convert -size 5x30 gradient:'rgba('$2','$4')'-'rgba('$3','$5')' $1.png
