export PATH=$PATH:$6/bin/:/bin/
export DYLD_LIBRARY_PATH="$6/lib/"
k=${1//\\/\/}
cd $k
convert -size 33x51 xc:none -fill white -draw "roundRectangle 0,0 33,51 15,15" t_img.png -compose SrcIn -composite $2.png
rm t_img.png