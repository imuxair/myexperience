define("Form1", function() {
    return function(controller) {
        function addWidgetsForm1() {
            this.setDefaultUnit(kony.flex.DP);
            var substitutecontainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70%",
                "id": "substitutecontainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "50%"
            }, {}, {});
            substitutecontainer.setDefaultUnit(kony.flex.DP);
            var substitutecontent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "substitutecontent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            substitutecontent.setDefaultUnit(kony.flex.DP);
            var Image0fe1dfacc26db4e = new kony.ui.Image2({
                "centerX": "50%",
                "height": "20%",
                "id": "Image0fe1dfacc26db4e",
                "isVisible": true,
                "left": "30%",
                "skin": "slImage",
                "src": "fblogo_removebg_preview.png",
                "top": "0",
                "width": "65%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var RichText0a554124efa7941 = new kony.ui.RichText({
                "height": "220dp",
                "id": "RichText0a554124efa7941",
                "isVisible": true,
                "left": "26%",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0a4a33d6a04824f",
                "text": "Facebook helps you connect and share with the people in your life.",
                "top": "3%",
                "width": "70%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            substitutecontent.add(Image0fe1dfacc26db4e, RichText0a554124efa7941);
            substitutecontainer.add(substitutecontent);
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1366,
                "1024": {
                    "substitutecontainer": {
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {}
            this.add(substitutecontainer);
        };
        return [{
            "addWidgets": addWidgetsForm1,
            "enabledForIdleTimeout": false,
            "id": "Form1",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366],
            "info": {
                "kuid": "fd83ef15294f417992e1e46759b876a8"
            }
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});