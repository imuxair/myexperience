define("loginpage", function() {
    return function(controller) {
        function addWidgetsloginpage() {
            this.setDefaultUnit(kony.flex.DP);
            var mainwrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "mainwrapper",
                "isVisible": true,
                "layoutType": kony.flex.RESPONSIVE_GRID,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0i2985b4322e046",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            mainwrapper.setDefaultUnit(kony.flex.DP);
            var forspace = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "forspace",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 0,
                        "1024": 1,
                        "1366": 2
                    }
                }
            }, {}, {});
            forspace.setDefaultUnit(kony.flex.DP);
            forspace.add();
            var centerwrapper = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "800dp",
                "id": "centerwrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2dp",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 12,
                        "1024": 10,
                        "1366": 8
                    }
                }
            }, {}, {});
            centerwrapper.setDefaultUnit(kony.flex.DP);
            var leftcol = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "leftcol",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "9.95%",
                "isModalContainer": false,
                "skin": "CopyslFbox0j8bdf668d10141",
                "top": 7,
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            leftcol.setDefaultUnit(kony.flex.DP);
            var logo = new kony.ui.Image2({
                "height": "40dp",
                "id": "logo",
                "isVisible": true,
                "left": "30%",
                "skin": "slImage",
                "src": "fblogo_removebg_preview.png",
                "top": "70dp",
                "width": "40%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var recentlogin = new kony.ui.Label({
                "centerX": "50%",
                "id": "recentlogin",
                "isVisible": true,
                "left": "20%",
                "skin": "CopydefLabel0j9cee48813ac4f",
                "text": "Recent logins",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var instruction = new kony.ui.Label({
                "centerX": "50%",
                "id": "instruction",
                "isVisible": true,
                "skin": "CopydefLabel0aa09486861af43",
                "text": "Click your picture or add an account",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var idbutton = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "230dp",
                "id": "idbutton",
                "isVisible": true,
                "layoutType": kony.flex.RESPONSIVE_GRID,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0h644a4fbef0748",
                "top": "40dp",
                "width": "80%"
            }, {}, {});
            idbutton.setDefaultUnit(kony.flex.DP);
            var FlexContainer0b54c845fe3c443 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "230dp",
                "id": "FlexContainer0b54c845fe3c443",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c4409acc56cf41668b63bcc515bc8861,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 12,
                        "1024": 5,
                        "1366": 4
                    }
                }
            }, {}, {});
            FlexContainer0b54c845fe3c443.setDefaultUnit(kony.flex.DP);
            var Image0edb0dc3fe0cb41 = new kony.ui.Image2({
                "height": "80%",
                "id": "Image0edb0dc3fe0cb41",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlexContainer0i97a1a13eb2345 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "FlexContainer0i97a1a13eb2345",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0b253c2d48be74e",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0i97a1a13eb2345.setDefaultUnit(kony.flex.DP);
            var Label0bef25b8a7bd044 = new kony.ui.Label({
                "id": "Label0bef25b8a7bd044",
                "isVisible": true,
                "left": "0",
                "skin": "defLabel",
                "text": "Label",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0i97a1a13eb2345.add(Label0bef25b8a7bd044);
            var FlexContainer0ie216d0af66f4c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15%",
                "id": "FlexContainer0ie216d0af66f4c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_de8604ee2ecb44a3b1b280d5357af509,
                "skin": "slFbox",
                "top": 0,
                "width": "20%"
            }, {}, {});
            FlexContainer0ie216d0af66f4c.setDefaultUnit(kony.flex.DP);
            var Image0ce29f737d04947 = new kony.ui.Image2({
                "height": "70%",
                "id": "Image0ce29f737d04947",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "close.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ie216d0af66f4c.add(Image0ce29f737d04947);
            var FlexContainer0ad4acc2bfe8f45 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "14%",
                "id": "FlexContainer0ad4acc2bfe8f45",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "82%",
                "isModalContainer": false,
                "skin": "CopyslFbox0b509af9b0ffb46",
                "top": "0",
                "width": "17%"
            }, {}, {});
            FlexContainer0ad4acc2bfe8f45.setDefaultUnit(kony.flex.DP);
            var Label0c787c110c50145 = new kony.ui.Label({
                "height": "100%",
                "id": "Label0c787c110c50145",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0d7fa7029b6634c",
                "text": "4",
                "top": "0",
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ad4acc2bfe8f45.add(Label0c787c110c50145);
            FlexContainer0b54c845fe3c443.add(Image0edb0dc3fe0cb41, FlexContainer0i97a1a13eb2345, FlexContainer0ie216d0af66f4c, FlexContainer0ad4acc2bfe8f45);
            var FlexContainer0dd9a869734db42 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "FlexContainer0dd9a869734db42",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 4,
                        "1024": 2,
                        "1366": 2
                    }
                }
            }, {}, {});
            FlexContainer0dd9a869734db42.setDefaultUnit(kony.flex.DP);
            FlexContainer0dd9a869734db42.add();
            var FlexContainer0acf1d8a815d247 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "230dp",
                "id": "FlexContainer0acf1d8a815d247",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0g722bdfc843641",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 12,
                        "1024": 5,
                        "1366": 4
                    }
                }
            }, {}, {});
            FlexContainer0acf1d8a815d247.setDefaultUnit(kony.flex.DP);
            var FlexContainer0h8cd73e274b546 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "FlexContainer0h8cd73e274b546",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_de09f69773d54d13b4edb5989201f82b,
                "skin": "CopyslFbox0g13db3dbefcc45",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0h8cd73e274b546.setDefaultUnit(kony.flex.DP);
            var Image0b4f21b5d629b48 = new kony.ui.Image2({
                "height": "25%",
                "id": "Image0b4f21b5d629b48",
                "isVisible": true,
                "left": "37.50%",
                "skin": "slImage",
                "src": "frame__9_.png",
                "top": "37.50%",
                "width": "25%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0h8cd73e274b546.add(Image0b4f21b5d629b48);
            var FlexContainer0h47f36908fb448 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "FlexContainer0h47f36908fb448",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0i94562f0084d4c",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0h47f36908fb448.setDefaultUnit(kony.flex.DP);
            var Label0c4c5b2434b5d4b = new kony.ui.Label({
                "id": "Label0c4c5b2434b5d4b",
                "isVisible": true,
                "left": "0",
                "skin": "defLabel",
                "text": "Label",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0h47f36908fb448.add(Label0c4c5b2434b5d4b);
            FlexContainer0acf1d8a815d247.add(FlexContainer0h8cd73e274b546, FlexContainer0h47f36908fb448);
            idbutton.add(FlexContainer0b54c845fe3c443, FlexContainer0dd9a869734db42, FlexContainer0acf1d8a815d247);
            leftcol.add(logo, recentlogin, instruction, idbutton);
            var rightcol = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": 430,
                "id": "rightcol",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15%",
                "width": "50%"
            }, {}, {});
            rightcol.setDefaultUnit(kony.flex.DP);
            var loginbox = new kony.ui.FlexContainer({
                "centerX": "50%",
                "centerY": "40%",
                "clipBounds": true,
                "height": "320dp",
                "id": "loginbox",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0c24d2f5880994d",
                "top": 0,
                "width": "80%"
            }, {}, {});
            loginbox.setDefaultUnit(kony.flex.DP);
            var TextField0bdfbf97bfb3649 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "defTextBoxFocus",
                "height": "60dp",
                "id": "TextField0bdfbf97bfb3649",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0",
                "placeholder": "Email address or phone number",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0dd26de3459414b",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "20dp",
                "width": "85%"
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var TextField0gb9d747558a34d = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "defTextBoxFocus",
                "height": "60dp",
                "id": "TextField0gb9d747558a34d",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0",
                "placeholder": "Password",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0j5eb1f47b53f43",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": 20,
                "width": "85%"
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var Button0fb91783a68a148 = new kony.ui.Button({
                "centerX": "49.93%",
                "focusSkin": "defBtnFocus",
                "height": "60dp",
                "id": "Button0fb91783a68a148",
                "isVisible": true,
                "left": "0",
                "onClick": controller.AS_Button_ce34b5be03064aba90b30727c3141ddb,
                "skin": "CopydefBtnNormal0je6444376e5f40",
                "text": "Log in",
                "top": "20dp",
                "width": "85%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0fce238fbb3534d = new kony.ui.Label({
                "centerX": "50%",
                "id": "Label0fce238fbb3534d",
                "isVisible": true,
                "left": 0,
                "skin": "CopydefLabel0bd05222ef27248",
                "text": "Forgotten password?",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlexContainer0d83e32d025934f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "2dp",
                "id": "FlexContainer0d83e32d025934f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ae59540011724b",
                "top": "15dp",
                "width": "90%"
            }, {}, {});
            FlexContainer0d83e32d025934f.setDefaultUnit(kony.flex.DP);
            FlexContainer0d83e32d025934f.add();
            var Button0bb602a0905b74a = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "60dp",
                "id": "Button0bb602a0905b74a",
                "isVisible": true,
                "left": "20%",
                "onClick": controller.AS_Button_g264f46ce4034282878558d2097c3301,
                "skin": "CopydefBtnNormal0a57007cac89f42",
                "text": "Create New Account",
                "top": "3%",
                "width": "60%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            loginbox.add(TextField0bdfbf97bfb3649, TextField0gb9d747558a34d, Button0fb91783a68a148, Label0fce238fbb3534d, FlexContainer0d83e32d025934f, Button0bb602a0905b74a);
            var RichText0af0e7d699f0e48 = new kony.ui.RichText({
                "centerX": "50%",
                "id": "RichText0af0e7d699f0e48",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0i8a151142d9148",
                "text": "<b>Create a page</b> for a brand or business",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            rightcol.add(loginbox, RichText0af0e7d699f0e48);
            centerwrapper.add(leftcol, rightcol);
            var forspace2 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "forspace2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 0,
                        "1024": 1,
                        "1366": 2
                    }
                }
            }, {}, {});
            forspace2.setDefaultUnit(kony.flex.DP);
            forspace2.add();
            var forspacevertical = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "0dp",
                "id": "forspacevertical",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 12,
                        "1024": 12,
                        "1366": 12
                    }
                }
            }, {}, {});
            forspacevertical.setDefaultUnit(kony.flex.DP);
            forspacevertical.add();
            var FlexContainer0i2c98ee99aa14c = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "20%",
                "id": "FlexContainer0i2c98ee99aa14c",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0j0c4d329571246",
                "top": "7dp",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 12,
                        "1024": 12,
                        "1366": 12
                    }
                }
            }, {}, {});
            FlexContainer0i2c98ee99aa14c.setDefaultUnit(kony.flex.DP);
            var RichText0e8a17a672b9549 = new kony.ui.RichText({
                "id": "RichText0e8a17a672b9549",
                "isVisible": true,
                "left": "10%",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0bf485b817c1843",
                "text": "<a style=\"color:#646b7a;font-size:15px;\" href=\"\">English (UK)</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Urdu</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Pushto</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Arabic</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Hindi</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">japanese</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Farsi</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">German</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Deutsch/a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Espanol</a>  <span style=\"background-color:#f5f6f7;color:#646b7a;font-size:15px;padding: 0px 10px 0px 10px;border: 1px solid grey;\">+</span>",
                "top": "0",
                "width": "80%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 1],
                "paddingInPixel": false
            }, {});
            var RichText0bebde516241d41 = new kony.ui.RichText({
                "height": "2dp",
                "id": "RichText0bebde516241d41",
                "isVisible": true,
                "left": "10%",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0b2583364258044",
                "text": "<hr />",
                "top": "5dp",
                "width": "80%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var RichText0i0ecb5e53f2241 = new kony.ui.RichText({
                "id": "RichText0i0ecb5e53f2241",
                "isVisible": true,
                "left": "10%",
                "linkSkin": "defRichTextLink",
                "skin": "defRichTextNormal",
                "text": "<a style=\"color:#646b7a;font-size:15px;\" href=\"\">Sign Up </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Log In </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Messenger </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Facebook Lite </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Watch </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Places </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Games </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Market Place </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Facebook Pay </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Jobs </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Oculus </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Portal </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Instagram </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Bullitin </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Local </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Fundraisers </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Services </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Voting Information Center </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Groups </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">About </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Create ad </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Create Page </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Developers </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Careers </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Privacy </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Cookies </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Ad Choices </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Terms </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Help</a>\n",
                "top": "6dp",
                "width": "80%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var RichText0a414d2b1800948 = new kony.ui.RichText({
                "id": "RichText0a414d2b1800948",
                "isVisible": true,
                "left": "10%",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0ab18604afd754f",
                "text": "Meta © 2021",
                "top": "14dp",
                "width": "80%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0i2c98ee99aa14c.add(RichText0e8a17a672b9549, RichText0bebde516241d41, RichText0i0ecb5e53f2241, RichText0a414d2b1800948);
            mainwrapper.add(forspace, centerwrapper, forspace2, forspacevertical, FlexContainer0i2c98ee99aa14c);
            var pidlogin = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "pidlogin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0df3da269334e4a",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            pidlogin.setDefaultUnit(kony.flex.DP);
            var takepassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "65%",
                "id": "takepassword",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0g22b35a0c42b4e",
                "top": "0",
                "width": "500dp",
                "zIndex": 1
            }, {}, {});
            takepassword.setDefaultUnit(kony.flex.DP);
            var Copyclosebutton0f81113a2a9f443 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "Copyclosebutton0f81113a2a9f443",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "92%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c1e57e41a4ba44ab947134b1464c525c,
                "skin": "CopyslFbox0hac003be6f7944",
                "top": "2%",
                "width": "25dp",
                "zIndex": 100
            }, {}, {});
            Copyclosebutton0f81113a2a9f443.setDefaultUnit(kony.flex.DP);
            var CopyImage0b23eb4d15edd4e = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0b23eb4d15edd4e",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "close.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            Copyclosebutton0f81113a2a9f443.add(CopyImage0b23eb4d15edd4e);
            var FlexContainer0i0cbcfdf189540 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "220dp",
                "id": "FlexContainer0i0cbcfdf189540",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ie3aa8681ece45",
                "top": "5%",
                "width": "220dp"
            }, {}, {});
            FlexContainer0i0cbcfdf189540.setDefaultUnit(kony.flex.DP);
            var Image0abd0c9a7dc3f46 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0abd0c9a7dc3f46",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0i0cbcfdf189540.add(Image0abd0c9a7dc3f46);
            var Label0ba2ab74069b640 = new kony.ui.Label({
                "centerX": "50%",
                "id": "Label0ba2ab74069b640",
                "isVisible": true,
                "left": 0,
                "skin": "CopydefLabel0b3a571b7a7f043",
                "text": "Syed Uzair Hussain",
                "top": "268dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextField0bd6122edcaf54e = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "60dp",
                "id": "TextField0bd6122edcaf54e",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5%",
                "placeholder": "Password",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0fa45750eab2c44",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "50%",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var CheckBoxGroup0c996cfe7c8cd4d = new kony.ui.CheckBoxGroup({
                "height": "8%",
                "id": "CheckBoxGroup0c996cfe7c8cd4d",
                "isVisible": true,
                "left": 0,
                "masterData": [
                    ["cbg2", "Remember me"]
                ],
                "skin": "CopyslCheckBoxGroup0e2acd08a97de4f",
                "top": "62%",
                "width": "100%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [6, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Button0i247afca0a744f = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "60dp",
                "id": "Button0i247afca0a744f",
                "isVisible": true,
                "left": "5%",
                "skin": "CopydefBtnNormal0bc5251805b1a48",
                "text": "Log in",
                "top": "70%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0j45c2d9ecb4a4c = new kony.ui.Label({
                "id": "Label0j45c2d9ecb4a4c",
                "isVisible": true,
                "left": "32.50%",
                "skin": "CopydefLabel0j706e23bf6b04c",
                "text": "Forgotten Password",
                "top": "541dp",
                "width": "35%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            takepassword.add(Copyclosebutton0f81113a2a9f443, FlexContainer0i0cbcfdf189540, Label0ba2ab74069b640, TextField0bd6122edcaf54e, CheckBoxGroup0c996cfe7c8cd4d, Button0i247afca0a744f, Label0j45c2d9ecb4a4c);
            pidlogin.add(takepassword);
            var Loginto = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "Loginto",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0d7cb058f889b4a",
                "top": "0",
                "width": "100%"
            }, {}, {});
            Loginto.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c5f92f9acae145 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0c5f92f9acae145",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0f2481fa9338748",
                "top": "0",
                "width": "550dp"
            }, {}, {});
            FlexContainer0c5f92f9acae145.setDefaultUnit(kony.flex.DP);
            var Copyclosebutton0f27776e4c78f4d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "Copyclosebutton0f27776e4c78f4d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "92%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i02e2c8ffccb4193a21a05717093de99,
                "skin": "CopyslFbox0hac003be6f7944",
                "top": "2%",
                "width": "25dp",
                "zIndex": 100
            }, {}, {});
            Copyclosebutton0f27776e4c78f4d.setDefaultUnit(kony.flex.DP);
            var CopyImage0bdcb9f3f1ab149 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0bdcb9f3f1ab149",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "close.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            Copyclosebutton0f27776e4c78f4d.add(CopyImage0bdcb9f3f1ab149);
            var Label0d491a57fcccf49 = new kony.ui.Label({
                "id": "Label0d491a57fcccf49",
                "isVisible": true,
                "left": "30%",
                "skin": "CopydefLabel0f18ce3ec3bb348",
                "text": "Create Account",
                "top": "24dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlexContainer0ic0464a86e974f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "0.50%",
                "id": "FlexContainer0ic0464a86e974f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0b3e7fba79ada47",
                "top": "15%",
                "width": "100%"
            }, {}, {});
            FlexContainer0ic0464a86e974f.setDefaultUnit(kony.flex.DP);
            FlexContainer0ic0464a86e974f.add();
            var TextField0a93dea32fb1a43 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "70dp",
                "id": "TextField0a93dea32fb1a43",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5%",
                "placeholder": "Email address or phone number",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0b9fd690630e64d",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "20%",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var TextField0e43c6f82f0c548 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "70dp",
                "id": "TextField0e43c6f82f0c548",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5%",
                "placeholder": "Password",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0d85312d7f62f4d",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "40%",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var CheckBoxGroup0h4c7743fa9e643 = new kony.ui.CheckBoxGroup({
                "height": "10%",
                "id": "CheckBoxGroup0h4c7743fa9e643",
                "isVisible": true,
                "left": 0,
                "masterData": [
                    ["cbg1", "Remember password"]
                ],
                "skin": "CopyslCheckBoxGroup0e2e59188a30342",
                "top": "60%",
                "width": "100%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [6, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Button0j7035f30922343 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "60dp",
                "id": "Button0j7035f30922343",
                "isVisible": true,
                "left": "5%",
                "skin": "CopydefBtnNormal0j3d8472cdf0b46",
                "text": "Create",
                "top": "70%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0f7d580550d4944 = new kony.ui.Label({
                "id": "Label0f7d580550d4944",
                "isVisible": true,
                "left": "32.50%",
                "skin": "CopydefLabel0cf62c52fa7924e",
                "text": "Forgotten password?",
                "top": "90%",
                "width": "35%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c5f92f9acae145.add(Copyclosebutton0f27776e4c78f4d, Label0d491a57fcccf49, FlexContainer0ic0464a86e974f, TextField0a93dea32fb1a43, TextField0e43c6f82f0c548, CheckBoxGroup0h4c7743fa9e643, Button0j7035f30922343, Label0f7d580550d4944);
            Loginto.add(FlexContainer0c5f92f9acae145);
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1366,
                "640": {
                    "mainwrapper": {
                        "height": {
                            "type": "ref",
                            "value": kony.flex.USE_PREFFERED_SIZE
                        },
                        "segmentProps": []
                    },
                    "centerwrapper": {
                        "height": {
                            "type": "string",
                            "value": "750dp"
                        },
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "leftcol": {
                        "left": {
                            "type": "string",
                            "value": "5%"
                        },
                        "segmentProps": []
                    },
                    "logo": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "left": {
                            "type": "number",
                            "value": "0"
                        },
                        "top": {
                            "type": "string",
                            "value": "75dp"
                        },
                        "segmentProps": []
                    },
                    "recentlogin": {
                        "centerX": {
                            "type": "string",
                            "value": ""
                        },
                        "left": {
                            "type": "number",
                            "value": "0"
                        },
                        "text": "Recent logins",
                        "top": {
                            "type": "string",
                            "value": "30dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "instruction": {
                        "centerX": {
                            "type": "string",
                            "value": ""
                        },
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "text": "Click your picture or add an account",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "idbutton": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "500dp"
                        },
                        "isVisible": true,
                        "left": {
                            "type": "string",
                            "value": "10%"
                        },
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0b54c845fe3c443": {
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "Image0edb0dc3fe0cb41": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "src": "syed.jpg",
                        "width": {
                            "type": "string",
                            "value": "200px"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0i97a1a13eb2345": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "20%"
                        },
                        "skin": "CopyslFbox0jc8ef518231948",
                        "width": {
                            "type": "string",
                            "value": "200px"
                        },
                        "segmentProps": []
                    },
                    "Label0bef25b8a7bd044": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "skin": "CopydefLabel0ad2dfc28e5a840",
                        "text": "Syed",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0dd9a869734db42": {
                        "height": {
                            "type": "string",
                            "value": "30dp"
                        },
                        "isVisible": true,
                        "segmentProps": []
                    },
                    "FlexContainer0acf1d8a815d247": {
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "FlexContainer0h8cd73e274b546": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "80%"
                        },
                        "width": {
                            "type": "string",
                            "value": "200px"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0h47f36908fb448": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "20%"
                        },
                        "skin": "CopyslFbox0e31b8ab9379342",
                        "width": {
                            "type": "string",
                            "value": "200px"
                        },
                        "segmentProps": []
                    },
                    "Label0c4c5b2434b5d4b": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "skin": "CopydefLabel0b3c8fbce5f2647",
                        "text": "Add Account",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "rightcol": {
                        "height": {
                            "type": "number",
                            "value": "480"
                        },
                        "left": {
                            "type": "string",
                            "value": ""
                        },
                        "skin": "CopyslFbox0fa98c59ef08e45",
                        "top": {
                            "type": "number",
                            "value": "25"
                        },
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "loginbox": {
                        "centerY": {
                            "type": "string",
                            "value": ""
                        },
                        "height": {
                            "type": "string",
                            "value": "380dp"
                        },
                        "left": {
                            "type": "string",
                            "value": "5%"
                        },
                        "width": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "forspacevertical": {
                        "segmentProps": []
                    },
                    "FlexContainer0i2c98ee99aa14c": {
                        "height": {
                            "type": "ref",
                            "value": kony.flex.USE_PREFFERED_SIZE
                        },
                        "segmentProps": []
                    },
                    "RichText0e8a17a672b9549": {
                        "text": "<a style=\"color:#646b7a;font-size:15px;\" href=\"\">English (UK)</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Urdu</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Pushto</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Arabic</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Hindi</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">japanese</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Farsi</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">German</a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Deutsch/a> <a style=\"color:#646b7a;font-size:15px\" href=\"\">Espanol</a>  <span style=\"background-color:#f5f6f7;color:#646b7a;font-size:15px;padding: 0px 10px 0px 10px;border: 1px solid grey;\">+</span>",
                        "segmentProps": []
                    },
                    "RichText0i0ecb5e53f2241": {
                        "text": "<a style=\"color:#646b7a;font-size:15px;\" href=\"\">Sign Up </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Log In </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Messenger </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Facebook Lite </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Watch </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Places </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Games </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Market Place </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Facebook Pay </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Jobs </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Oculus </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Portal </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Instagram </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Bullitin </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Local </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Fundraisers </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Services </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Voting Information Center </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Groups </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">About </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Create ad </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Create Page </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Developers </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Careers </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Privacy </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Cookies </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Ad Choices </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Terms </a><a style=\"color:#646b7a;font-size:15px;\" href=\"\">Help</a>\n",
                        "top": {
                            "type": "string",
                            "value": "0dp"
                        },
                        "segmentProps": []
                    },
                    "RichText0a414d2b1800948": {
                        "top": {
                            "type": "string",
                            "value": "4dp"
                        },
                        "segmentProps": []
                    },
                    "takepassword": {
                        "width": {
                            "type": "string",
                            "value": "70%"
                        },
                        "segmentProps": []
                    }
                },
                "1024": {
                    "mainwrapper": {
                        "centerX": {
                            "type": "string",
                            "value": "50.00%"
                        },
                        "centerY": {
                            "type": "string",
                            "value": "50.00%"
                        },
                        "height": {
                            "type": "ref",
                            "value": kony.flex.USE_PREFFERED_SIZE
                        },
                        "right": {
                            "type": "string",
                            "value": ""
                        },
                        "top": {
                            "type": "string",
                            "value": "0dp"
                        },
                        "segmentProps": []
                    },
                    "centerwrapper": {
                        "height": {
                            "type": "string",
                            "value": "920dp"
                        },
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "leftcol": {
                        "left": {
                            "type": "string",
                            "value": "15%"
                        },
                        "width": {
                            "type": "string",
                            "value": "70%"
                        },
                        "segmentProps": []
                    },
                    "logo": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "left": {
                            "type": "number",
                            "value": "0"
                        },
                        "top": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "segmentProps": []
                    },
                    "recentlogin": {
                        "left": {
                            "type": "number",
                            "value": "0"
                        },
                        "text": "Recent logins",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "instruction": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "text": "Click your picture or add an account",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "idbutton": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "240dp"
                        },
                        "left": {
                            "type": "number",
                            "value": "0"
                        },
                        "width": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0b54c845fe3c443": {
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "skin": "CopyslFbox0f6bf162cc5b642",
                        "segmentProps": []
                    },
                    "Image0edb0dc3fe0cb41": {
                        "src": "syed.jpg",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0i97a1a13eb2345": {
                        "height": {
                            "type": "string",
                            "value": "20%"
                        },
                        "top": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "Label0bef25b8a7bd044": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "skin": "CopydefLabel0b2fddeb7ecf244",
                        "text": "Syed",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0dd9a869734db42": {
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0acf1d8a815d247": {
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0h8cd73e274b546": {
                        "height": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0h47f36908fb448": {
                        "height": {
                            "type": "string",
                            "value": "20%"
                        },
                        "top": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "Label0c4c5b2434b5d4b": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "skin": "CopydefLabel0d636e113625440",
                        "text": "Add Account",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "rightcol": {
                        "height": {
                            "type": "number",
                            "value": "420"
                        },
                        "isVisible": true,
                        "left": {
                            "type": "string",
                            "value": "15%"
                        },
                        "top": {
                            "type": "string",
                            "value": "10dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "70%"
                        },
                        "segmentProps": []
                    },
                    "loginbox": {
                        "centerY": {
                            "type": "string",
                            "value": ""
                        },
                        "height": {
                            "type": "string",
                            "value": "390dp"
                        },
                        "left": {
                            "type": "string",
                            "value": ""
                        },
                        "top": {
                            "type": "string",
                            "value": ""
                        },
                        "width": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "TextField0bdfbf97bfb3649": {
                        "height": {
                            "type": "string",
                            "value": "65dp"
                        },
                        "skin": "CopydefTextBoxNormal0f019df02435441",
                        "text": "",
                        "segmentProps": []
                    },
                    "TextField0gb9d747558a34d": {
                        "height": {
                            "type": "string",
                            "value": "65dp"
                        },
                        "text": "",
                        "top": {
                            "type": "string",
                            "value": "3%"
                        },
                        "segmentProps": []
                    },
                    "Button0fb91783a68a148": {
                        "height": {
                            "type": "string",
                            "value": "65dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "3%"
                        },
                        "segmentProps": []
                    },
                    "Button0bb602a0905b74a": {
                        "bottom": {
                            "type": "string",
                            "value": ""
                        },
                        "height": {
                            "type": "string",
                            "value": "65dp"
                        },
                        "skin": "CopydefBtnNormal0c10b67e5e00746",
                        "text": "Create New Account",
                        "segmentProps": []
                    },
                    "forspacevertical": {
                        "segmentProps": []
                    },
                    "FlexContainer0i2c98ee99aa14c": {
                        "height": {
                            "type": "ref",
                            "value": kony.flex.USE_PREFFERED_SIZE
                        },
                        "top": {
                            "type": "string",
                            "value": "33dp"
                        },
                        "segmentProps": []
                    },
                    "pidlogin": {
                        "segmentProps": []
                    },
                    "Loginto": {
                        "segmentProps": []
                    }
                },
                "1366": {
                    "mainwrapper": {
                        "height": {
                            "type": "ref",
                            "value": kony.flex.USE_PREFFERED_SIZE
                        },
                        "segmentProps": []
                    },
                    "forspace": {
                        "height": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "centerwrapper": {
                        "height": {
                            "type": "string",
                            "value": "700dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "14dp"
                        },
                        "segmentProps": []
                    },
                    "leftcol": {
                        "left": {
                            "type": "number",
                            "value": "0"
                        },
                        "top": {
                            "type": "number",
                            "value": "43"
                        },
                        "width": {
                            "type": "string",
                            "value": "50%"
                        },
                        "segmentProps": []
                    },
                    "logo": {
                        "centerX": {
                            "type": "string",
                            "value": "50%"
                        },
                        "left": {
                            "type": "string",
                            "value": ""
                        },
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "idbutton": {
                        "segmentProps": []
                    },
                    "FlexContainer0b54c845fe3c443": {
                        "height": {
                            "type": "string",
                            "value": "210dp"
                        },
                        "skin": "CopyslFbox0e800f0ed08d14e",
                        "segmentProps": []
                    },
                    "Image0edb0dc3fe0cb41": {
                        "height": {
                            "type": "string",
                            "value": "170dp"
                        },
                        "maxWidth": {
                            "type": "string",
                            "value": ""
                        },
                        "src": "syed.jpg",
                        "segmentProps": []
                    },
                    "FlexContainer0i97a1a13eb2345": {
                        "height": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "170dp"
                        },
                        "segmentProps": []
                    },
                    "Label0bef25b8a7bd044": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "height": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "skin": "CopydefLabel0bdfdc8f9dd9e4a",
                        "text": "Syed",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0dd9a869734db42": {
                        "height": {
                            "type": "string",
                            "value": "200dp"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0acf1d8a815d247": {
                        "height": {
                            "type": "string",
                            "value": "210dp"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0h8cd73e274b546": {
                        "height": {
                            "type": "string",
                            "value": "170dp"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0h47f36908fb448": {
                        "height": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "Label0c4c5b2434b5d4b": {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "height": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "skin": "CopydefLabel0ie46f8e059634a",
                        "text": "Add Account",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "rightcol": {
                        "isVisible": true,
                        "top": {
                            "type": "string",
                            "value": "26%"
                        },
                        "width": {
                            "type": "string",
                            "value": "50%"
                        },
                        "segmentProps": []
                    },
                    "loginbox": {
                        "centerX": {
                            "type": "string",
                            "value": ""
                        },
                        "centerY": {
                            "type": "string",
                            "value": ""
                        },
                        "height": {
                            "type": "string",
                            "value": "330dp"
                        },
                        "left": {
                            "type": "string",
                            "value": "15%"
                        },
                        "width": {
                            "type": "string",
                            "value": "70%"
                        },
                        "segmentProps": []
                    },
                    "TextField0bdfbf97bfb3649": {
                        "height": {
                            "type": "string",
                            "value": "50dp"
                        },
                        "skin": "CopydefTextBoxNormal0jef258d8901544",
                        "text": "",
                        "segmentProps": []
                    },
                    "TextField0gb9d747558a34d": {
                        "height": {
                            "type": "string",
                            "value": "50dp"
                        },
                        "text": "",
                        "top": {
                            "type": "number",
                            "value": "14"
                        },
                        "segmentProps": []
                    },
                    "Button0fb91783a68a148": {
                        "height": {
                            "type": "string",
                            "value": "50dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "14dp"
                        },
                        "segmentProps": []
                    },
                    "Button0bb602a0905b74a": {
                        "height": {
                            "type": "string",
                            "value": "50dp"
                        },
                        "skin": "CopydefBtnNormal0dbc6a930c9414e",
                        "text": "Create New Account",
                        "segmentProps": []
                    },
                    "forspace2": {
                        "height": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0i2c98ee99aa14c": {
                        "top": {
                            "type": "string",
                            "value": "22dp"
                        },
                        "segmentProps": []
                    },
                    "pidlogin": {
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {}
            this.add(mainwrapper, pidlogin, Loginto);
        };
        return [{
            "addWidgets": addWidgetsloginpage,
            "enabledForIdleTimeout": false,
            "id": "loginpage",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366],
            "info": {
                "kuid": "cc4eca383f294cc3b1980017e9354e57"
            }
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});