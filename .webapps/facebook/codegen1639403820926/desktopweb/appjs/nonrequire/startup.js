kony.appinit.setApplicationMetaConfiguration("appid", "facebook");
kony.appinit.setApplicationMetaConfiguration("build", "debug");
kony.appinit.setApplicationMetaConfiguration("locales", []);
kony.appinit.setApplicationMetaConfiguration("i18nArray", []);
//startup.js
var appConfig = {
    appId: "facebook",
    appName: "facebook",
    appVersion: "1.0.0",
    isDebug: true,
    hotReloadURL: "ws://10.0.70.116:9099",
    isMFApp: false,
    eventTypes: [],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        isMVC: true,
        responsive: true,
        buttonAsLabel: true,
        APILevel: 9200,
        rtlMirroringInWidgetPropertySetter: true,
        FormControllerSyncLoad: false
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    requirejs.config({
        baseUrl: kony.appinit.getStaticContentPath() + 'desktopweb/appjs'
    });
    require(['kvmodules'], function() {
        applicationController = require("applicationController");
        kony.application.setApplicationInitializationEvents({
            init: applicationController.appInit,
            postappinit: applicationController.postAppInitCallBack,
            showstartupform: function() {
                new kony.mvc.Navigation("loginpage").navigate();
            }
        });
    });
};

function loadResources() {
    kony.theme.packagedthemes(["default"]);
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "eventTypes": appConfig.eventTypes,
    }
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
};

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    //This is the entry point for the application.When Locale comes,Local API call will be the entry point.
    loadResources();
};
debugger;