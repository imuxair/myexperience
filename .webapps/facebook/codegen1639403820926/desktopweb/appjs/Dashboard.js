define("Dashboard", function() {
    return function(controller) {
        function addWidgetsDashboard() {
            this.setDefaultUnit(kony.flex.DP);
            var menubar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "menubar",
                "isVisible": true,
                "layoutType": kony.flex.RESPONSIVE_GRID,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0b14b4bde666d49",
                "top": "0",
                "width": "99.10%",
                "zIndex": 2
            }, {}, {});
            menubar.setDefaultUnit(kony.flex.DP);
            var collogo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "collogo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0df3df70454d442",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 4,
                        "1024": 4,
                        "1366": 4
                    }
                }
            }, {}, {});
            collogo.setDefaultUnit(kony.flex.DP);
            var Image0b4d6135169694d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "60px",
                "id": "Image0b4d6135169694d",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "facebook_logo_removebg_preview.png",
                "top": "0",
                "width": "60px"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextField0h69b95ad0f0b49 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "80%",
                "id": "TextField0h69b95ad0f0b49",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "25%",
                "placeholder": "Search into Facebook",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0j97bf61716254f",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10%",
                "width": "70%"
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [15, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var FlexContainer0e7d0ff2d6d7141 = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "FlexContainer0e7d0ff2d6d7141",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "40px"
            }, {}, {});
            FlexContainer0e7d0ff2d6d7141.setDefaultUnit(kony.flex.DP);
            var Image0b06fadbf07d248 = new kony.ui.Image2({
                "height": "60%",
                "id": "Image0b06fadbf07d248",
                "isVisible": true,
                "left": "28%",
                "skin": "slImage",
                "src": "frame__1_.png",
                "top": "20%",
                "width": "12%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0e7d0ff2d6d7141.add(Image0b06fadbf07d248);
            var FlexContainer0hcf66822e0064f = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "FlexContainer0hcf66822e0064f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 3,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "40px"
            }, {}, {});
            FlexContainer0hcf66822e0064f.setDefaultUnit(kony.flex.DP);
            var Image0he896fdb9e2f4e = new kony.ui.Image2({
                "height": "150dp",
                "id": "Image0he896fdb9e2f4e",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "0",
                "width": "150dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0hcf66822e0064f.add(Image0he896fdb9e2f4e);
            collogo.add(Image0b4d6135169694d, TextField0h69b95ad0f0b49, FlexContainer0e7d0ff2d6d7141, FlexContainer0hcf66822e0064f);
            var mediabutton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "mediabutton",
                "isVisible": true,
                "layoutType": kony.flex.RESPONSIVE_GRID,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ffd0588cfa9247",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 0,
                        "1024": 4,
                        "1366": 4
                    }
                }
            }, {}, {});
            mediabutton.setDefaultUnit(kony.flex.DP);
            var forspace1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "forspace1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 1,
                        "1024": 1,
                        "1366": 1
                    }
                }
            }, {}, {});
            forspace1.setDefaultUnit(kony.flex.DP);
            forspace1.add();
            var home = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "home",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 2,
                        "1024": 2,
                        "1366": 2
                    }
                }
            }, {}, {});
            home.setDefaultUnit(kony.flex.DP);
            var Image0d64e24e704dd46 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0d64e24e704dd46",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__10_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            home.add(Image0d64e24e704dd46);
            var friends = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "friends",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 2,
                        "1024": 2,
                        "1366": 2
                    }
                }
            }, {}, {});
            friends.setDefaultUnit(kony.flex.DP);
            var Image0f5b843cab47a45 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0f5b843cab47a45",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__11_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            friends.add(Image0f5b843cab47a45);
            var fwatch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "fwatch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 2,
                        "1024": 2,
                        "1366": 2
                    }
                }
            }, {}, {});
            fwatch.setDefaultUnit(kony.flex.DP);
            var Image0d7c1b66233b44f = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0d7c1b66233b44f",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__12_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            fwatch.add(Image0d7c1b66233b44f);
            var groups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "groups",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 2,
                        "1024": 2,
                        "1366": 2
                    }
                }
            }, {}, {});
            groups.setDefaultUnit(kony.flex.DP);
            var Image0b06cd49b38e044 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0b06cd49b38e044",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__13_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            groups.add(Image0b06cd49b38e044);
            var gaming = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "gaming",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 2,
                        "1024": 2,
                        "1366": 2
                    }
                }
            }, {}, {});
            gaming.setDefaultUnit(kony.flex.DP);
            var Image0a82f1ae6d2e949 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0a82f1ae6d2e949",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__14_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            gaming.add(Image0a82f1ae6d2e949);
            var forspace2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "forspace2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 1,
                        "1024": 1,
                        "1366": 1
                    }
                }
            }, {}, {});
            forspace2.setDefaultUnit(kony.flex.DP);
            forspace2.add();
            mediabutton.add(forspace1, home, friends, fwatch, groups, gaming, forspace2);
            var contactbtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "contactbtn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0d8b7fd6771d041",
                "top": "0",
                "width": "100%",
                "responsiveConfig": {
                    "offset": {
                        "640": 0,
                        "1024": 0,
                        "1366": 0
                    },
                    "span": {
                        "640": 8,
                        "1024": 4,
                        "1366": 4
                    }
                }
            }, {}, {});
            contactbtn.setDefaultUnit(kony.flex.DP);
            var FlexGroup0c69159e4185041 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "FlexGroup0c69159e4185041",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "flexskin",
                "top": "20%",
                "width": "40px"
            }, {}, {});
            FlexGroup0c69159e4185041.setDefaultUnit(kony.flex.DP);
            var Image0d331432970ab48 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0d331432970ab48",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "baseline_arrow_drop_down_circle_black_24dp.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0c69159e4185041.add(Image0d331432970ab48);
            var FlexGroup0a70f84ee2e7c47 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "FlexGroup0a70f84ee2e7c47",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "flexskin",
                "top": "20%",
                "width": "40px"
            }, {}, {});
            FlexGroup0a70f84ee2e7c47.setDefaultUnit(kony.flex.DP);
            var Image0cda76bc834e049 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0cda76bc834e049",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__16_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0a70f84ee2e7c47.add(Image0cda76bc834e049);
            var FlexGroup0fa9d821e4f724a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "FlexGroup0fa9d821e4f724a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "flexskin",
                "top": "20%",
                "width": "40px"
            }, {}, {});
            FlexGroup0fa9d821e4f724a.setDefaultUnit(kony.flex.DP);
            var Image0j8bcfdc7135249 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0j8bcfdc7135249",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__15_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0fa9d821e4f724a.add(Image0j8bcfdc7135249);
            var FlexGroup0f6add11c756a4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "FlexGroup0f6add11c756a4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "flexskin",
                "top": "22%",
                "width": "40px"
            }, {}, {});
            FlexGroup0f6add11c756a4a.setDefaultUnit(kony.flex.DP);
            var Image0g22b7ea6ed1b46 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0g22b7ea6ed1b46",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "baseline_apps_black_24dp.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0f6add11c756a4a.add(Image0g22b7ea6ed1b46);
            var FlexGroup0e5c5dfcfb18144 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0e5c5dfcfb18144",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px"
            }, {}, {});
            FlexGroup0e5c5dfcfb18144.setDefaultUnit(kony.flex.DP);
            var FlexContainer0f076a40bee1d43 = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "40px",
                "id": "FlexContainer0f076a40bee1d43",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0d313fc87e46841",
                "top": "23%",
                "width": "40px"
            }, {}, {});
            FlexContainer0f076a40bee1d43.setDefaultUnit(kony.flex.DP);
            var Image0e4b02ea8264e4d = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0e4b02ea8264e4d",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "-3dp",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "ghanta"
            });
            FlexContainer0f076a40bee1d43.add(Image0e4b02ea8264e4d);
            var Label0ac2011a9e04945 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0ac2011a9e04945",
                "isVisible": true,
                "left": 0,
                "skin": "CopydefLabel0g35c4dcfc4c14f",
                "text": "Syed",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0e5c5dfcfb18144.add(FlexContainer0f076a40bee1d43, Label0ac2011a9e04945);
            contactbtn.add(FlexGroup0c69159e4185041, FlexGroup0a70f84ee2e7c47, FlexGroup0fa9d821e4f724a, FlexGroup0f6add11c756a4a, FlexGroup0e5c5dfcfb18144);
            menubar.add(collogo, mediabutton, contactbtn);
            var leftbar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "leftbar",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "20%",
                "zIndex": 2
            }, {}, {});
            leftbar.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c356654028714b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "FlexContainer0c356654028714b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0ef137928cff245"
            });
            FlexContainer0c356654028714b.setDefaultUnit(kony.flex.DP);
            var FlexGroup0d446a35f2de44b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "FlexGroup0d446a35f2de44b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "14dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "10%",
                "width": "50px"
            }, {}, {});
            FlexGroup0d446a35f2de44b.setDefaultUnit(kony.flex.DP);
            var Image0d67d01a26cbd43 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0d67d01a26cbd43",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0d446a35f2de44b.add(Image0d67d01a26cbd43);
            var Label0c97cb2c128d440 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0c97cb2c128d440",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Syed Uzair Hussain",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c356654028714b.add(FlexGroup0d446a35f2de44b, Label0c97cb2c128d440);
            var CopyFlexContainer0e22ba468052c43 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyFlexContainer0e22ba468052c43",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0bc4645183acc40"
            });
            CopyFlexContainer0e22ba468052c43.setDefaultUnit(kony.flex.DP);
            var CopyFlexGroup0d94cceb8099545 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "CopyFlexGroup0d94cceb8099545",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "15%",
                "width": "40px"
            }, {}, {});
            CopyFlexGroup0d94cceb8099545.setDefaultUnit(kony.flex.DP);
            var CopyImage0g10fe25f395e44 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0g10fe25f395e44",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "vector.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0d94cceb8099545.add(CopyImage0g10fe25f395e44);
            var CopyLabel0a450f1de7dd444 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0a450f1de7dd444",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Friends",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0e22ba468052c43.add(CopyFlexGroup0d94cceb8099545, CopyLabel0a450f1de7dd444);
            var CopyFlexContainer0df3a47b6c2284d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyFlexContainer0df3a47b6c2284d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0ed2fdd6198aa41"
            });
            CopyFlexContainer0df3a47b6c2284d.setDefaultUnit(kony.flex.DP);
            var CopyFlexGroup0ha7b788a8edc40 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "CopyFlexGroup0ha7b788a8edc40",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "15%",
                "width": "40px"
            }, {}, {});
            CopyFlexGroup0ha7b788a8edc40.setDefaultUnit(kony.flex.DP);
            var CopyImage0f5c52768925747 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0f5c52768925747",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "watch.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0ha7b788a8edc40.add(CopyImage0f5c52768925747);
            var CopyLabel0jc6fe72cc9a247 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0jc6fe72cc9a247",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Watch",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0df3a47b6c2284d.add(CopyFlexGroup0ha7b788a8edc40, CopyLabel0jc6fe72cc9a247);
            var CopyFlexContainer0a5a212f26bfb48 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyFlexContainer0a5a212f26bfb48",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e4bbc921908448",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0ac30216554a247"
            });
            CopyFlexContainer0a5a212f26bfb48.setDefaultUnit(kony.flex.DP);
            var CopyFlexGroup0a9a6ad6c12f44a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "CopyFlexGroup0a9a6ad6c12f44a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "13dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "15%",
                "width": "40px"
            }, {}, {});
            CopyFlexGroup0a9a6ad6c12f44a.setDefaultUnit(kony.flex.DP);
            var CopyImage0gf4fcf294c0145 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0gf4fcf294c0145",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "dontno.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0a9a6ad6c12f44a.add(CopyImage0gf4fcf294c0145);
            var CopyLabel0e1e661b4397f4d = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0e1e661b4397f4d",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Saved",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0a5a212f26bfb48.add(CopyFlexGroup0a9a6ad6c12f44a, CopyLabel0e1e661b4397f4d);
            var CopyFlexContainer0b529ef6f72eb43 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyFlexContainer0b529ef6f72eb43",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0ff4cce84852d4a"
            });
            CopyFlexContainer0b529ef6f72eb43.setDefaultUnit(kony.flex.DP);
            var CopyFlexGroup0bd7f74fe022e47 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "CopyFlexGroup0bd7f74fe022e47",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "14dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "15%",
                "width": "40px"
            }, {}, {});
            CopyFlexGroup0bd7f74fe022e47.setDefaultUnit(kony.flex.DP);
            var CopyImage0id1a965c7cf347 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0id1a965c7cf347",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "memories.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0bd7f74fe022e47.add(CopyImage0id1a965c7cf347);
            var CopyLabel0bac28a0bb4c747 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0bac28a0bb4c747",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Memories",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0b529ef6f72eb43.add(CopyFlexGroup0bd7f74fe022e47, CopyLabel0bac28a0bb4c747);
            var FlexContainer0c6594c57690a4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2dp",
                "id": "FlexContainer0c6594c57690a4f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0bc8757f58d6947",
                "top": "0",
                "width": "97%"
            }, {}, {});
            FlexContainer0c6594c57690a4f.setDefaultUnit(kony.flex.DP);
            FlexContainer0c6594c57690a4f.add();
            var CopyFlexContainer0fc05a1796a6d4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "CopyFlexContainer0fc05a1796a6d4f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0fc05a1796a6d4f.setDefaultUnit(kony.flex.DP);
            var CopyLabel0eb26b5284a9b41 = new kony.ui.Label({
                "centerY": "25%",
                "id": "CopyLabel0eb26b5284a9b41",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Shortcut",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0hbb9ab0c333642 = new kony.ui.Label({
                "id": "Label0hbb9ab0c333642",
                "isVisible": true,
                "left": "85%",
                "skin": "CopydefLabel0ff8b1bd984b349",
                "text": "Edit",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0fc05a1796a6d4f.add(CopyLabel0eb26b5284a9b41, Label0hbb9ab0c333642);
            var CopyFlexContainer0c4557edfcfa44d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyFlexContainer0c4557edfcfa44d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0i3e4e1fc0c4247"
            });
            CopyFlexContainer0c4557edfcfa44d.setDefaultUnit(kony.flex.DP);
            var CopyFlexGroup0c4451ed47b414c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "CopyFlexGroup0c4451ed47b414c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "15%",
                "width": "40px"
            }, {}, {});
            CopyFlexGroup0c4451ed47b414c.setDefaultUnit(kony.flex.DP);
            var CopyImage0bf6aef0740624c = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0bf6aef0740624c",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "gifts_removebg_preview.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0c4451ed47b414c.add(CopyImage0bf6aef0740624c);
            var CopyLabel0a368227352ee4a = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0a368227352ee4a",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Learn Quran Pak",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0c4557edfcfa44d.add(CopyFlexGroup0c4451ed47b414c, CopyLabel0a368227352ee4a);
            var CopyFlexContainer0h6513135e50642 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyFlexContainer0h6513135e50642",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {
                "hoverSkin": "CopyslFbox0ded69e8259644e"
            });
            CopyFlexContainer0h6513135e50642.setDefaultUnit(kony.flex.DP);
            var CopyFlexGroup0b2f7eb161d0842 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "CopyFlexGroup0b2f7eb161d0842",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i30c6dd945d94a",
                "top": "15%",
                "width": "40px"
            }, {}, {});
            CopyFlexGroup0b2f7eb161d0842.setDefaultUnit(kony.flex.DP);
            var CopyImage0i57568e1401948 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0i57568e1401948",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "gifts.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0b2f7eb161d0842.add(CopyImage0i57568e1401948);
            var CopyLabel0e5bae8176ba448 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0e5bae8176ba448",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopydefLabel0b9d9947aa4e44a",
                "text": "Zehrilay Saanp",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0h6513135e50642.add(CopyFlexGroup0b2f7eb161d0842, CopyLabel0e5bae8176ba448);
            leftbar.add(FlexContainer0c356654028714b, CopyFlexContainer0e22ba468052c43, CopyFlexContainer0df3a47b6c2284d, CopyFlexContainer0a5a212f26bfb48, CopyFlexContainer0b529ef6f72eb43, FlexContainer0c6594c57690a4f, CopyFlexContainer0fc05a1796a6d4f, CopyFlexContainer0c4557edfcfa44d, CopyFlexContainer0h6513135e50642);
            var FlexScrollGroup0i1b01947249041 = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "FlexScrollGroup0i1b01947249041",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            FlexScrollGroup0i1b01947249041.setDefaultUnit(kony.flex.DP);
            var FlexContainer0ic702d94ba964e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainer0ic702d94ba964e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "4dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0ic702d94ba964e.setDefaultUnit(kony.flex.DP);
            var FlexGroup0jf0a7a6b427240 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "FlexGroup0jf0a7a6b427240",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "40%"
            }, {}, {});
            FlexGroup0jf0a7a6b427240.setDefaultUnit(kony.flex.DP);
            var FlexContainer0b97174ba5bcd4d = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "220dp",
                "id": "FlexContainer0b97174ba5bcd4d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0b97174ba5bcd4d.setDefaultUnit(kony.flex.DP);
            var FlexScrollContainer0ic40891bce9542 = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "FlexScrollContainer0ic40891bce9542",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": 0,
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            FlexScrollContainer0ic40891bce9542.setDefaultUnit(kony.flex.DP);
            var FlexContainer0df3b738fa7fa48 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0df3b738fa7fa48",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0d0120c91d63c42",
                "top": "0",
                "width": "22%"
            }, {}, {});
            FlexContainer0df3b738fa7fa48.setDefaultUnit(kony.flex.DP);
            var FlexContainer0h0f86dad966643 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70%",
                "id": "FlexContainer0h0f86dad966643",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0h0f86dad966643.setDefaultUnit(kony.flex.DP);
            var Image0g24957b4708e41 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0g24957b4708e41",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0h0f86dad966643.add(Image0g24957b4708e41);
            var FlexContainer0j25a1c1837574c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30%",
                "id": "FlexContainer0j25a1c1837574c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0c2e47e82388b49",
                "top": "70%",
                "width": "100%"
            }, {}, {});
            FlexContainer0j25a1c1837574c.setDefaultUnit(kony.flex.DP);
            var Label0cb8db152c8764c = new kony.ui.Label({
                "centerX": "50%",
                "id": "Label0cb8db152c8764c",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0a000e330689d4c",
                "text": "Create Story",
                "top": "60%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0j25a1c1837574c.add(Label0cb8db152c8764c);
            var FlexContainer0fc2f0282feb449 = new kony.ui.FlexContainer({
                "centerX": "50%",
                "clipBounds": true,
                "height": "50px",
                "id": "FlexContainer0fc2f0282feb449",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0b25f68a9856b44",
                "top": "59%",
                "width": "50px"
            }, {}, {});
            FlexContainer0fc2f0282feb449.setDefaultUnit(kony.flex.DP);
            var Image0hd34e0735b4846 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0hd34e0735b4846",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__9_.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0fc2f0282feb449.add(Image0hd34e0735b4846);
            FlexContainer0df3b738fa7fa48.add(FlexContainer0h0f86dad966643, FlexContainer0j25a1c1837574c, FlexContainer0fc2f0282feb449);
            var CopyFlexContainer0e988cb80675e42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0e988cb80675e42",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d0120c91d63c42",
                "top": "0",
                "width": "22%"
            }, {}, {});
            CopyFlexContainer0e988cb80675e42.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0d469c9094d0e46 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0d469c9094d0e46",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0d469c9094d0e46.setDefaultUnit(kony.flex.DP);
            var CopyImage0a808c5d44d0f42 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0a808c5d44d0f42",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h1.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0d469c9094d0e46.add(CopyImage0a808c5d44d0f42);
            var CopyFlexContainer0f67c87412d4943 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22%",
                "id": "CopyFlexContainer0f67c87412d4943",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0b25f68a9856b44",
                "top": "12%",
                "width": "35%"
            }, {}, {});
            CopyFlexContainer0f67c87412d4943.setDefaultUnit(kony.flex.DP);
            var Image0gdce42502a704a = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0gdce42502a704a",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h2.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f67c87412d4943.add(Image0gdce42502a704a);
            var CopyLabel0f10c2bfb25244b = new kony.ui.Label({
                "id": "CopyLabel0f10c2bfb25244b",
                "isVisible": true,
                "left": "3%",
                "skin": "CopydefLabel0a000e330689d4c",
                "text": "Syed Ali Hassan",
                "top": "85%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0e988cb80675e42.add(CopyFlexContainer0d469c9094d0e46, CopyFlexContainer0f67c87412d4943, CopyLabel0f10c2bfb25244b);
            var CopyFlexContainer0h5e024d0a9d24d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0h5e024d0a9d24d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d0120c91d63c42",
                "top": "0",
                "width": "22%"
            }, {}, {});
            CopyFlexContainer0h5e024d0a9d24d.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0e4105617e21540 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0e4105617e21540",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0e4105617e21540.setDefaultUnit(kony.flex.DP);
            var CopyImage0j560293b18d840 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0j560293b18d840",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h3.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0e4105617e21540.add(CopyImage0j560293b18d840);
            var CopyFlexContainer0jbfa4ac8265d4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22%",
                "id": "CopyFlexContainer0jbfa4ac8265d4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0b25f68a9856b44",
                "top": "12%",
                "width": "35%"
            }, {}, {});
            CopyFlexContainer0jbfa4ac8265d4a.setDefaultUnit(kony.flex.DP);
            var CopyImage0afb5e961f6ab47 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0afb5e961f6ab47",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h4.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0jbfa4ac8265d4a.add(CopyImage0afb5e961f6ab47);
            var CopyLabel0cf96b8cab8244e = new kony.ui.Label({
                "id": "CopyLabel0cf96b8cab8244e",
                "isVisible": true,
                "left": "3%",
                "skin": "CopydefLabel0a000e330689d4c",
                "text": "Munawwar",
                "top": "85%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0h5e024d0a9d24d.add(CopyFlexContainer0e4105617e21540, CopyFlexContainer0jbfa4ac8265d4a, CopyLabel0cf96b8cab8244e);
            var CopyFlexContainer0be61e25c56314a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0be61e25c56314a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d0120c91d63c42",
                "top": "0",
                "width": "22%"
            }, {}, {});
            CopyFlexContainer0be61e25c56314a.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0f1c148b45a9048 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0f1c148b45a9048",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0f1c148b45a9048.setDefaultUnit(kony.flex.DP);
            var CopyImage0e93cbaa9bfe741 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0e93cbaa9bfe741",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h5.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f1c148b45a9048.add(CopyImage0e93cbaa9bfe741);
            var CopyFlexContainer0ca482f8e7ad74e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22%",
                "id": "CopyFlexContainer0ca482f8e7ad74e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0b25f68a9856b44",
                "top": "12%",
                "width": "35%"
            }, {}, {});
            CopyFlexContainer0ca482f8e7ad74e.setDefaultUnit(kony.flex.DP);
            var CopyImage0id41a9848c3241 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0id41a9848c3241",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h6.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0ca482f8e7ad74e.add(CopyImage0id41a9848c3241);
            var CopyLabel0i971eba7783e40 = new kony.ui.Label({
                "id": "CopyLabel0i971eba7783e40",
                "isVisible": true,
                "left": "3%",
                "skin": "CopydefLabel0a000e330689d4c",
                "text": "Zaman",
                "top": "85%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0be61e25c56314a.add(CopyFlexContainer0f1c148b45a9048, CopyFlexContainer0ca482f8e7ad74e, CopyLabel0i971eba7783e40);
            var CopyFlexContainer0edd96a9abaa745 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0edd96a9abaa745",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d0120c91d63c42",
                "top": "0",
                "width": "22%"
            }, {}, {});
            CopyFlexContainer0edd96a9abaa745.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0g46bd8fe6a1349 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0g46bd8fe6a1349",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0g46bd8fe6a1349.setDefaultUnit(kony.flex.DP);
            var CopyImage0cfe195940e584e = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0cfe195940e584e",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h5.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0g46bd8fe6a1349.add(CopyImage0cfe195940e584e);
            var CopyFlexContainer0b8a53f416e024d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22%",
                "id": "CopyFlexContainer0b8a53f416e024d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0b25f68a9856b44",
                "top": "12%",
                "width": "35%"
            }, {}, {});
            CopyFlexContainer0b8a53f416e024d.setDefaultUnit(kony.flex.DP);
            var CopyImage0d98f0e879d4145 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0d98f0e879d4145",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h6.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0b8a53f416e024d.add(CopyImage0d98f0e879d4145);
            var CopyLabel0c5da5a16b8bf40 = new kony.ui.Label({
                "id": "CopyLabel0c5da5a16b8bf40",
                "isVisible": true,
                "left": "3%",
                "skin": "CopydefLabel0a000e330689d4c",
                "text": "Zaman",
                "top": "85%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0edd96a9abaa745.add(CopyFlexContainer0g46bd8fe6a1349, CopyFlexContainer0b8a53f416e024d, CopyLabel0c5da5a16b8bf40);
            FlexScrollContainer0ic40891bce9542.add(FlexContainer0df3b738fa7fa48, CopyFlexContainer0e988cb80675e42, CopyFlexContainer0h5e024d0a9d24d, CopyFlexContainer0be61e25c56314a, CopyFlexContainer0edd96a9abaa745);
            var FlexContainer0f2d840442b1d4d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0f2d840442b1d4d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": -20,
                "skin": "CopyslFbox0e7c64f2bbd5e46",
                "top": 0,
                "width": "40px"
            }, {}, {});
            FlexContainer0f2d840442b1d4d.setDefaultUnit(kony.flex.DP);
            FlexContainer0f2d840442b1d4d.add();
            FlexContainer0b97174ba5bcd4d.add(FlexScrollContainer0ic40891bce9542, FlexContainer0f2d840442b1d4d);
            var FlexContainer0i77a27a8fd6044 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180dp",
                "id": "FlexContainer0i77a27a8fd6044",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e92962d2be7f4f",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0i77a27a8fd6044.setDefaultUnit(kony.flex.DP);
            var FlexContainer0ib1363ea304240 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0ib1363ea304240",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0ib1363ea304240.setDefaultUnit(kony.flex.DP);
            var FlexContainer0ed030780257c4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "FlexContainer0ed030780257c4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "CopyslFbox0c098e121cd5f4a",
                "top": "0",
                "width": "60px"
            }, {}, {});
            FlexContainer0ed030780257c4a.setDefaultUnit(kony.flex.DP);
            var Image0hf1fe368ec544b = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0hf1fe368ec544b",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ed030780257c4a.add(Image0hf1fe368ec544b);
            var TextField0j499af42635845 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "defTextBoxFocus",
                "height": "60px",
                "id": "TextField0j499af42635845",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "17%",
                "placeholder": "What's on your mind, Syed?",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0d85d9905be2d43",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0",
                "width": "80%"
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            FlexContainer0ib1363ea304240.add(FlexContainer0ed030780257c4a, TextField0j499af42635845);
            var FlexContainer0fcefcf63b7fd41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0fcefcf63b7fd41",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0fcefcf63b7fd41.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e68601d7ce7e42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0e68601d7ce7e42",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "33.33%"
            }, {}, {});
            FlexContainer0e68601d7ce7e42.setDefaultUnit(kony.flex.DP);
            var Image0df6a55437e8140 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40dp",
                "id": "Image0df6a55437e8140",
                "isVisible": true,
                "left": "16%",
                "skin": "slImage",
                "src": "frame__17_.png",
                "top": "0",
                "width": "35dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dcbd10b02fb848 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dcbd10b02fb848",
                "isVisible": true,
                "left": "36%",
                "skin": "CopydefLabel0d08ba1cedd9d4e",
                "text": "Live Video",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0e68601d7ce7e42.add(Image0df6a55437e8140, Label0dcbd10b02fb848);
            var CopyFlexContainer0f3b339fb45f34b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0f3b339fb45f34b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "33.33%"
            }, {}, {});
            CopyFlexContainer0f3b339fb45f34b.setDefaultUnit(kony.flex.DP);
            var CopyImage0ea14c690a1c948 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40dp",
                "id": "CopyImage0ea14c690a1c948",
                "isVisible": true,
                "left": "14%",
                "skin": "slImage",
                "src": "frame__18_.png",
                "top": "0",
                "width": "35dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0a32599046b4744 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0a32599046b4744",
                "isVisible": true,
                "left": "36%",
                "skin": "CopydefLabel0d08ba1cedd9d4e",
                "text": "photo/video",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f3b339fb45f34b.add(CopyImage0ea14c690a1c948, CopyLabel0a32599046b4744);
            var CopyFlexContainer0e0245240d43748 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0e0245240d43748",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "33.33%"
            }, {}, {});
            CopyFlexContainer0e0245240d43748.setDefaultUnit(kony.flex.DP);
            var CopyImage0ec8355f9064444 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40dp",
                "id": "CopyImage0ec8355f9064444",
                "isVisible": true,
                "left": "8%",
                "skin": "slImage",
                "src": "frame__19_.png",
                "top": "0",
                "width": "35dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0g46cb8246a614c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0g46cb8246a614c",
                "isVisible": true,
                "left": "28%",
                "skin": "CopydefLabel0d08ba1cedd9d4e",
                "text": "feeling/activity",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0e0245240d43748.add(CopyImage0ec8355f9064444, CopyLabel0g46cb8246a614c);
            FlexContainer0fcefcf63b7fd41.add(FlexContainer0e68601d7ce7e42, CopyFlexContainer0f3b339fb45f34b, CopyFlexContainer0e0245240d43748);
            FlexContainer0i77a27a8fd6044.add(FlexContainer0ib1363ea304240, FlexContainer0fcefcf63b7fd41);
            var FlexContainer0bd932c16491f4e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "FlexContainer0bd932c16491f4e",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0cdd5841b85074c",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0bd932c16491f4e.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e57a209f31cf4b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "FlexContainer0e57a209f31cf4b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i7d57f4045524f",
                "top": "0",
                "width": "190px"
            }, {}, {});
            FlexContainer0e57a209f31cf4b.setDefaultUnit(kony.flex.DP);
            var Image0e85f5379f0194c = new kony.ui.Image2({
                "height": "50%",
                "id": "Image0e85f5379f0194c",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__17_.png",
                "top": "25%",
                "width": "25%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0j5387c38de5641 = new kony.ui.Label({
                "id": "Label0j5387c38de5641",
                "isVisible": true,
                "left": "22%",
                "skin": "CopydefLabel0i8baa54b23224c",
                "text": "Create room",
                "top": "29%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0e57a209f31cf4b.add(Image0e85f5379f0194c, Label0j5387c38de5641);
            var FlexContainer0f605e30e42a74e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "FlexContainer0f605e30e42a74e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f22d70e92f7d44",
                "top": "0",
                "width": "60px"
            }, {}, {});
            FlexContainer0f605e30e42a74e.setDefaultUnit(kony.flex.DP);
            var Image0ada5793c0f6c4b = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0ada5793c0f6c4b",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h1.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0f605e30e42a74e.add(Image0ada5793c0f6c4b);
            var CopyFlexContainer0f3dec778befa44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "CopyFlexContainer0f3dec778befa44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f22d70e92f7d44",
                "top": "0",
                "width": "60px"
            }, {}, {});
            CopyFlexContainer0f3dec778befa44.setDefaultUnit(kony.flex.DP);
            var CopyImage0b287d6bc933144 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0b287d6bc933144",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h2.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f3dec778befa44.add(CopyImage0b287d6bc933144);
            var CopyFlexContainer0b14fdb6d8a2c48 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "CopyFlexContainer0b14fdb6d8a2c48",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f22d70e92f7d44",
                "top": "0",
                "width": "60px"
            }, {}, {});
            CopyFlexContainer0b14fdb6d8a2c48.setDefaultUnit(kony.flex.DP);
            var CopyImage0dac075b363fc43 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0dac075b363fc43",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h3.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0b14fdb6d8a2c48.add(CopyImage0dac075b363fc43);
            var CopyFlexContainer0a658b60a8cc847 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "CopyFlexContainer0a658b60a8cc847",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f22d70e92f7d44",
                "top": "0",
                "width": "60px"
            }, {}, {});
            CopyFlexContainer0a658b60a8cc847.setDefaultUnit(kony.flex.DP);
            var CopyImage0g6e40b26d58740 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0g6e40b26d58740",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h4.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0a658b60a8cc847.add(CopyImage0g6e40b26d58740);
            var CopyFlexContainer0cc096727327141 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "CopyFlexContainer0cc096727327141",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f22d70e92f7d44",
                "top": "0",
                "width": "60px"
            }, {}, {});
            CopyFlexContainer0cc096727327141.setDefaultUnit(kony.flex.DP);
            var CopyImage0bb033f54e9034d = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0bb033f54e9034d",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h5.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0cc096727327141.add(CopyImage0bb033f54e9034d);
            var CopyFlexContainer0acd38202c0d344 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "CopyFlexContainer0acd38202c0d344",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f22d70e92f7d44",
                "top": "0",
                "width": "60px"
            }, {}, {});
            CopyFlexContainer0acd38202c0d344.setDefaultUnit(kony.flex.DP);
            var CopyImage0g0ae1c8d5b224c = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0g0ae1c8d5b224c",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h6.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0acd38202c0d344.add(CopyImage0g0ae1c8d5b224c);
            FlexContainer0bd932c16491f4e.add(FlexContainer0e57a209f31cf4b, FlexContainer0f605e30e42a74e, CopyFlexContainer0f3dec778befa44, CopyFlexContainer0b14fdb6d8a2c48, CopyFlexContainer0a658b60a8cc847, CopyFlexContainer0cc096727327141, CopyFlexContainer0acd38202c0d344);
            var FlexContainer0g5051f72c1c94c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "550dp",
                "id": "FlexContainer0g5051f72c1c94c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0j09e4ed05ac34d",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0g5051f72c1c94c.setDefaultUnit(kony.flex.DP);
            var FlexContainer0ibd1e95d2f8047 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "17%",
                "id": "FlexContainer0ibd1e95d2f8047",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e43f3803e31a42",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0ibd1e95d2f8047.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c1dbe2f4d60647 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "FlexContainer0c1dbe2f4d60647",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a4a96143a2ed4e",
                "top": "7dp",
                "width": "70px"
            }, {}, {});
            FlexContainer0c1dbe2f4d60647.setDefaultUnit(kony.flex.DP);
            FlexContainer0c1dbe2f4d60647.add();
            var Label0dc1f895a5fec45 = new kony.ui.Label({
                "id": "Label0dc1f895a5fec45",
                "isVisible": true,
                "left": "87dp",
                "skin": "CopydefLabel0f464d985e58a44",
                "text": "Lashkar",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0cd4ab9474f4540 = new kony.ui.Label({
                "id": "Label0cd4ab9474f4540",
                "isVisible": true,
                "left": "87dp",
                "skin": "CopydefLabel0fb157fa832fc4d",
                "text": "1hr",
                "top": 42,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0a2ad2b9e67164f = new kony.ui.Label({
                "id": "Label0a2ad2b9e67164f",
                "isVisible": true,
                "left": "90%",
                "skin": "CopydefLabel0f36f0f74508340",
                "text": "...",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ibd1e95d2f8047.add(FlexContainer0c1dbe2f4d60647, Label0dc1f895a5fec45, Label0cd4ab9474f4540, Label0a2ad2b9e67164f);
            var FlexContainer0adc22618f8314e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "17%",
                "id": "FlexContainer0adc22618f8314e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0b281e11e44ec43",
                "top": "83%",
                "width": "100%"
            }, {}, {});
            FlexContainer0adc22618f8314e.setDefaultUnit(kony.flex.DP);
            var FlexContainer0a0c876fa4f9c45 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48%",
                "id": "FlexContainer0a0c876fa4f9c45",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0a0c876fa4f9c45.setDefaultUnit(kony.flex.DP);
            var Label0g292a8a13d3841 = new kony.ui.Label({
                "id": "Label0g292a8a13d3841",
                "isVisible": true,
                "left": "85%",
                "skin": "CopydefLabel0d77502b17c0841",
                "text": "12 Shares",
                "top": "25%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "CopydefLabel0j0194495e7884a"
            });
            var FlexContainer0j27c2e52d6c649 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0j27c2e52d6c649",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "20%"
            }, {}, {});
            FlexContainer0j27c2e52d6c649.setDefaultUnit(kony.flex.DP);
            var Image0dd695a2c4b2243 = new kony.ui.Image2({
                "height": "45%",
                "id": "Image0dd695a2c4b2243",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "baseline_thumb_up_black_24dp.png",
                "top": "25%",
                "width": "30%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0b01fe04042654a = new kony.ui.Label({
                "id": "Label0b01fe04042654a",
                "isVisible": true,
                "left": "30%",
                "skin": "CopydefLabel0hf2e0e4666c645",
                "text": "22",
                "top": "30%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0j27c2e52d6c649.add(Image0dd695a2c4b2243, Label0b01fe04042654a);
            FlexContainer0a0c876fa4f9c45.add(Label0g292a8a13d3841, FlexContainer0j27c2e52d6c649);
            var CopyFlexContainer0jb1d293ddaf142 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2%",
                "id": "CopyFlexContainer0jb1d293ddaf142",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2.50%",
                "isModalContainer": false,
                "skin": "CopyslFbox0a00a476f077b46",
                "top": "48%",
                "width": "95%"
            }, {}, {});
            CopyFlexContainer0jb1d293ddaf142.setDefaultUnit(kony.flex.DP);
            CopyFlexContainer0jb1d293ddaf142.add();
            var FlexContainer0j012843f9cf940 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0j012843f9cf940",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%"
            }, {}, {});
            FlexContainer0j012843f9cf940.setDefaultUnit(kony.flex.DP);
            var FlexContainer0dc963190fb6e43 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0dc963190fb6e43",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "30%"
            }, {}, {
                "hoverSkin": "CopyslFbox0i5995fd3876d42"
            });
            FlexContainer0dc963190fb6e43.setDefaultUnit(kony.flex.DP);
            var Image0g3ed2d1a181147 = new kony.ui.Image2({
                "height": "50%",
                "id": "Image0g3ed2d1a181147",
                "isVisible": true,
                "left": "21%",
                "right": "5dp",
                "skin": "slImage",
                "src": "baseline_thumb_up_black_24dp.png",
                "top": "21%",
                "width": "15%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0e92a9ab812374a = new kony.ui.Label({
                "id": "Label0e92a9ab812374a",
                "isVisible": true,
                "left": "2dp",
                "skin": "CopydefLabel0d27e39842ad74a",
                "text": "Like",
                "top": "30%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0dc963190fb6e43.add(Image0g3ed2d1a181147, Label0e92a9ab812374a);
            var FlexContainer0g9249d7aa88b45 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0g9249d7aa88b45",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "30%"
            }, {}, {
                "hoverSkin": "CopyslFbox0ec3d8f2f5c4e40"
            });
            FlexContainer0g9249d7aa88b45.setDefaultUnit(kony.flex.DP);
            var Image0faa3f58e2b274b = new kony.ui.Image2({
                "height": "70%",
                "id": "Image0faa3f58e2b274b",
                "isVisible": true,
                "left": "10%",
                "skin": "slImage",
                "src": "icons8_comments_50.png",
                "top": "15%",
                "width": "30%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0ccbf72acf4a14b = new kony.ui.Label({
                "id": "Label0ccbf72acf4a14b",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0da2e0f3bc9bc40",
                "text": "Comment",
                "top": "25%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0g9249d7aa88b45.add(Image0faa3f58e2b274b, Label0ccbf72acf4a14b);
            var FlexContainer0a1c9c3338e3342 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0a1c9c3338e3342",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "30%"
            }, {}, {
                "hoverSkin": "CopyslFbox0cbaa8abc343d40"
            });
            FlexContainer0a1c9c3338e3342.setDefaultUnit(kony.flex.DP);
            var Image0f43e5a1a9df44b = new kony.ui.Image2({
                "zoomEnabled": false,
                "zoomValue": 1,
                "height": "80%",
                "id": "Image0f43e5a1a9df44b",
                "isVisible": true,
                "left": "22%",
                "skin": "slImage",
                "src": "frame__21_.png",
                "top": "13%",
                "width": "40%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0aec56b4700a546 = new kony.ui.Label({
                "id": "Label0aec56b4700a546",
                "isVisible": true,
                "left": "-18dp",
                "skin": "CopydefLabel0d4352bbaf6ce41",
                "text": "Share",
                "top": "30%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0a1c9c3338e3342.add(Image0f43e5a1a9df44b, Label0aec56b4700a546);
            var FlexContainer0h6c3ad94d06145 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0h6c3ad94d06145",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "10%"
            }, {}, {});
            FlexContainer0h6c3ad94d06145.setDefaultUnit(kony.flex.DP);
            var FlexContainer0id9dac8433294c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0id9dac8433294c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0j04b8c7f946e44",
                "top": "25%",
                "width": "50%"
            }, {}, {});
            FlexContainer0id9dac8433294c.setDefaultUnit(kony.flex.DP);
            var Image0g9e6d2a43f6241 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0g9e6d2a43f6241",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0id9dac8433294c.add(Image0g9e6d2a43f6241);
            var Image0fefedb56aedf49 = new kony.ui.Image2({
                "height": "50%",
                "id": "Image0fefedb56aedf49",
                "isVisible": true,
                "left": "50%",
                "skin": "slImage",
                "src": "baseline_arrow_drop_down_circle_black_24dp.png",
                "top": "25%",
                "width": "50%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0h6c3ad94d06145.add(FlexContainer0id9dac8433294c, Image0fefedb56aedf49);
            FlexContainer0j012843f9cf940.add(FlexContainer0dc963190fb6e43, FlexContainer0g9249d7aa88b45, FlexContainer0a1c9c3338e3342, FlexContainer0h6c3ad94d06145);
            FlexContainer0adc22618f8314e.add(FlexContainer0a0c876fa4f9c45, CopyFlexContainer0jb1d293ddaf142, FlexContainer0j012843f9cf940);
            var Image0f2afc0c267c14e = new kony.ui.Image2({
                "height": "66%",
                "id": "Image0f2afc0c267c14e",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "status.jpg",
                "top": "17%",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0g5051f72c1c94c.add(FlexContainer0ibd1e95d2f8047, FlexContainer0adc22618f8314e, Image0f2afc0c267c14e);
            var CopyFlexContainer0ie5c406d9a664d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "550dp",
                "id": "CopyFlexContainer0ie5c406d9a664d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0j09e4ed05ac34d",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0ie5c406d9a664d.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0gae4382adb4f4e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "17%",
                "id": "CopyFlexContainer0gae4382adb4f4e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e43f3803e31a42",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0gae4382adb4f4e.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0e219afd9e64141 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "CopyFlexContainer0e219afd9e64141",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a4a96143a2ed4e",
                "top": "7dp",
                "width": "70px"
            }, {}, {});
            CopyFlexContainer0e219afd9e64141.setDefaultUnit(kony.flex.DP);
            CopyFlexContainer0e219afd9e64141.add();
            var CopyLabel0bec842078f3141 = new kony.ui.Label({
                "id": "CopyLabel0bec842078f3141",
                "isVisible": true,
                "left": "87dp",
                "skin": "CopydefLabel0f464d985e58a44",
                "text": "Lashkar",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0g2846dd92c9c4c = new kony.ui.Label({
                "id": "CopyLabel0g2846dd92c9c4c",
                "isVisible": true,
                "left": "87dp",
                "skin": "CopydefLabel0fb157fa832fc4d",
                "text": "1hr",
                "top": 42,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0j192811b760b48 = new kony.ui.Label({
                "id": "CopyLabel0j192811b760b48",
                "isVisible": true,
                "left": "90%",
                "skin": "CopydefLabel0f36f0f74508340",
                "text": "...",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0gae4382adb4f4e.add(CopyFlexContainer0e219afd9e64141, CopyLabel0bec842078f3141, CopyLabel0g2846dd92c9c4c, CopyLabel0j192811b760b48);
            var CopyFlexContainer0j9e0a2c67d6948 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "17%",
                "id": "CopyFlexContainer0j9e0a2c67d6948",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0b281e11e44ec43",
                "top": "83%",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0j9e0a2c67d6948.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0h229e881704f40 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48%",
                "id": "CopyFlexContainer0h229e881704f40",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0h229e881704f40.setDefaultUnit(kony.flex.DP);
            var CopyLabel0ec1a0f00ec6745 = new kony.ui.Label({
                "id": "CopyLabel0ec1a0f00ec6745",
                "isVisible": true,
                "left": "85%",
                "skin": "CopydefLabel0d77502b17c0841",
                "text": "12 Shares",
                "top": "25%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyFlexContainer0i38425f554a748 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0i38425f554a748",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "20%"
            }, {}, {});
            CopyFlexContainer0i38425f554a748.setDefaultUnit(kony.flex.DP);
            var CopyImage0a8316dc20fea43 = new kony.ui.Image2({
                "height": "40%",
                "id": "CopyImage0a8316dc20fea43",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "baseline_thumb_up_black_24dp.png",
                "top": "25%",
                "width": "30%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0j308cdeb529442 = new kony.ui.Label({
                "id": "CopyLabel0j308cdeb529442",
                "isVisible": true,
                "left": "30%",
                "skin": "CopydefLabel0hf2e0e4666c645",
                "text": "22",
                "top": "30%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0i38425f554a748.add(CopyImage0a8316dc20fea43, CopyLabel0j308cdeb529442);
            CopyFlexContainer0h229e881704f40.add(CopyLabel0ec1a0f00ec6745, CopyFlexContainer0i38425f554a748);
            var CopyFlexContainer0a35073b14b2143 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2%",
                "id": "CopyFlexContainer0a35073b14b2143",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2.50%",
                "isModalContainer": false,
                "skin": "CopyslFbox0a00a476f077b46",
                "top": "48%",
                "width": "95%"
            }, {}, {});
            CopyFlexContainer0a35073b14b2143.setDefaultUnit(kony.flex.DP);
            CopyFlexContainer0a35073b14b2143.add();
            var CopyFlexContainer0h7d84cd087024d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "CopyFlexContainer0h7d84cd087024d",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0h7d84cd087024d.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0e0130d64dd9e42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0e0130d64dd9e42",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "30%"
            }, {}, {});
            CopyFlexContainer0e0130d64dd9e42.setDefaultUnit(kony.flex.DP);
            var CopyImage0a1b41f02a2a345 = new kony.ui.Image2({
                "height": "60%",
                "id": "CopyImage0a1b41f02a2a345",
                "isVisible": true,
                "left": "10%",
                "skin": "slImage",
                "src": "baseline_thumb_up_black_24dp.png",
                "top": "17%",
                "width": "15%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0d690efe57ac145 = new kony.ui.Label({
                "id": "CopyLabel0d690efe57ac145",
                "isVisible": true,
                "left": "3dp",
                "skin": "CopydefLabel0d27e39842ad74a",
                "text": "Like",
                "top": "33%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0e0130d64dd9e42.add(CopyImage0a1b41f02a2a345, CopyLabel0d690efe57ac145);
            var CopyFlexContainer0b0a891d8767a44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0b0a891d8767a44",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "30%"
            }, {}, {});
            CopyFlexContainer0b0a891d8767a44.setDefaultUnit(kony.flex.DP);
            var CopyImage0f7a56dd9e53c4c = new kony.ui.Image2({
                "height": "70%",
                "id": "CopyImage0f7a56dd9e53c4c",
                "isVisible": true,
                "left": "10%",
                "skin": "slImage",
                "src": "icons8_comments_50.png",
                "top": "15%",
                "width": "30%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0c1b5dc7c8ba641 = new kony.ui.Label({
                "id": "CopyLabel0c1b5dc7c8ba641",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0da2e0f3bc9bc40",
                "text": "Comment",
                "top": "25%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0b0a891d8767a44.add(CopyImage0f7a56dd9e53c4c, CopyLabel0c1b5dc7c8ba641);
            var CopyFlexContainer0j9aeb3ee1f8a4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0j9aeb3ee1f8a4f",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "30%"
            }, {}, {});
            CopyFlexContainer0j9aeb3ee1f8a4f.setDefaultUnit(kony.flex.DP);
            var CopyImage0h6f3a758a1fa4e = new kony.ui.Image2({
                "height": "90%",
                "id": "CopyImage0h6f3a758a1fa4e",
                "isVisible": true,
                "left": "22%",
                "skin": "slImage",
                "src": "frame__21_.png",
                "top": "2%",
                "width": "30%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0h71dd3cc78a744 = new kony.ui.Label({
                "id": "CopyLabel0h71dd3cc78a744",
                "isVisible": true,
                "left": "-12dp",
                "skin": "CopydefLabel0d4352bbaf6ce41",
                "text": "Share",
                "top": "27%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0j9aeb3ee1f8a4f.add(CopyImage0h6f3a758a1fa4e, CopyLabel0h71dd3cc78a744);
            var CopyFlexContainer0d25c5fffc23b41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "CopyFlexContainer0d25c5fffc23b41",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "10%"
            }, {}, {});
            CopyFlexContainer0d25c5fffc23b41.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0f78008d9c62f46 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "CopyFlexContainer0f78008d9c62f46",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0j04b8c7f946e44",
                "top": "25%",
                "width": "50%"
            }, {}, {});
            CopyFlexContainer0f78008d9c62f46.setDefaultUnit(kony.flex.DP);
            var CopyImage0ge92b41c154e48 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0ge92b41c154e48",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "syed.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f78008d9c62f46.add(CopyImage0ge92b41c154e48);
            var CopyImage0ab1335b3abd34b = new kony.ui.Image2({
                "height": "50%",
                "id": "CopyImage0ab1335b3abd34b",
                "isVisible": true,
                "left": "50%",
                "skin": "slImage",
                "src": "baseline_arrow_drop_down_circle_black_24dp.png",
                "top": "25%",
                "width": "50%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0d25c5fffc23b41.add(CopyFlexContainer0f78008d9c62f46, CopyImage0ab1335b3abd34b);
            CopyFlexContainer0h7d84cd087024d.add(CopyFlexContainer0e0130d64dd9e42, CopyFlexContainer0b0a891d8767a44, CopyFlexContainer0j9aeb3ee1f8a4f, CopyFlexContainer0d25c5fffc23b41);
            CopyFlexContainer0j9e0a2c67d6948.add(CopyFlexContainer0h229e881704f40, CopyFlexContainer0a35073b14b2143, CopyFlexContainer0h7d84cd087024d);
            CopyFlexContainer0ie5c406d9a664d.add(CopyFlexContainer0gae4382adb4f4e, CopyFlexContainer0j9e0a2c67d6948);
            var FlexContainer0jd1d3829382843 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "500dp",
                "id": "FlexContainer0jd1d3829382843",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0aab1ecdf3d4540",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0jd1d3829382843.setDefaultUnit(kony.flex.DP);
            var FlexContainer0dfad5e35b29f48 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15%",
                "id": "FlexContainer0dfad5e35b29f48",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0dfad5e35b29f48.setDefaultUnit(kony.flex.DP);
            var FlexContainer0a4b2955668244b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0a4b2955668244b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "50%"
            }, {}, {});
            FlexContainer0a4b2955668244b.setDefaultUnit(kony.flex.DP);
            var Label0dec6e576de2948 = new kony.ui.Label({
                "id": "Label0dec6e576de2948",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0h5f02bd8fe5c43",
                "text": "People you may Know",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0a4b2955668244b.add(Label0dec6e576de2948);
            var FlexContainer0ae835e23349d41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "FlexContainer0ae835e23349d41",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "3%",
                "skin": "slFbox",
                "top": "0",
                "width": "5%"
            }, {}, {});
            FlexContainer0ae835e23349d41.setDefaultUnit(kony.flex.DP);
            var Label0b7c331d9b9ee45 = new kony.ui.Label({
                "id": "Label0b7c331d9b9ee45",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0b28dd37a3bea4f",
                "text": "...",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ae835e23349d41.add(Label0b7c331d9b9ee45);
            FlexContainer0dfad5e35b29f48.add(FlexContainer0a4b2955668244b, FlexContainer0ae835e23349d41);
            var FlexScrollContainer0c29e8095586a44 = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "70%",
                "horizontalScrollIndicator": true,
                "id": "FlexScrollContainer0c29e8095586a44",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15%",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            FlexScrollContainer0c29e8095586a44.setDefaultUnit(kony.flex.DP);
            var FlexContainer0fcd441ec708c44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "FlexContainer0fcd441ec708c44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d15757def3f54f",
                "top": "0",
                "width": "25%"
            }, {}, {});
            FlexContainer0fcd441ec708c44.setDefaultUnit(kony.flex.DP);
            var FlexContainer0d95415dffacc41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65%",
                "id": "FlexContainer0d95415dffacc41",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            FlexContainer0d95415dffacc41.setDefaultUnit(kony.flex.DP);
            var Image0ea59cd0967004d = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0ea59cd0967004d",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "iqbal.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0d95415dffacc41.add(Image0ea59cd0967004d);
            var FlexContainer0c52d3aab1f9048 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35%",
                "id": "FlexContainer0c52d3aab1f9048",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65%",
                "width": "100%"
            }, {}, {});
            FlexContainer0c52d3aab1f9048.setDefaultUnit(kony.flex.DP);
            var Label0cf7f7027846347 = new kony.ui.Label({
                "id": "Label0cf7f7027846347",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0iced092d40224d",
                "text": "Muhammad Junaid",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dc4692bbbd854a = new kony.ui.Label({
                "id": "Label0dc4692bbbd854a",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0baa0d1e4413e47",
                "text": "1 Mutual friend",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Button0ce3d45429beb46 = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "40dp",
                "id": "Button0ce3d45429beb46",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefBtnNormal0ed7abba1588e4e",
                "text": "Add Friend",
                "top": "10dp",
                "width": "90%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Image0d5fdcb71470740 = new kony.ui.Image2({
                "height": "20%",
                "id": "Image0d5fdcb71470740",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "frame__11_.png",
                "top": "-30dp",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c52d3aab1f9048.add(Label0cf7f7027846347, Label0dc4692bbbd854a, Button0ce3d45429beb46, Image0d5fdcb71470740);
            FlexContainer0fcd441ec708c44.add(FlexContainer0d95415dffacc41, FlexContainer0c52d3aab1f9048);
            var CopyFlexContainer0eac2f338f2e94e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "CopyFlexContainer0eac2f338f2e94e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d15757def3f54f",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0eac2f338f2e94e.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0caa97ed7c77140 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65%",
                "id": "CopyFlexContainer0caa97ed7c77140",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0caa97ed7c77140.setDefaultUnit(kony.flex.DP);
            var CopyImage0ie8995d69b7e49 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0ie8995d69b7e49",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "one.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0caa97ed7c77140.add(CopyImage0ie8995d69b7e49);
            var CopyFlexContainer0e93bdfbfe9a345 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35%",
                "id": "CopyFlexContainer0e93bdfbfe9a345",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65%",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0e93bdfbfe9a345.setDefaultUnit(kony.flex.DP);
            var CopyLabel0a38bdddc30c94a = new kony.ui.Label({
                "id": "CopyLabel0a38bdddc30c94a",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0iced092d40224d",
                "text": "Khalil",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0de24e6e7a6364f = new kony.ui.Label({
                "id": "CopyLabel0de24e6e7a6364f",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0baa0d1e4413e47",
                "text": "1 Mutual friend",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyButton0d429de45e77340 = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "40dp",
                "id": "CopyButton0d429de45e77340",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefBtnNormal0ed7abba1588e4e",
                "text": "Add Friend",
                "top": "10dp",
                "width": "90%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyImage0d44baacc6bc64c = new kony.ui.Image2({
                "height": "20%",
                "id": "CopyImage0d44baacc6bc64c",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "frame__11_.png",
                "top": "-30dp",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0e93bdfbfe9a345.add(CopyLabel0a38bdddc30c94a, CopyLabel0de24e6e7a6364f, CopyButton0d429de45e77340, CopyImage0d44baacc6bc64c);
            CopyFlexContainer0eac2f338f2e94e.add(CopyFlexContainer0caa97ed7c77140, CopyFlexContainer0e93bdfbfe9a345);
            var CopyFlexContainer0h67a6128baab4b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "CopyFlexContainer0h67a6128baab4b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d15757def3f54f",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0h67a6128baab4b.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0d43c264eec5b4d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65%",
                "id": "CopyFlexContainer0d43c264eec5b4d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0d43c264eec5b4d.setDefaultUnit(kony.flex.DP);
            var CopyImage0i12470930fbc4a = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0i12470930fbc4a",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "two.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0d43c264eec5b4d.add(CopyImage0i12470930fbc4a);
            var CopyFlexContainer0d4c340b92c0443 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35%",
                "id": "CopyFlexContainer0d4c340b92c0443",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65%",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0d4c340b92c0443.setDefaultUnit(kony.flex.DP);
            var CopyLabel0d80c11ad26f14b = new kony.ui.Label({
                "id": "CopyLabel0d80c11ad26f14b",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0iced092d40224d",
                "text": "Maham",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0ccd8e13c6efb4b = new kony.ui.Label({
                "id": "CopyLabel0ccd8e13c6efb4b",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0baa0d1e4413e47",
                "text": "1 Mutual friend",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyButton0fef16789a85f46 = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "40dp",
                "id": "CopyButton0fef16789a85f46",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefBtnNormal0ed7abba1588e4e",
                "text": "Add Friend",
                "top": "10dp",
                "width": "90%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyImage0f150100b8d5c4b = new kony.ui.Image2({
                "height": "20%",
                "id": "CopyImage0f150100b8d5c4b",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "frame__11_.png",
                "top": "-30dp",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0d4c340b92c0443.add(CopyLabel0d80c11ad26f14b, CopyLabel0ccd8e13c6efb4b, CopyButton0fef16789a85f46, CopyImage0f150100b8d5c4b);
            CopyFlexContainer0h67a6128baab4b.add(CopyFlexContainer0d43c264eec5b4d, CopyFlexContainer0d4c340b92c0443);
            var CopyFlexContainer0d62ac6ee86e44b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "CopyFlexContainer0d62ac6ee86e44b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d15757def3f54f",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0d62ac6ee86e44b.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0a0fbfaf4446041 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65%",
                "id": "CopyFlexContainer0a0fbfaf4446041",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0a0fbfaf4446041.setDefaultUnit(kony.flex.DP);
            var CopyImage0ab0c1a0b508e4f = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0ab0c1a0b508e4f",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "three.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0a0fbfaf4446041.add(CopyImage0ab0c1a0b508e4f);
            var CopyFlexContainer0jb7067bc3c494d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35%",
                "id": "CopyFlexContainer0jb7067bc3c494d",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65%",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0jb7067bc3c494d.setDefaultUnit(kony.flex.DP);
            var CopyLabel0f0a0d8b3ec2f44 = new kony.ui.Label({
                "id": "CopyLabel0f0a0d8b3ec2f44",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0iced092d40224d",
                "text": "Kamran",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0h918d7f7785746 = new kony.ui.Label({
                "id": "CopyLabel0h918d7f7785746",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0baa0d1e4413e47",
                "text": "1 Mutual friend",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyButton0c88a330d76684a = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "defBtnFocus",
                "height": "40dp",
                "id": "CopyButton0c88a330d76684a",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefBtnNormal0ed7abba1588e4e",
                "text": "Add Friend",
                "top": "10dp",
                "width": "90%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyImage0f633540a807d4e = new kony.ui.Image2({
                "height": "20%",
                "id": "CopyImage0f633540a807d4e",
                "isVisible": true,
                "left": "7dp",
                "skin": "slImage",
                "src": "frame__11_.png",
                "top": "-30dp",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0jb7067bc3c494d.add(CopyLabel0f0a0d8b3ec2f44, CopyLabel0h918d7f7785746, CopyButton0c88a330d76684a, CopyImage0f633540a807d4e);
            CopyFlexContainer0d62ac6ee86e44b.add(CopyFlexContainer0a0fbfaf4446041, CopyFlexContainer0jb7067bc3c494d);
            FlexScrollContainer0c29e8095586a44.add(FlexContainer0fcd441ec708c44, CopyFlexContainer0eac2f338f2e94e, CopyFlexContainer0h67a6128baab4b, CopyFlexContainer0d62ac6ee86e44b);
            var CopyFlexContainer0d6ef59d7f5b34e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15%",
                "id": "CopyFlexContainer0d6ef59d7f5b34e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85%",
                "width": "100%"
            }, {}, {});
            CopyFlexContainer0d6ef59d7f5b34e.setDefaultUnit(kony.flex.DP);
            var FlexContainer0dcf4201c396046 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0dcf4201c396046",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "20%"
            }, {}, {});
            FlexContainer0dcf4201c396046.setDefaultUnit(kony.flex.DP);
            var Label0jc3d5de2e18c4c = new kony.ui.Label({
                "id": "Label0jc3d5de2e18c4c",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0ecb06fa7ae4849",
                "text": "See all",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0dcf4201c396046.add(Label0jc3d5de2e18c4c);
            CopyFlexContainer0d6ef59d7f5b34e.add(FlexContainer0dcf4201c396046);
            FlexContainer0jd1d3829382843.add(FlexContainer0dfad5e35b29f48, FlexScrollContainer0c29e8095586a44, CopyFlexContainer0d6ef59d7f5b34e);
            FlexGroup0jf0a7a6b427240.add(FlexContainer0b97174ba5bcd4d, FlexContainer0i77a27a8fd6044, FlexContainer0bd932c16491f4e, FlexContainer0g5051f72c1c94c, CopyFlexContainer0ie5c406d9a664d, FlexContainer0jd1d3829382843);
            FlexContainer0ic702d94ba964e.add(FlexGroup0jf0a7a6b427240);
            FlexScrollGroup0i1b01947249041.add(FlexContainer0ic702d94ba964e);
            var rightbar = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "rightbar",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "80%",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "80dp",
                "verticalScrollIndicator": true,
                "width": "20%",
                "zIndex": 2
            }, {}, {});
            rightbar.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e41d691bcd1343 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0e41d691bcd1343",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "80%"
            }, {}, {});
            FlexContainer0e41d691bcd1343.setDefaultUnit(kony.flex.DP);
            var Label0bd1e19a6452d40 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0bd1e19a6452d40",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0d73a5c9c281948",
                "text": "Your Pages",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0f553e2c4233243 = new kony.ui.Label({
                "id": "Label0f553e2c4233243",
                "isVisible": true,
                "left": "32%",
                "skin": "CopydefLabel0facd22c6cf3547",
                "text": "...",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0e41d691bcd1343.add(Label0bd1e19a6452d40, Label0f553e2c4233243);
            var FlexContainer0c8b0832134164a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "FlexContainer0c8b0832134164a",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "80%"
            }, {}, {});
            FlexContainer0c8b0832134164a.setDefaultUnit(kony.flex.DP);
            var FlexContainer0b3539a8d3ecb45 = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "70%",
                "id": "FlexContainer0b3539a8d3ecb45",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ab82b7e795bb42",
                "top": "0",
                "width": "20%"
            }, {}, {});
            FlexContainer0b3539a8d3ecb45.setDefaultUnit(kony.flex.DP);
            var Image0id0f6387911943 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0id0f6387911943",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "page.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0b3539a8d3ecb45.add(Image0id0f6387911943);
            var Label0d7500841e91743 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0d7500841e91743",
                "isVisible": true,
                "left": "2%",
                "skin": "CopydefLabel0b166feed4fd44e",
                "text": "Raeeson ki dunya",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c8b0832134164a.add(FlexContainer0b3539a8d3ecb45, Label0d7500841e91743);
            var FlexContainer0f812f6dfb6244f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0f812f6dfb6244f",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "70%"
            }, {}, {});
            FlexContainer0f812f6dfb6244f.setDefaultUnit(kony.flex.DP);
            var Image0f3f2dcf2ffb043 = new kony.ui.Image2({
                "height": "80%",
                "id": "Image0f3f2dcf2ffb043",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "frame__20_.png",
                "top": "10%",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0af04b973eedd4c = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0af04b973eedd4c",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0bbc484c2dd5a46",
                "text": "Create Promotion",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0f812f6dfb6244f.add(Image0f3f2dcf2ffb043, Label0af04b973eedd4c);
            var FlexContainer0d87c8392347643 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2dp",
                "id": "FlexContainer0d87c8392347643",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ff8a9359dda642",
                "top": "2dp",
                "width": "80%"
            }, {}, {});
            FlexContainer0d87c8392347643.setDefaultUnit(kony.flex.DP);
            FlexContainer0d87c8392347643.add();
            var FlexContainer0c33607fdf4f04d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "FlexContainer0c33607fdf4f04d",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "80%"
            }, {}, {});
            FlexContainer0c33607fdf4f04d.setDefaultUnit(kony.flex.DP);
            var FlexContainer0g73f8ad168ca48 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0g73f8ad168ca48",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "60%"
            }, {}, {});
            FlexContainer0g73f8ad168ca48.setDefaultUnit(kony.flex.DP);
            var Label0addd7566852144 = new kony.ui.Label({
                "id": "Label0addd7566852144",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0f0b13dbd85ca46",
                "text": "Contacts",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0g73f8ad168ca48.add(Label0addd7566852144);
            var FlexContainer0f8db99ad83a14c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexContainer0f8db99ad83a14c",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "40%"
            }, {}, {});
            FlexContainer0f8db99ad83a14c.setDefaultUnit(kony.flex.DP);
            var Image0d1239081b9c746 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "50%",
                "id": "Image0d1239081b9c746",
                "isVisible": true,
                "left": "10%",
                "skin": "slImage",
                "src": "untitled__1_.png",
                "top": "0",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Image0b22fa206386645 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40%",
                "id": "Image0b22fa206386645",
                "isVisible": true,
                "left": "10%",
                "skin": "slImage",
                "src": "frame__1_.png",
                "top": "0",
                "width": "20%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0d2938f0b41c442 = new kony.ui.Label({
                "id": "Label0d2938f0b41c442",
                "isVisible": true,
                "left": "10%",
                "skin": "CopydefLabel0hb506fce213249",
                "text": "...",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0f8db99ad83a14c.add(Image0d1239081b9c746, Image0b22fa206386645, Label0d2938f0b41c442);
            FlexContainer0c33607fdf4f04d.add(FlexContainer0g73f8ad168ca48, FlexContainer0f8db99ad83a14c);
            var CopyFlexContainer0a0214cca58f544 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyFlexContainer0a0214cca58f544",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0cb022730b9cf46",
                "top": "0",
                "width": "80%"
            }, {}, {
                "hoverSkin": "CopyslFbox0hb0327d305b04d"
            });
            CopyFlexContainer0a0214cca58f544.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0f57aa8e534f443 = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "CopyFlexContainer0f57aa8e534f443",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ab82b7e795bb42",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0f57aa8e534f443.setDefaultUnit(kony.flex.DP);
            var CopyImage0afe6d7f49b9c45 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0afe6d7f49b9c45",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "two.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f57aa8e534f443.add(CopyImage0afe6d7f49b9c45);
            var CopyLabel0f1fd9b325b554b = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0f1fd9b325b554b",
                "isVisible": true,
                "left": "2%",
                "skin": "CopydefLabel0b0e734fcbb5a4c",
                "text": "Sanober",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0a0214cca58f544.add(CopyFlexContainer0f57aa8e534f443, CopyLabel0f1fd9b325b554b);
            var CopyFlexContainer0f64a4b6b208d49 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyFlexContainer0f64a4b6b208d49",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "80%"
            }, {}, {});
            CopyFlexContainer0f64a4b6b208d49.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0b0d62185f94e40 = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "CopyFlexContainer0b0d62185f94e40",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ab82b7e795bb42",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0b0d62185f94e40.setDefaultUnit(kony.flex.DP);
            var CopyImage0e93e94f634d444 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0e93e94f634d444",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "iqbal.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0b0d62185f94e40.add(CopyImage0e93e94f634d444);
            var CopyLabel0afb97ee23eff47 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0afb97ee23eff47",
                "isVisible": true,
                "left": "2%",
                "skin": "CopydefLabel0b0e734fcbb5a4c",
                "text": "Allama Iqbal",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f64a4b6b208d49.add(CopyFlexContainer0b0d62185f94e40, CopyLabel0afb97ee23eff47);
            var CopyFlexContainer0j4922c9c33d34e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyFlexContainer0j4922c9c33d34e",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "80%"
            }, {}, {});
            CopyFlexContainer0j4922c9c33d34e.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0b0af4a9c61a24f = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "CopyFlexContainer0b0af4a9c61a24f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ab82b7e795bb42",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0b0af4a9c61a24f.setDefaultUnit(kony.flex.DP);
            var CopyImage0cb746fe5e5294d = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0cb746fe5e5294d",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "one.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0b0af4a9c61a24f.add(CopyImage0cb746fe5e5294d);
            var CopyLabel0e2842bc09f2a45 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0e2842bc09f2a45",
                "isVisible": true,
                "left": "2%",
                "skin": "CopydefLabel0b0e734fcbb5a4c",
                "text": "Maqbool",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0j4922c9c33d34e.add(CopyFlexContainer0b0af4a9c61a24f, CopyLabel0e2842bc09f2a45);
            var CopyFlexContainer0g4cb738baca245 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyFlexContainer0g4cb738baca245",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "80%"
            }, {}, {});
            CopyFlexContainer0g4cb738baca245.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0f14c00d8ed384c = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "CopyFlexContainer0f14c00d8ed384c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ab82b7e795bb42",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0f14c00d8ed384c.setDefaultUnit(kony.flex.DP);
            var CopyImage0c673e377156743 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0c673e377156743",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "three.jpg",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0f14c00d8ed384c.add(CopyImage0c673e377156743);
            var CopyLabel0jc84230b3bb045 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0jc84230b3bb045",
                "isVisible": true,
                "left": "2%",
                "skin": "CopydefLabel0b0e734fcbb5a4c",
                "text": "kabeer",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0g4cb738baca245.add(CopyFlexContainer0f14c00d8ed384c, CopyLabel0jc84230b3bb045);
            var CopyFlexContainer0jb2d29cdf0a042 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "CopyFlexContainer0jb2d29cdf0a042",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "80%"
            }, {}, {});
            CopyFlexContainer0jb2d29cdf0a042.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0dcb61eedd8fb4c = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "CopyFlexContainer0dcb61eedd8fb4c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0ab82b7e795bb42",
                "top": "0",
                "width": "25%"
            }, {}, {});
            CopyFlexContainer0dcb61eedd8fb4c.setDefaultUnit(kony.flex.DP);
            var CopyImage0c0c7e5b854a048 = new kony.ui.Image2({
                "height": "100%",
                "id": "CopyImage0c0c7e5b854a048",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "h1.png",
                "top": "0",
                "width": "100%"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0dcb61eedd8fb4c.add(CopyImage0c0c7e5b854a048);
            var CopyLabel0c6c58350ec4344 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyLabel0c6c58350ec4344",
                "isVisible": true,
                "left": "2%",
                "skin": "CopydefLabel0b0e734fcbb5a4c",
                "text": "Shabbeer",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexContainer0jb2d29cdf0a042.add(CopyFlexContainer0dcb61eedd8fb4c, CopyLabel0c6c58350ec4344);
            var FlexContainer0b7bf629febaa40 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2dp",
                "id": "FlexContainer0b7bf629febaa40",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0be74ce7b6a8546",
                "top": "5dp",
                "width": "80%"
            }, {}, {});
            FlexContainer0b7bf629febaa40.setDefaultUnit(kony.flex.DP);
            FlexContainer0b7bf629febaa40.add();
            var FlexContainer0ca9bca3acadf49 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0ca9bca3acadf49",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "80%"
            }, {}, {});
            FlexContainer0ca9bca3acadf49.setDefaultUnit(kony.flex.DP);
            var Label0f15cf2fe07cb4f = new kony.ui.Label({
                "id": "Label0f15cf2fe07cb4f",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0ca558771c59c4b",
                "text": "Group Conversations",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ca9bca3acadf49.add(Label0f15cf2fe07cb4f);
            var FlexContainer0d218f0073dd844 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "FlexContainer0d218f0073dd844",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0fc60e8939b8941",
                "top": "0",
                "width": "80%"
            }, {}, {
                "hoverSkin": "CopyslFbox0d127a243640541"
            });
            FlexContainer0d218f0073dd844.setDefaultUnit(kony.flex.DP);
            var FlexContainer0b9be0f5478a143 = new kony.ui.FlexContainer({
                "centerY": "47%",
                "clipBounds": true,
                "height": "60%",
                "id": "FlexContainer0b9be0f5478a143",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0d1863ef520f542",
                "top": "0",
                "width": "25%"
            }, {}, {});
            FlexContainer0b9be0f5478a143.setDefaultUnit(kony.flex.DP);
            var Label0e9dfd35fb89b4b = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50%",
                "id": "Label0e9dfd35fb89b4b",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0j62fe5656b8a42",
                "text": "+",
                "top": "0",
                "width": "50%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0b9be0f5478a143.add(Label0e9dfd35fb89b4b);
            var Label0ha45625da29e46 = new kony.ui.Label({
                "id": "Label0ha45625da29e46",
                "isVisible": true,
                "left": "3%",
                "skin": "CopydefLabel0j5bfd1eeb2c34e",
                "text": "Create new group",
                "top": "27%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0d218f0073dd844.add(FlexContainer0b9be0f5478a143, Label0ha45625da29e46);
            rightbar.add(FlexContainer0e41d691bcd1343, FlexContainer0c8b0832134164a, FlexContainer0f812f6dfb6244f, FlexContainer0d87c8392347643, FlexContainer0c33607fdf4f04d, CopyFlexContainer0a0214cca58f544, CopyFlexContainer0f64a4b6b208d49, CopyFlexContainer0j4922c9c33d34e, CopyFlexContainer0g4cb738baca245, CopyFlexContainer0jb2d29cdf0a042, FlexContainer0b7bf629febaa40, FlexContainer0ca9bca3acadf49, FlexContainer0d218f0073dd844);
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1366,
                "640": {
                    "TextField0h69b95ad0f0b49": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "FlexContainer0e7d0ff2d6d7141": {
                        "skin": "flexskin",
                        "top": {
                            "type": "string",
                            "value": "10%"
                        },
                        "segmentProps": []
                    },
                    "Image0b06fadbf07d248": {
                        "height": {
                            "type": "string",
                            "value": "75%"
                        },
                        "left": {
                            "type": "string",
                            "value": "12.50%"
                        },
                        "top": {
                            "type": "string",
                            "value": "12.50%"
                        },
                        "width": {
                            "type": "string",
                            "value": "75%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0hcf66822e0064f": {
                        "skin": "flexskin",
                        "top": {
                            "type": "string",
                            "value": "10%"
                        },
                        "segmentProps": []
                    },
                    "Image0he896fdb9e2f4e": {
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "src": "icons8_menu_24.png",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexGroup0c69159e4185041": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "Image0g22b7ea6ed1b46": {
                        "segmentProps": []
                    },
                    "FlexGroup0e5c5dfcfb18144": {
                        "centerY": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "40px"
                        },
                        "width": {
                            "type": "string",
                            "value": "40px"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0f076a40bee1d43": {
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "right": {
                            "type": "string",
                            "value": "4%"
                        },
                        "top": {
                            "type": "number",
                            "value": "0"
                        },
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "Label0ac2011a9e04945": {
                        "isVisible": false,
                        "text": "Syed",
                        "segmentProps": []
                    },
                    "leftbar": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "FlexGroup0jf0a7a6b427240": {
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0fc2f0282feb449": {
                        "skin": "CopyslFbox0ed179def7bfc44",
                        "segmentProps": []
                    },
                    "Image0df6a55437e8140": {
                        "height": {
                            "type": "string",
                            "value": "30dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "25dp"
                        },
                        "segmentProps": []
                    },
                    "Label0dcbd10b02fb848": {
                        "skin": "CopydefLabel0f67ca7f182354a",
                        "text": "Live Video",
                        "segmentProps": []
                    },
                    "CopyImage0ea14c690a1c948": {
                        "height": {
                            "type": "string",
                            "value": "30dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "25dp"
                        },
                        "segmentProps": []
                    },
                    "CopyLabel0a32599046b4744": {
                        "skin": "CopydefLabel0g115c2deb4854b",
                        "segmentProps": []
                    },
                    "CopyImage0ec8355f9064444": {
                        "height": {
                            "type": "string",
                            "value": "30dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "25dp"
                        },
                        "segmentProps": []
                    },
                    "CopyLabel0g46cb8246a614c": {
                        "skin": "CopydefLabel0e3dbb656a2f344",
                        "text": "feeling/activity",
                        "segmentProps": []
                    },
                    "Label0a2ad2b9e67164f": {
                        "left": {
                            "type": "string",
                            "value": "85%"
                        },
                        "segmentProps": []
                    },
                    "Label0g292a8a13d3841": {
                        "left": {
                            "type": "string",
                            "value": "78%"
                        },
                        "text": "12 Shares",
                        "segmentProps": []
                    },
                    "FlexContainer0h6c3ad94d06145": {
                        "left": {
                            "type": "string",
                            "value": "-4%"
                        },
                        "segmentProps": []
                    },
                    "CopyLabel0j192811b760b48": {
                        "left": {
                            "type": "string",
                            "value": "85%"
                        },
                        "segmentProps": []
                    },
                    "CopyLabel0ec1a0f00ec6745": {
                        "left": {
                            "type": "string",
                            "value": "78%"
                        },
                        "segmentProps": []
                    },
                    "CopyFlexContainer0d25c5fffc23b41": {
                        "left": {
                            "type": "string",
                            "value": "-4%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0fcd441ec708c44": {
                        "width": {
                            "type": "string",
                            "value": "30%"
                        },
                        "segmentProps": []
                    },
                    "Image0d5fdcb71470740": {
                        "height": {
                            "type": "string",
                            "value": "15%"
                        },
                        "left": {
                            "type": "string",
                            "value": "12dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "-29dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "15%"
                        },
                        "segmentProps": []
                    },
                    "CopyFlexContainer0eac2f338f2e94e": {
                        "width": {
                            "type": "string",
                            "value": "30%"
                        },
                        "segmentProps": []
                    },
                    "CopyImage0d44baacc6bc64c": {
                        "height": {
                            "type": "string",
                            "value": "15%"
                        },
                        "left": {
                            "type": "string",
                            "value": "13dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "-29dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "15%"
                        },
                        "segmentProps": []
                    },
                    "CopyFlexContainer0h67a6128baab4b": {
                        "width": {
                            "type": "string",
                            "value": "30%"
                        },
                        "segmentProps": []
                    },
                    "CopyImage0f150100b8d5c4b": {
                        "height": {
                            "type": "string",
                            "value": "15%"
                        },
                        "left": {
                            "type": "string",
                            "value": "12dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "-28dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "15%"
                        },
                        "segmentProps": []
                    },
                    "CopyFlexContainer0d62ac6ee86e44b": {
                        "width": {
                            "type": "string",
                            "value": "30%"
                        },
                        "segmentProps": []
                    },
                    "CopyImage0f633540a807d4e": {
                        "height": {
                            "type": "string",
                            "value": "15%"
                        },
                        "left": {
                            "type": "string",
                            "value": "14dp"
                        },
                        "top": {
                            "type": "string",
                            "value": "-28dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "15%"
                        },
                        "segmentProps": []
                    },
                    "rightbar": {
                        "isVisible": false,
                        "segmentProps": []
                    }
                },
                "1024": {
                    "TextField0h69b95ad0f0b49": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "FlexContainer0e7d0ff2d6d7141": {
                        "skin": "flexskin",
                        "top": {
                            "type": "string",
                            "value": "5%"
                        },
                        "segmentProps": []
                    },
                    "Image0b06fadbf07d248": {
                        "height": {
                            "type": "string",
                            "value": "75%"
                        },
                        "left": {
                            "type": "string",
                            "value": "12.50%"
                        },
                        "top": {
                            "type": "string",
                            "value": "12.50%"
                        },
                        "width": {
                            "type": "string",
                            "value": "75%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0hcf66822e0064f": {
                        "skin": "flexskin",
                        "top": {
                            "type": "string",
                            "value": "5%"
                        },
                        "segmentProps": []
                    },
                    "Image0he896fdb9e2f4e": {
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "src": "icons8_menu_24.png",
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "gaming": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "FlexGroup0c69159e4185041": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "Image0d331432970ab48": {
                        "segmentProps": []
                    },
                    "Image0cda76bc834e049": {
                        "segmentProps": []
                    },
                    "Image0j8bcfdc7135249": {
                        "segmentProps": []
                    },
                    "Image0g22b7ea6ed1b46": {
                        "segmentProps": []
                    },
                    "FlexGroup0e5c5dfcfb18144": {
                        "centerY": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "40px"
                        },
                        "width": {
                            "type": "string",
                            "value": "40px"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0f076a40bee1d43": {
                        "centerY": {
                            "type": "string",
                            "value": "50%"
                        },
                        "height": {
                            "type": "string",
                            "value": "100%"
                        },
                        "width": {
                            "type": "string",
                            "value": "100%"
                        },
                        "segmentProps": []
                    },
                    "Label0ac2011a9e04945": {
                        "isVisible": false,
                        "skin": "CopydefLabel0cfdc2e808fd34f",
                        "segmentProps": []
                    },
                    "leftbar": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "FlexContainer0ic702d94ba964e": {
                        "segmentProps": []
                    },
                    "FlexGroup0jf0a7a6b427240": {
                        "centerX": {
                            "type": "string",
                            "value": ""
                        },
                        "left": {
                            "type": "string",
                            "value": "2%"
                        },
                        "width": {
                            "type": "string",
                            "value": "67%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0df3b738fa7fa48": {
                        "segmentProps": []
                    },
                    "FlexContainer0fc2f0282feb449": {
                        "centerX": {
                            "type": "string",
                            "value": "48%"
                        },
                        "skin": "CopyslFbox0d388bb9973724d",
                        "segmentProps": []
                    },
                    "Label0dc1f895a5fec45": {
                        "skin": "CopydefLabel0e3312657981f46",
                        "segmentProps": []
                    },
                    "CopyLabel0bec842078f3141": {
                        "skin": "CopydefLabel0h6f0cf8c3a594c",
                        "segmentProps": []
                    },
                    "rightbar": {
                        "left": {
                            "type": "string",
                            "value": "69%"
                        },
                        "width": {
                            "type": "string",
                            "value": "30%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0c8b0832134164a": {
                        "width": {
                            "type": "string",
                            "value": "90%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0b3539a8d3ecb45": {
                        "height": {
                            "type": "string",
                            "value": "50%"
                        },
                        "segmentProps": []
                    },
                    "Label0d7500841e91743": {
                        "segmentProps": []
                    },
                    "FlexContainer0f812f6dfb6244f": {
                        "left": {
                            "type": "string",
                            "value": "12%"
                        },
                        "width": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    },
                    "Image0f3f2dcf2ffb043": {
                        "height": {
                            "type": "string",
                            "value": "60%"
                        },
                        "top": {
                            "type": "string",
                            "value": "20%"
                        },
                        "segmentProps": []
                    },
                    "Label0af04b973eedd4c": {
                        "left": {
                            "type": "string",
                            "value": "4%"
                        },
                        "skin": "CopydefLabel0a6add256eeb341",
                        "text": "Create Promotion",
                        "segmentProps": []
                    },
                    "FlexContainer0g73f8ad168ca48": {
                        "width": {
                            "type": "string",
                            "value": "50%"
                        },
                        "segmentProps": []
                    },
                    "Label0addd7566852144": {
                        "text": "Contacts",
                        "segmentProps": []
                    },
                    "FlexContainer0f8db99ad83a14c": {
                        "width": {
                            "type": "string",
                            "value": "50%"
                        },
                        "segmentProps": []
                    },
                    "Image0d1239081b9c746": {
                        "centerY": {
                            "type": "string",
                            "value": ""
                        },
                        "left": {
                            "type": "string",
                            "value": "2%"
                        },
                        "top": {
                            "type": "string",
                            "value": "10dp"
                        },
                        "segmentProps": []
                    },
                    "Image0b22fa206386645": {
                        "centerY": {
                            "type": "string",
                            "value": ""
                        },
                        "left": {
                            "type": "string",
                            "value": "18%"
                        },
                        "top": {
                            "type": "string",
                            "value": "15dp"
                        },
                        "segmentProps": []
                    },
                    "Label0d2938f0b41c442": {
                        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                        "height": {
                            "type": "ref",
                            "value": kony.flex.USE_PREFFERED_SIZE
                        },
                        "left": {
                            "type": "string",
                            "value": "18%"
                        },
                        "top": {
                            "type": "number",
                            "value": "0"
                        },
                        "segmentProps": []
                    },
                    "CopyFlexContainer0f57aa8e534f443": {
                        "segmentProps": []
                    },
                    "CopyLabel0f1fd9b325b554b": {
                        "segmentProps": []
                    },
                    "CopyFlexContainer0b0d62185f94e40": {
                        "segmentProps": []
                    },
                    "CopyFlexContainer0b0af4a9c61a24f": {
                        "segmentProps": []
                    },
                    "CopyFlexContainer0f14c00d8ed384c": {
                        "segmentProps": []
                    },
                    "CopyFlexContainer0dcb61eedd8fb4c": {
                        "segmentProps": []
                    },
                    "Label0f15cf2fe07cb4f": {
                        "skin": "CopydefLabel0e33878bdf68f47",
                        "text": "Group Conversations",
                        "segmentProps": []
                    },
                    "FlexContainer0b9be0f5478a143": {
                        "width": {
                            "type": "string",
                            "value": "20%"
                        },
                        "segmentProps": []
                    },
                    "Label0ha45625da29e46": {
                        "left": {
                            "type": "string",
                            "value": "2%"
                        },
                        "skin": "CopydefLabel0d0b6d3ca587a4e",
                        "text": "Create new group",
                        "top": {
                            "type": "string",
                            "value": "32%"
                        },
                        "segmentProps": []
                    }
                },
                "1366": {
                    "TextField0h69b95ad0f0b49": {
                        "left": {
                            "type": "string",
                            "value": "2%"
                        },
                        "segmentProps": []
                    },
                    "FlexContainer0e7d0ff2d6d7141": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "FlexContainer0hcf66822e0064f": {
                        "isVisible": false,
                        "segmentProps": []
                    },
                    "leftbar": {
                        "segmentProps": []
                    },
                    "FlexContainer0fc2f0282feb449": {
                        "skin": "CopyslFbox0f5275413250c4f",
                        "segmentProps": []
                    },
                    "Label0dc1f895a5fec45": {
                        "skin": "CopydefLabel0b71f8c8ade2745",
                        "segmentProps": []
                    },
                    "CopyLabel0bec842078f3141": {
                        "skin": "CopydefLabel0bd9f0c010dbd4f",
                        "segmentProps": []
                    },
                    "Label0bd1e19a6452d40": {
                        "text": "Your Pages",
                        "segmentProps": []
                    },
                    "FlexContainer0b3539a8d3ecb45": {
                        "height": {
                            "type": "string",
                            "value": "60%"
                        },
                        "segmentProps": []
                    },
                    "Label0d7500841e91743": {
                        "segmentProps": []
                    },
                    "Image0f3f2dcf2ffb043": {
                        "height": {
                            "type": "string",
                            "value": "60%"
                        },
                        "top": {
                            "type": "string",
                            "value": "20%"
                        },
                        "segmentProps": []
                    },
                    "Label0af04b973eedd4c": {
                        "left": {
                            "type": "string",
                            "value": "3%"
                        },
                        "text": "Create Promotion",
                        "segmentProps": []
                    },
                    "Label0addd7566852144": {
                        "text": "Contacts",
                        "segmentProps": []
                    },
                    "Label0f15cf2fe07cb4f": {
                        "text": "Group Conversations",
                        "segmentProps": []
                    },
                    "FlexContainer0b9be0f5478a143": {
                        "width": {
                            "type": "string",
                            "value": "20%"
                        },
                        "segmentProps": []
                    },
                    "Label0ha45625da29e46": {
                        "top": {
                            "type": "string",
                            "value": "31%"
                        },
                        "segmentProps": []
                    }
                }
            }
            this.add(menubar, leftbar, FlexScrollGroup0i1b01947249041, rightbar);
        };
        return [{
            "addWidgets": addWidgetsDashboard,
            "enabledForIdleTimeout": false,
            "id": "Dashboard",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0gf3cd251a1924f",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366],
            "info": {
                "kuid": "b7a6ffcef4c44a45a78f8ebb32dcf27a"
            }
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});